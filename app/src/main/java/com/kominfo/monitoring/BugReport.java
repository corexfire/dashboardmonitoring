package com.kominfo.monitoring;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.kominfo.monitoring.config.AppConfig;
import com.kominfo.monitoring.helper.Encryptions;
import  com.kominfo.monitoring.helper.SQLiteHandler;

import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class BugReport extends Fragment {
    SQLiteHandler db;
    String GET_UID;
    EditText subject, content;
    Button btnSend;
    Encryptions encryptions;
    public BugReport() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bug_report, container, false);
        db = new SQLiteHandler(getActivity().getApplicationContext());
        GET_UID = db.getUserDetails().get("uid");
        subject = (EditText)  view.findViewById(R.id.editTextSubject);
        content = (EditText) view.findViewById(R.id.editTextContent);
        encryptions = new Encryptions();
        btnSend = (Button) view.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AndroidNetworking.post(AppConfig.link + "bugs/report")
                        .addBodyParameter("userID",  encryptions.getDecrypt(GET_UID))
                        .addBodyParameter("subject", subject.getText().toString())
                        .addBodyParameter("content", content.getText().toString())
                        .setTag("Bug Report")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.optString("status").equals("200")) {
                                    Toast.makeText(getActivity().getApplicationContext(), "Bug Report sent", Toast.LENGTH_SHORT).show();
                                    subject.setText("");
                                    content.setText("");
                                }

                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

}
