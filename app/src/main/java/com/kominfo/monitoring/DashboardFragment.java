package com.kominfo.monitoring;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kominfo.monitoring.helper.GetVersion;
import com.kominfo.monitoring.helper.SQLiteHandler;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    TextView textSite, textCoordinate, percenProgress, countUncomplete, countActivityLog, countLocal,textVersionName;
    String GET_PERCENTAGE, GET_PROGRESS, GET_ACTIVITY, GET_SITE, GET_COORDINATE;
    SQLiteHandler db;
    Button btnTaskList, btnLog;
    GetVersion getVersion;
    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        getVersion = new GetVersion();
        textSite = (TextView) view.findViewById(R.id.textSite);
        textCoordinate = (TextView) view.findViewById(R.id.textCoordinate);
        percenProgress = (TextView) view.findViewById(R.id.percenProgress);
        countUncomplete = (TextView) view.findViewById(R.id.countUncomplete);
        countActivityLog = (TextView) view.findViewById(R.id.countActivityLog);
        countLocal = (TextView) view.findViewById(R.id.countLocal);
        textVersionName = (TextView) view.findViewById(R.id.textVersionName);
        textVersionName.setText(getVersion.getVersionName(getActivity().getApplicationContext()));
        btnLog = (Button) view.findViewById(R.id.btnLog);
        btnLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogFragment dasbboard = new LogFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, dasbboard, "taskList");
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().setTitle("Activity Log");
                fragmentTransaction.commit();
            }
        });
        btnTaskList = (Button) view.findViewById(R.id.btnTaskList);
        btnTaskList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SiteListFragment dasbboard = new SiteListFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, dasbboard, "sitelist");
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().setTitle("Site List");
                fragmentTransaction.commit();
            }
        });

        db = new SQLiteHandler(getActivity().getApplicationContext());
        try {

            GET_PERCENTAGE = db.getDashboardDetails().get("persentase");
            GET_PROGRESS = db.getDashboardDetails().get("progress");
            GET_ACTIVITY = db.getDashboardDetails().get("activity_log");
            GET_SITE  = db.getSite().get("title");
            GET_COORDINATE = "Long: " + db.getSite().get("longitude") + " - Lat: " + db.getSite().get("latitude");
            percenProgress.setText(GET_PERCENTAGE + "%");
            countActivityLog.setText(GET_ACTIVITY);
            countUncomplete.setText(GET_PROGRESS);
            textSite.setText(db.getUserDetails().get("name"));
            textCoordinate.setText(GET_COORDINATE);
            Thread.sleep(400);
        }catch (Exception e){

        }

        // Inflate the layout for this fragment
        return view;
    }

}
