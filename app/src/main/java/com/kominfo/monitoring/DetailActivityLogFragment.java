package com.kominfo.monitoring;


import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.kominfo.monitoring.config.AppConfig;
import com.kominfo.monitoring.helper.CheckConnection;
import com.kominfo.monitoring.helper.Encryptions;
import com.kominfo.monitoring.helper.GPSTracker;
import com.kominfo.monitoring.helper.ImageToString;
import com.kominfo.monitoring.helper.SQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Date;

import static android.R.attr.id;
import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailActivityLogFragment extends Fragment {
    final static String SITE_ACTIVITY_ID = "site_activity_id";
    final static  String ACTIVITY_TITLE = "activity_TITLE";
    final static String ACTIVITY_ID = "activity_id";
    final static String PERCENTACE = "percentage";
    final static String DATE_MODIFIED = "date_modified";
    final static String MILESTONE_ID = "milestone_id";
    final static String SITE_ID = "site_id";
    final static String ID = "id";
    final static String DATE_CREATED = "date_created";
    final static String LONGITUDE = "longitude";
    final static String LATITUDE = "latitude";
    final static String CONTENT = "content";
    final static String STATE = "state";
    ImageToString imageToString;
    Bitmap myBitmap, myBitmap2, myBitmap3, myBitmap4, myBitmap5, myBitmap6, myBitmap7, myBitmap8, myBitmap9, myBitmap10, myBitmap11, myBitmap12, myBitmap13, myBitmap14, myBitmap15;
    FloatingActionButton floatEdit, floatTakePicture, floatIssue;
    SQLiteHandler db;
    LocationManager locationManager;
    String new_id;
    String GET_LAT_IMAGE;
    String GET_LONG_IMAGE;
    String GET_ID;
    String GET_LONGITUDE;
    String GET_LATITUDE;
    String GET_ACTIVITY_ID;
    String GET_ACTIVITY_NAME;
    String GET_PERCENTAGE;
    String GET_DATE_MODIFIED;
    String GET_DATE_CREATED;
    String GET_MILESTONE_ID;
    String GET_SITE_ACTIVITY_ID;
    String GET_SITE_ID;
    String GET_CONTENT;
    String GET_MILESTONE_NAME;
    String GET_SITE_NAME;
    String GET_STATE;
    String filePath1_64, filePath2_64,filePath3_64,filePath4_64,filePath5_64,filePath6_64,filePath7_64,filePath8_64,filePath9_64,filePath10_64,filePath11_64,filePath12_64,filePath13_64,filePath14_64,filePath15_64;
    String filePath1, filePath2, filePath3, filePath4,filePath5,filePath6,filePath7,filePath8,filePath9,filePath10,filePath11,filePath12,filePath13,filePath14,filePath15;
    Integer count = 0;
    Integer totalCount = 0;
    Uri outputFileUri;
    LinearLayout lo1, lo2, lo3, lo4, lo5, lo6, lo7, lo8, lo9, lo10, lo11, lo12, lo13, lo14, lo15;
    ProgressBar progressBar;
    GPSTracker gps;
    private String KEY_IMAGE = "image";
    private String KEY_NAME = "image_name";
    File mediaFile;
    Encryptions encryptions;
    private String KEY_IMAGE_2 = "image_2";
    private String KEY_NAME_2 = "image_name_2";
    File mediaFile2;

    private String KEY_IMAGE_3 = "image_3";
    private String KEY_NAME_3 = "image_name_3";
    File mediaFile3;

    private String KEY_IMAGE_4 = "image_4";
    private String KEY_NAME_4 = "image_name_4";
    File mediaFile4;

    private String KEY_IMAGE_5 = "image_5";
    private String KEY_NAME_5 = "image_name_5";
    File mediaFile5;


    CheckConnection checkConnection;
    String f1, f2,f3,f4,f5;
    TextView textTitle, milestone, site, persentase, modified, coordinate;
    TextView textImageLat1, textImageLong1,textImageLat2, textImageLong2,textImageLat3, textImageLong3, textImageLat4, textImageLong4,textImageLat5, textImageLong5,textImageLat6, textImageLong6,textImageLat7, textImageLong7,textImageLat8, textImageLong8,textImageLat9, textImageLong9,textImageLat10, textImageLong10,textImageLat11, textImageLong11,textImageLat12, textImageLong12,textImageLat13, textImageLong13,textImageLat14, textImageLong14,textImageLat15, textImageLong15;
    Button deleteImg1,deleteImg2,deleteImg3,deleteImg4,deleteImg5,deleteImg6,deleteImg7,deleteImg8,deleteImg9,deleteImg10,deleteImg11,deleteImg12,deleteImg13,deleteImg14,deleteImg15;
    Integer uploadFoto = 0;
    Button btnTakePicture, btnSave, btnDelete;
    ImageView img1, img2, img3, img4, img5, img6, img7, img8, img9, img10, img11, img12, img13, img14, img15;
    TextView txtDeskripsi, txtPersentase, txtUploadPhoto;
    LinearLayout linearLayout;
    EditText et_description;
    Button btnDeteleImg1, btnDeteleImg2,btnDeteleImg3,btnDeteleImg4,btnDeteleImg5,btnDeteleImg6,btnDeteleImg7,btnDeteleImg8,btnDeteleImg9,btnDeteleImg10,btnDeteleImg11,btnDeteleImg12,btnDeteleImg13,btnDeteleImg14,btnDeteleImg15;
    Button btnSaveLocal, btnCancel;

    public DetailActivityLogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_activity_log, container, false);
        Bundle args = getArguments();
        encryptions = new Encryptions();
        imageToString = new ImageToString();
        db = new SQLiteHandler(getActivity().getApplicationContext());
        btnSaveLocal = (Button) view.findViewById(R.id.btnSave);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        txtUploadPhoto = (TextView) view.findViewById(R.id.textUploadFoto);
        btnDeteleImg1 = (Button) view.findViewById(R.id.btnDeteleImg1);
        btnDeteleImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath1 = null;
                                    lo1.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg2 = (Button) view.findViewById(R.id.btnDeteleImg2);
        btnDeteleImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath2 = null;
                                    lo2.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg3 = (Button) view.findViewById(R.id.btnDeteleImg3);
        btnDeteleImg3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath3 = null;
                                    lo3.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg4 = (Button) view.findViewById(R.id.btnDeteleImg4);
        btnDeteleImg4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath4 = null;
                                    lo4.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg5 = (Button) view.findViewById(R.id.btnDeteleImg5);
        btnDeteleImg5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath5 = null;
                                    lo5.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg6 = (Button) view.findViewById(R.id.btnDeteleImg6);
        btnDeteleImg6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath6 = null;
                                    lo6.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg7 = (Button) view.findViewById(R.id.btnDeteleImg7);
        btnDeteleImg7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath7 = null;
                                    lo7.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg8 = (Button) view.findViewById(R.id.btnDeteleImg8);
        btnDeteleImg8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath8 = null;
                                    lo8.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg9 = (Button) view.findViewById(R.id.btnDeteleImg9);
        btnDeteleImg9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath9 = null;
                                    lo9.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg10 = (Button) view.findViewById(R.id.btnDeteleImg10);
        btnDeteleImg10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath10 = null;
                                    lo10.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg11 = (Button) view.findViewById(R.id.btnDeteleImg11);
        btnDeteleImg11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath11 = null;
                                    lo11.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg12 = (Button) view.findViewById(R.id.btnDeteleImg12);
        btnDeteleImg12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath12 = null;
                                    lo12.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg13 = (Button) view.findViewById(R.id.btnDeteleImg13);
        btnDeteleImg13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath13 = null;
                                    lo13.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg14 = (Button) view.findViewById(R.id.btnDeteleImg14);
        btnDeteleImg14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath14 = null;
                                    lo14.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        btnDeteleImg15 = (Button) view.findViewById(R.id.btnDeteleImg15);
        btnDeteleImg15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage("Really want to delete this picture?")
                            .setNegativeButton(android.R.string.no,null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filePath15 = null;
                                    lo15.setVisibility(View.GONE);
                                    totalCount -= 1;
                                    txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
                                }
                            });
                }catch (Exception e){

                }
            }
        });
        floatEdit = (FloatingActionButton) view.findViewById(R.id.btnedit);
        floatEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtDeskripsi.setVisibility(View.GONE);
                et_description.setText(txtDeskripsi.getText().toString());
                et_description.setVisibility(View.VISIBLE);
                floatTakePicture.setVisibility(View.VISIBLE);
                floatEdit.setVisibility(View.GONE);
                btnSave.setVisibility(View.GONE);
                btnDelete.setVisibility(View.GONE);
                btnSaveLocal.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
                if(filePath1 != null){
                    btnDeteleImg1.setVisibility(View.VISIBLE);
                }
                if(filePath2 != null){
                    btnDeteleImg2.setVisibility(View.VISIBLE);
                }
                if(filePath3 != null){
                    btnDeteleImg3.setVisibility(View.VISIBLE);
                }
                if(filePath4 != null){
                    btnDeteleImg4.setVisibility(View.VISIBLE);
                }
                if(filePath5 != null){
                    btnDeteleImg5.setVisibility(View.VISIBLE);
                }
                if(filePath6 != null){
                    btnDeteleImg6.setVisibility(View.VISIBLE);
                }
                if(filePath7 != null){
                    btnDeteleImg7.setVisibility(View.VISIBLE);
                }
                if(filePath8 != null){
                    btnDeteleImg8.setVisibility(View.VISIBLE);
                }
                if(filePath9 != null){
                    btnDeteleImg9.setVisibility(View.VISIBLE);
                }
                if(filePath10 != null){
                    btnDeteleImg10.setVisibility(View.VISIBLE);
                }
                if(filePath11 != null){
                    btnDeteleImg11.setVisibility(View.VISIBLE);
                }
                if(filePath12 != null){
                    btnDeteleImg12.setVisibility(View.VISIBLE);
                }
                if(filePath13 != null){
                    btnDeteleImg13.setVisibility(View.VISIBLE);
                }
                if(filePath14 != null){
                    btnDeteleImg14.setVisibility(View.VISIBLE);
                }
                if(filePath15 != null){
                    btnDeteleImg15.setVisibility(View.VISIBLE);
                }

            }
        });
        floatTakePicture = (FloatingActionButton) view.findViewById(R.id.btnTakePicture);
        floatIssue = (FloatingActionButton) view.findViewById(R.id.btnIssue);
        et_description = (EditText) view.findViewById(R.id.et_deskripsi);
        if (args != null) {
            GET_ACTIVITY_NAME =  args.getString(ACTIVITY_TITLE);
            GET_ACTIVITY_ID = args.getString(ACTIVITY_ID);
            GET_DATE_CREATED = args.getString(DATE_CREATED);
            GET_DATE_MODIFIED = args.getString(DATE_MODIFIED);
            GET_SITE_ACTIVITY_ID = args.getString(SITE_ACTIVITY_ID);
            GET_PERCENTAGE = args.getString(PERCENTACE);
            GET_MILESTONE_ID = args.getString(MILESTONE_ID);
            GET_SITE_ID = args.getString(SITE_ID);
            GET_LATITUDE = args.getString(LATITUDE);
            GET_LONGITUDE = args.getString(LONGITUDE);
            GET_CONTENT = args.getString(CONTENT);
            GET_ID =  args.getString(ID);
            GET_STATE = args.getString(STATE);

        }
        Log.println(Log.DEBUG, "current date", GET_DATE_CREATED);
        Log.println(Log.DEBUG,"GET_SITE_ACTIVITY_ID",GET_SITE_ACTIVITY_ID);
        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
        if(GET_STATE.equals("online")){
            linearLayout.setVisibility(View.GONE);
            floatIssue.setVisibility(View.GONE);
            floatEdit.setVisibility(View.GONE);
        }else{
            floatIssue.setVisibility(View.VISIBLE);
            floatEdit.setVisibility(View.VISIBLE);
        }
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar3);
        txtDeskripsi = (TextView) view.findViewById(R.id.edt_deskripsi);
        txtDeskripsi.setText(GET_CONTENT);
        txtPersentase = (TextView) view.findViewById(R.id.edt_persentase);
        txtPersentase.setText(GET_PERCENTAGE);
        textTitle = (TextView) view.findViewById(R.id.textTask);
        textTitle.setText(GET_ACTIVITY_NAME);
        textImageLat1 = (TextView) view.findViewById(R.id.textImageLat);
        textImageLat1.setText(GET_LATITUDE);
        textImageLong1 = (TextView) view.findViewById(R.id.textImageLong);
        textImageLong1.setText(GET_LONGITUDE);
        textImageLat2 = (TextView) view.findViewById(R.id.textImageLat2);
        textImageLat2.setText(GET_LATITUDE);
        textImageLong2 = (TextView) view.findViewById(R.id.textImageLong2);
        textImageLong2.setText(GET_LONGITUDE);
        textImageLat3 = (TextView) view.findViewById(R.id.textImageLat3);
        textImageLat3.setText(GET_LATITUDE);
        textImageLong3 = (TextView) view.findViewById(R.id.textImageLong3);
        textImageLong3.setText(GET_LONGITUDE);
        textImageLat4 = (TextView) view.findViewById(R.id.textImageLat4);
        textImageLat4.setText(GET_LATITUDE);
        textImageLong4 = (TextView) view.findViewById(R.id.textImageLong4);
        textImageLong4.setText(GET_LONGITUDE);
        textImageLat5 = (TextView) view.findViewById(R.id.textImageLat5);
        textImageLat5.setText(GET_LATITUDE);
        textImageLong5 = (TextView) view.findViewById(R.id.textImageLong5);
        textImageLong5.setText(GET_LONGITUDE);
        textImageLat6 = (TextView) view.findViewById(R.id.textImageLat6);
        textImageLat6.setText(GET_LATITUDE);
        textImageLong6 = (TextView) view.findViewById(R.id.textImageLong6);
        textImageLong6.setText(GET_LONGITUDE);
        textImageLat7 = (TextView) view.findViewById(R.id.textImageLat7);
        textImageLat7.setText(GET_LATITUDE);
        textImageLong7 = (TextView) view.findViewById(R.id.textImageLong7);
        textImageLong7.setText(GET_LONGITUDE);
        textImageLat8 = (TextView) view.findViewById(R.id.textImageLat8);
        textImageLat8.setText(GET_LATITUDE);
        textImageLong8 = (TextView) view.findViewById(R.id.textImageLong8);
        textImageLong8.setText(GET_LONGITUDE);
        textImageLat9 = (TextView) view.findViewById(R.id.textImageLat9);
        textImageLat9.setText(GET_LATITUDE);
        textImageLong9 = (TextView) view.findViewById(R.id.textImageLong9);
        textImageLong9.setText(GET_LONGITUDE);
        textImageLat10 = (TextView) view.findViewById(R.id.textImageLat10);
        textImageLat10.setText(GET_LATITUDE);
        textImageLong10 = (TextView) view.findViewById(R.id.textImageLong10);
        textImageLong10.setText(GET_LONGITUDE);
        textImageLat11 = (TextView) view.findViewById(R.id.textImageLat11);
        textImageLat11.setText(GET_LATITUDE);
        textImageLong11 = (TextView) view.findViewById(R.id.textImageLong11);
        textImageLong11.setText(GET_LONGITUDE);
        textImageLat12 = (TextView) view.findViewById(R.id.textImageLat12);
        textImageLat12.setText(GET_LATITUDE);
        textImageLong12 = (TextView) view.findViewById(R.id.textImageLong12);
        textImageLong12.setText(GET_LONGITUDE);
        textImageLat13 = (TextView) view.findViewById(R.id.textImageLat13);
        textImageLat13.setText(GET_LATITUDE);
        textImageLong13 = (TextView) view.findViewById(R.id.textImageLong13);
        textImageLong13.setText(GET_LONGITUDE);
        textImageLat14 = (TextView) view.findViewById(R.id.textImageLat14);
        textImageLat14.setText(GET_LATITUDE);
        textImageLong14 = (TextView) view.findViewById(R.id.textImageLong14);
        textImageLong14.setText(GET_LONGITUDE);
        textImageLat15 = (TextView) view.findViewById(R.id.textImageLat15);
        textImageLat15.setText(GET_LATITUDE);
        textImageLong15 = (TextView) view.findViewById(R.id.textImageLong15);
        textImageLong15.setText(GET_LONGITUDE);
        coordinate = (TextView) view.findViewById(R.id.textCoordinate);
        coordinate.setText("Lat : " + GET_LATITUDE + " - Long : " + GET_LONGITUDE);
        milestone = (TextView) view.findViewById(R.id.textMilestone) ;
        lo1 = (LinearLayout) view.findViewById(R.id.linear1);
        lo2 = (LinearLayout) view.findViewById(R.id.linear2);
        lo3 = (LinearLayout) view.findViewById(R.id.linear3);
        lo4 = (LinearLayout) view.findViewById(R.id.linear4);
        lo5 = (LinearLayout) view.findViewById(R.id.linear5);
        lo6 = (LinearLayout) view.findViewById(R.id.linear6);
        lo7 = (LinearLayout) view.findViewById(R.id.linear7);
        lo8 = (LinearLayout) view.findViewById(R.id.linear8);
        lo9 = (LinearLayout) view.findViewById(R.id.linear9);
        lo10 = (LinearLayout) view.findViewById(R.id.linear10);
        lo11 = (LinearLayout) view.findViewById(R.id.linear11);
        lo12 = (LinearLayout) view.findViewById(R.id.linear12);
        lo13 = (LinearLayout) view.findViewById(R.id.linear13);
        lo14 = (LinearLayout) view.findViewById(R.id.linear14);
        lo15 = (LinearLayout) view.findViewById(R.id.linear15);
        site = (TextView) view.findViewById(R.id.textSite);
        img1 = (ImageView) view.findViewById(R.id.img1);
        img2 = (ImageView) view.findViewById(R.id.img2);
        img3 = (ImageView) view.findViewById(R.id.img3);
        img4 = (ImageView) view.findViewById(R.id.img4);
        img5 = (ImageView) view.findViewById(R.id.img5);
        img6 = (ImageView) view.findViewById(R.id.img6);
        img7 = (ImageView) view.findViewById(R.id.img7);
        img8 = (ImageView) view.findViewById(R.id.img8);
        img9 = (ImageView) view.findViewById(R.id.img9);
        img10 = (ImageView) view.findViewById(R.id.img10);
        img11 = (ImageView) view.findViewById(R.id.img11);
        img12 = (ImageView) view.findViewById(R.id.img12);
        img13 = (ImageView) view.findViewById(R.id.img13);
        img14 = (ImageView) view.findViewById(R.id.img14);
        img15 = (ImageView) view.findViewById(R.id.img15);
        filePath1_64 = "";
        filePath2_64 = "";
        filePath3_64 = "";
        filePath4_64 = "";
        filePath5_64 = "";
        filePath6_64 = "";
        filePath7_64 = "";
        filePath8_64 = "";
        filePath9_64 = "";
        filePath10_64 = "";
        filePath11_64 = "";
        filePath12_64 = "";
        filePath13_64 = "";
        filePath14_64 = "";
        filePath15_64 = "";
        persentase = (TextView) view.findViewById(R.id.textPercentage);
        modified = (TextView) view.findViewById(R.id.textModified);
        btnSave = (Button) view.findViewById(R.id.btnUpload);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkConnection.isInternetAvailable(getContext()))
                upload();
                else
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage(R.string.no_internet)
                            .setNegativeButton(android.R.string.yes, null)
                            .create().show();
                }
            }
        });
        btnDelete = (Button) view.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.deleteTempLocal(encryptions.getEncrypt(GET_ID));
                LogFragment dasbboard = new LogFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, dasbboard, "taskList");
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().setTitle("Activity Log");
                fragmentTransaction.commit();
            }
        });

        String queryMilestone = "Select * from dm_milestone where id ='" +  encryptions.getEncrypt(GET_MILESTONE_ID) + "'";
        Log.println(Log.DEBUG,"query",queryMilestone);
        SQLiteDatabase dbs = db.getReadableDatabase();
        Cursor cursorMilestone = dbs.rawQuery(queryMilestone, null);
        cursorMilestone.moveToFirst();
        if(cursorMilestone.getCount() > 0){
            GET_MILESTONE_NAME = cursorMilestone.getString(cursorMilestone.getColumnIndex("name"));
            milestone.setText(encryptions.getDecrypt(GET_MILESTONE_NAME));
        }
        cursorMilestone.close();

        String querySite = "Select * from dm_site where id ='" + encryptions.getEncrypt(GET_SITE_ID) + "'";
        Log.println(Log.DEBUG,"query",querySite);
        SQLiteDatabase dbSite = db.getReadableDatabase();
        Cursor cursorSite = dbSite.rawQuery(querySite, null);
        cursorSite.moveToFirst();
        if(cursorSite.getCount() > 0){
            GET_SITE_NAME = cursorSite.getString(cursorSite.getColumnIndex("title"));
//            GET_STATE = cursorSite.getString(cursorSite.getColumnIndex("state"));
            site.setText(encryptions.getDecrypt(GET_SITE_NAME));
        }
        cursorSite.close();
        Integer count = 0;
        String query = "SELECT * FROM dm_activity_pict where activity_log_id ='" + encryptions.getEncrypt(GET_ID) + "'";
        Log.println(Log.DEBUG,"query",query);
        SQLiteDatabase dbActLocal = db.getReadableDatabase();

        Cursor cursorActLocal = dbActLocal.rawQuery(query, null);
        cursorActLocal.moveToFirst();
//        File mediaStorageDir = new File(
//                Environment.getExternalStorageDirectory()
//                        + File.separator
//                        + "bts_kominfo"
//                        + File.separator
//                        + "picture"
//                        + File.separator
//                        + GET_ID
//                        + File.separator
//
//        );
        Log.println(Log.DEBUG,"query count",String.valueOf(cursorActLocal.getCount()));
        if(cursorActLocal.getCount() > 0){
            do {
                count += 1;
                switch (count){
                    case 1:
                        filePath1 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath1 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }else{
//                            filePath1 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }

                        Log.println(Log.DEBUG, "filePath1", filePath1);
                        break;
                    case 2:
                        filePath2 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath2 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath2 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath2", filePath2);
                        break;
                    case 3:
                        filePath3 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath3 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath3 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath3", filePath3);
                        break;
                    case 4:
                        filePath4 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath4 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath4 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath4", filePath4);
                        break;
                    case 5:
                        filePath5 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath5 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath5 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath5", filePath5);
                        break;
                    case 6:
                        filePath6 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath6 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }else{
//                            filePath6 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }

                        Log.println(Log.DEBUG, "filePath1", filePath6);
                        break;
                    case 7:
                        filePath7 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath7 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath7 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath2", filePath7);
                        break;
                    case 8:
                        filePath8 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath8 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath8 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath3", filePath8);
                        break;
                    case 9:
                        filePath9 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath9 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath9 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath4", filePath9);
                        break;
                    case 10:
                        filePath10 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath10 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath10 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath5", filePath10);
                        break;
                    case 11:
                        filePath11 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath11 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }else{
//                            filePath11 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }

                        Log.println(Log.DEBUG, "filePath1", filePath11);
                        break;
                    case 12:
                        filePath12 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath12 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath12 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath2", filePath12);
                        break;
                    case 13:
                        filePath13 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath13 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath13 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath3", filePath13);
                        break;
                    case 14:
                        filePath14 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath14 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath14 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath4", filePath14);
                        break;
                    case 15:
                        filePath15 = cursorActLocal.getString(cursorActLocal.getColumnIndex("s_photo"));
//                        if(GET_STATE.equals("local")){
//                            filePath15 = cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
//                        else {
//                            filePath15 = mediaStorageDir.getPath() + "/" + cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"));
//                        }
                        Log.println(Log.DEBUG, "filePath5", filePath15);
                        break;
                }

            }while (cursorActLocal.moveToNext());
            cursorActLocal.close();
            totalCount=count;
            txtUploadPhoto.setText("Upload Foto ("+ String.valueOf(totalCount)+"/15)");
        }

        try{
            if(filePath1 != null){
                Log.println(Log.DEBUG,"GET_STATE", GET_STATE);
                Log.println(Log.DEBUG,"filepath detail", filePath1);
//                myBitmap = BitmapFactory.decodeFile(filePath1);
//                if(GET_STATE.equals("local")){
//                    myBitmap = getResizedBitmap(myBitmap, 320, 320);
//                    myBitmap = mark(myBitmap);
//                }
//
                filePath1_64 =  filePath1;
//
//                Log.println(Log.DEBUG,"photo", filePath1_64);
//                img1.setImageBitmap(myBitmap);
                img1.setImageBitmap(imageToString.Base64ToImage(filePath1));
                textImageLat1.setText("Lat : " + GET_LATITUDE);
                textImageLong1.setText("Long : " + GET_LONGITUDE);
                lo1.setVisibility(View.VISIBLE);
            }else{
                filePath1_64="";
            }

            if(filePath2 != null){
                img2.setImageBitmap(imageToString.Base64ToImage(filePath2));
//                myBitmap2 = BitmapFactory.decodeFile(filePath2);
//                if(GET_STATE.equals("local")){
//                    myBitmap2 = getResizedBitmap(myBitmap2, 320, 320);
//                    myBitmap2 = mark(myBitmap2);
//                }
//                img2.setImageBitmap(myBitmap2);
                filePath2_64 =  filePath2;
                textImageLat2.setText("Lat : " + GET_LATITUDE);
                textImageLong2.setText("Long : " + GET_LONGITUDE);
                lo2.setVisibility(View.VISIBLE);
            }else{
                filePath2_64="";
            }

            if(filePath3 != null){
                img3.setImageBitmap(imageToString.Base64ToImage(filePath3));
//                myBitmap3 = BitmapFactory.decodeFile(filePath3);
//                if(GET_STATE.equals("local")){
//                    myBitmap3 = getResizedBitmap(myBitmap3, 320, 320);
//                    myBitmap3 = mark(myBitmap3);
//                }
//                img3.setImageBitmap(myBitmap3);
                filePath3_64 =  filePath3;
                textImageLat3.setText("Lat : " + GET_LATITUDE);
                textImageLong3.setText("Long : " + GET_LONGITUDE);
                lo3.setVisibility(View.VISIBLE);

            }else{
                filePath3_64="";
            }

            if(filePath4 != null){
                img4.setImageBitmap(imageToString.Base64ToImage(filePath4));
//                myBitmap4 = BitmapFactory.decodeFile(filePath4);
//                if(GET_STATE.equals("local")){
//                    myBitmap4 = getResizedBitmap(myBitmap4, 320, 320);
//                    myBitmap4 = mark(myBitmap4);
//                }
//                img4.setImageBitmap(myBitmap4);
                filePath4_64 =  filePath4;
                textImageLat4.setText("Lat : " + GET_LATITUDE);
                textImageLong4.setText("Long : " + GET_LONGITUDE);
                lo4.setVisibility(View.VISIBLE);
            }else{
                filePath4_64="";
            }

            if(filePath5 != null){
                img5.setImageBitmap(imageToString.Base64ToImage(filePath5));
//                myBitmap5 = BitmapFactory.decodeFile(filePath5);
//
//                if(GET_STATE.equals("local")){
//                    myBitmap5 = getResizedBitmap(myBitmap5, 320, 320);
//                    myBitmap5 = mark(myBitmap5);
//                }
                filePath5_64 =  filePath5;
//                img5.setImageBitmap(myBitmap5);
                textImageLat5.setText("Lat : " + GET_LATITUDE);
                textImageLong5.setText("Long : " + GET_LONGITUDE);
                lo5.setVisibility(View.VISIBLE);
            }else{
                filePath5_64="";
            }
            if(filePath6 != null){
                img6.setImageBitmap(imageToString.Base64ToImage(filePath6));
//                Log.println(Log.DEBUG,"GET_STATE", GET_STATE);
//                Log.println(Log.DEBUG,"filepath detail", filePath6);
//                myBitmap6 = BitmapFactory.decodeFile(filePath6);
//                if(GET_STATE.equals("local")){
//                    myBitmap6 = getResizedBitmap(myBitmap6, 320, 320);
//                    myBitmap6 = mark(myBitmap6);
//                }

                filePath6_64 =  filePath6;
                Log.println(Log.DEBUG,"photo", filePath6_64);
//                img6.setImageBitmap(myBitmap6);
                textImageLat6.setText("Lat : " + GET_LATITUDE);
                textImageLong6.setText("Long : " + GET_LONGITUDE);
                lo6.setVisibility(View.VISIBLE);
            }else{
                filePath6_64="";
            }

            if(filePath7 != null){
                img7.setImageBitmap(imageToString.Base64ToImage(filePath7));
//                myBitmap7 = BitmapFactory.decodeFile(filePath7);
//                if(GET_STATE.equals("local")){
//                    myBitmap7 = getResizedBitmap(myBitmap7, 320, 320);
//                    myBitmap7 = mark(myBitmap7);
//                }
//                img7.setImageBitmap(myBitmap7);
                filePath7_64 =  filePath7;
                textImageLat7.setText("Lat : " + GET_LATITUDE);
                textImageLong7.setText("Long : " + GET_LONGITUDE);
                lo7.setVisibility(View.VISIBLE);
            }else{
                filePath7_64="";
            }

            if(filePath8 != null){
                img8.setImageBitmap(imageToString.Base64ToImage(filePath8));
//                myBitmap8 = BitmapFactory.decodeFile(filePath8);
//                if(GET_STATE.equals("local")){
//                    myBitmap8 = getResizedBitmap(myBitmap8, 320, 320);
//                    myBitmap8 = mark(myBitmap8);
//                }
//                img8.setImageBitmap(myBitmap8);
                filePath8_64 =  filePath8;
                textImageLat8.setText("Lat : " + GET_LATITUDE);
                textImageLong8.setText("Long : " + GET_LONGITUDE);
                lo8.setVisibility(View.VISIBLE);

            }else{
                filePath8_64="";
            }

            if(filePath9 != null){
                img9.setImageBitmap(imageToString.Base64ToImage(filePath9));
//                myBitmap9 = BitmapFactory.decodeFile(filePath9);
//                if(GET_STATE.equals("local")){
//                    myBitmap9 = getResizedBitmap(myBitmap9, 320, 320);
//                    myBitmap9 = mark(myBitmap9);
//                }
//                img9.setImageBitmap(myBitmap9);
                filePath9_64 =  filePath9;
                textImageLat9.setText("Lat : " + GET_LATITUDE);
                textImageLong9.setText("Long : " + GET_LONGITUDE);
                lo9.setVisibility(View.VISIBLE);
            }else{
                filePath9_64="";
            }

            if(filePath10 != null){
                img10.setImageBitmap(imageToString.Base64ToImage(filePath10));
//                myBitmap10 = BitmapFactory.decodeFile(filePath10);
//
//                if(GET_STATE.equals("local")){
//                    myBitmap10 = getResizedBitmap(myBitmap10, 320, 320);
//                    myBitmap10 = mark(myBitmap10);
//                }
                filePath10_64 =  filePath10;
//                img10.setImageBitmap(myBitmap10);
                textImageLat10.setText("Lat : " + GET_LATITUDE);
                textImageLong10.setText("Long : " + GET_LONGITUDE);
                lo10.setVisibility(View.VISIBLE);
            }else{
                filePath10_64="";
            }
            if(filePath11 != null){
                img11.setImageBitmap(imageToString.Base64ToImage(filePath11));
//                Log.println(Log.DEBUG,"GET_STATE", GET_STATE);
//                Log.println(Log.DEBUG,"filepath detail", filePath11);
//                myBitmap11 = BitmapFactory.decodeFile(filePath11);
//                if(GET_STATE.equals("local")){
//                    myBitmap11 = getResizedBitmap(myBitmap11, 320, 320);
//                    myBitmap11 = mark(myBitmap11);
//                }

                filePath11_64 =  filePath11;
                Log.println(Log.DEBUG,"photo", filePath11_64);
//                img11.setImageBitmap(myBitmap11);
                textImageLat11.setText("Lat : " + GET_LATITUDE);
                textImageLong11.setText("Long : " + GET_LONGITUDE);
                lo11.setVisibility(View.VISIBLE);
            }else{
                filePath11_64="";
            }

            if(filePath12 != null){
                img12.setImageBitmap(imageToString.Base64ToImage(filePath12));
//                myBitmap12 = BitmapFactory.decodeFile(filePath12);
//                if(GET_STATE.equals("local")){
//                    myBitmap12 = getResizedBitmap(myBitmap12, 320, 320);
//                    myBitmap12 = mark(myBitmap12);
//                }
//                img12.setImageBitmap(myBitmap12);
                filePath12_64 =  filePath12;
                textImageLat12.setText("Lat : " + GET_LATITUDE);
                textImageLong12.setText("Long : " + GET_LONGITUDE);
                lo12.setVisibility(View.VISIBLE);
            }else{
                filePath12_64="";
            }

            if(filePath13 != null){
                img13.setImageBitmap(imageToString.Base64ToImage(filePath13));
//                myBitmap13 = BitmapFactory.decodeFile(filePath13);
//                if(GET_STATE.equals("local")){
//                    myBitmap13 = getResizedBitmap(myBitmap13, 320, 320);
//                    myBitmap13 = mark(myBitmap13);
//                }
//                img13.setImageBitmap(myBitmap13);
                filePath13_64 =  filePath13;
                textImageLat13.setText("Lat : " + GET_LATITUDE);
                textImageLong13.setText("Long : " + GET_LONGITUDE);
                lo13.setVisibility(View.VISIBLE);

            }else{
                filePath13_64="";
            }

            if(filePath14 != null){
                img14.setImageBitmap(imageToString.Base64ToImage(filePath14));
//                myBitmap14 = BitmapFactory.decodeFile(filePath14);
//                if(GET_STATE.equals("local")){
//                    myBitmap14 = getResizedBitmap(myBitmap14, 320, 320);
//                    myBitmap14 = mark(myBitmap14);
//                }
//                img14.setImageBitmap(myBitmap14);
                filePath14_64 =  filePath14;
                textImageLat14.setText("Lat : " + GET_LATITUDE);
                textImageLong14.setText("Long : " + GET_LONGITUDE);
                lo14.setVisibility(View.VISIBLE);
            }else{
                filePath14_64="";
            }

            if(filePath15 != null){
                img15.setImageBitmap(imageToString.Base64ToImage(filePath15));
//                myBitmap15 = BitmapFactory.decodeFile(filePath15);
//
//                if(GET_STATE.equals("local")){
//                    myBitmap15 = getResizedBitmap(myBitmap15, 320, 320);
//                    myBitmap15 = mark(myBitmap15);
//                }
                filePath15_64 =  filePath15;
//                img15.setImageBitmap(myBitmap15);
                textImageLat15.setText("Lat : " + GET_LATITUDE);
                textImageLong15.setText("Long : " + GET_LONGITUDE);
                lo15.setVisibility(View.VISIBLE);
            }else{
                filePath15_64="";
            }
        }catch (Exception e){
            Toast.makeText(getActivity().getApplicationContext(), "There was a problem displaying images",
                    Toast.LENGTH_SHORT).show();
        }



        // Inflate the layout for this fragment
        return view;
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public Bitmap mark(Bitmap src) {

        int w = src.getWidth();
        int h = src.getHeight();
        int pw = w - 300;
        int ph = h - 170;
        int pw2 = w - 300;
        int ph2 = h - 150;
        int pw3 = w - 300;
        int ph3 = h - 130;
        int pw4 = w - 300;
        int ph4 = h - 100;
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
        Canvas canvas = new Canvas(result);

        canvas.drawBitmap(src, 0, 0, null);
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);

        //paint.setAlpha(20);
        paint.setTextSize(15);
        //paint.setAntiAlias(true);
        paint.setUnderlineText(false);
//        if(gps.canGetLocation()){
            canvas.drawText("latitude: " + GET_LATITUDE + "/n", pw, ph, paint);
            canvas.drawText("longitude: " + GET_LONGITUDE + "/n", pw2, ph2, paint);
//        }

        canvas.drawText("time: " + new Date(), pw3, ph3, paint);
        canvas.drawText("user: " + db.getUserDetails().get("name"), pw4, ph4, paint);
        canvas.drawText("user: " + db.getUserDetails().get("username"), pw4, ph4, paint);
        return result;
    }

    void updateSiteActivity(){
        if(GET_PERCENTAGE.equals("100")){
            SQLiteDatabase dbs = db.getWritableDatabase();
            dbs.delete("dm_site_activity","id ='" + GET_SITE_ID+"'",null);
            dbs.close();
        }else {

            String query = "SELECT * FROM dm_site_activity where id = '" + GET_SITE_ID + "'";
            SQLiteDatabase dbs = db.getReadableDatabase();
            Cursor cursor = dbs.rawQuery(query, null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                    String queryUpdate = "UPDATE dm_site_activity SET percentage = '"+ GET_PERCENTAGE +"' where id ='"+ GET_SITE_ID +"'";
            }
            cursor.close();
            dbs.close();
        }
    }

    void fillData(String id){

        AndroidNetworking.post(AppConfig.link + "data/activity/log/update")
                .addBodyParameter("user_id", encryptions.getDecrypt(db.getUserDetails().get("uid")))
                .addBodyParameter("id",id)
                .setTag("get data from service")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.optString("status").equals("200")) {
                            Log.println(Log.DEBUG,"RESPONSE", "200");
                            try {
                                JSONArray arr = response.getJSONArray("data");
                                for (int i =0; i<arr.length();i++) {
                                    JSONObject o = arr.getJSONObject(i);
                                    Log.println(Log.DEBUG, "get_id", o.getString("id"));
                                    new_id = o.getString("id");
                                    db.addActivityLog(encryptions.getEncrypt(o.getString("id")),
                                            encryptions.getEncrypt(o.getString("user_id")),
                                            encryptions.getEncrypt(o.getString("site_activity_id")),
                                            encryptions.getEncrypt(o.getString("milestone_id")),
                                            encryptions.getEncrypt(o.getString("site_id")),
                                            encryptions.getEncrypt(o.getString("activity_id")),
                                            encryptions.getEncrypt(o.getString("title")),
                                            encryptions.getEncrypt(o.getString("content")),
                                            encryptions.getEncrypt(o.getString("percentage")),
                                            encryptions.getEncrypt(o.getString("photo")),
                                            encryptions.getEncrypt(o.getString("longitude")),
                                            encryptions.getEncrypt(o.getString("latitude")),
                                            encryptions.getEncrypt(o.getString("date_created")),
                                            encryptions.getEncrypt(o.getString("date_uploaded")),
                                            encryptions.getEncrypt(o.getString("date_modified")),
                                            encryptions.getEncrypt(o.getString("status")),
                                            encryptions.getEncrypt(o.getString("id_modified")),encryptions.getEncrypt("online")
                                    );
                                }

                                AndroidNetworking.post(AppConfig.link + "data/activity/pict/update")
                                        .addBodyParameter("id", new_id)
                                        .setTag("get picture")
                                        .setPriority(Priority.MEDIUM)
                                        .build()
                                        .getAsJSONObject(new JSONObjectRequestListener() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                try{
                                                    JSONArray arr = response.getJSONArray("data");
//                                    progressBar.setMax(arr.length());
                                                    for (int i =0; i<arr.length();i++) {
                                                        JSONObject o = arr.getJSONObject(i);
                                                        final String get_id = encryptions.getEncrypt(o.getString("id"));
                                                        final String get_activity_log_id = encryptions.getEncrypt(o.getString("activity_log_id"));
                                                        final String get_photo = encryptions.getEncrypt(o.getString("photo"));
                                                        final String get_date_created = encryptions.getEncrypt(o.getString("date_created"));
                                                        new CountDownTimer(3000, 1000) {
                                                            public void onTick(long millisUntilFinished) {

                                                                File mediaStorageDir = new File(
                                                                        Environment.getExternalStorageDirectory()
                                                                                + File.separator
                                                                                + "bts_kominfo"
                                                                                + File.separator
                                                                                + "picture"
                                                                                + File.separator
                                                                                + encryptions.getDecrypt(get_activity_log_id)
                                                                );
                                                                if (!mediaStorageDir.exists()) {
                                                                    mediaStorageDir.mkdirs();
                                                                }


                                                                //TODO download picture from server
                                                                AndroidNetworking.download("https://bpppti.teknusa.com/assets/uploads/activity_log/" + encryptions.getDecrypt(get_activity_log_id) + "/" + encryptions.getDecrypt(get_photo), mediaStorageDir.getPath(), encryptions.getDecrypt(get_photo))
                                                                        .setTag("Download Picture")
                                                                        .setPriority(Priority.MEDIUM)
                                                                        .build()
                                                                        .setDownloadProgressListener(new DownloadProgressListener() {
                                                                            @Override
                                                                            public void onProgress(long bytesDownloaded, long totalBytes) {

                                                                            }
                                                                        })
                                                                        .startDownload(new DownloadListener() {
                                                                            @Override
                                                                            public void onDownloadComplete() {
                                                                                File mediaStorageDir = new File(
                                                                                        Environment.getExternalStorageDirectory()
                                                                                                + File.separator
                                                                                                + "bts_kominfo"
                                                                                                + File.separator
                                                                                                + "picture"
                                                                                                + File.separator
                                                                                                + encryptions.getDecrypt(get_activity_log_id)
                                                                                );
                                                                                String filepathEncrypt =mediaStorageDir.getPath() + "/" + encryptions.getDecrypt(get_photo);


                                                                                Bitmap myBitmapEncrypt = BitmapFactory.decodeFile(filepathEncrypt);
                                                                                db.addActivityPict(get_id, get_activity_log_id, get_photo,
                                                                                        get_date_created,imageToString.getStringImage(myBitmapEncrypt));
                                                                                updateSiteActivity();
                                                                                Log.println(Log.DEBUG, "download", "success");
                                                                            }

                                                                            @Override
                                                                            public void onError(ANError anError) {
                                                                                Log.println(Log.DEBUG, "download", "failed");
                                                                            }
                                                                        });

                                                            }

                                                            public void onFinish() {

                                                            }

                                                        }.start();
                                                    }
                                                }catch (JSONException e){
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onError(ANError anError) {

                                            }
                                        });

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Log.println(Log.DEBUG,"RESPONSE", "400");
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    void upload(){
        showProgressBar();
        Log.println(Log.DEBUG,"insert data",encryptions.getDecrypt(db.getUserDetails().get("uid")));
        Log.println(Log.DEBUG,"insert data",GET_MILESTONE_ID);
        Log.println(Log.DEBUG,"insert data",GET_SITE_ID);
        Log.println(Log.DEBUG,"insert data",GET_ACTIVITY_ID);
        Log.println(Log.DEBUG,"insert data",GET_SITE_ACTIVITY_ID);
        Log.println(Log.DEBUG,"insert data",GET_ACTIVITY_NAME);
        Log.println(Log.DEBUG,"insert data",GET_CONTENT);
        Log.println(Log.DEBUG,"insert data",GET_PERCENTAGE);
        Log.println(Log.DEBUG,"insert data",GET_DATE_CREATED);
        Log.println(Log.DEBUG,"insert data",GET_LONGITUDE);
        Log.println(Log.DEBUG,"insert data",GET_LATITUDE);
        Log.println(Log.DEBUG,"insert data",filePath1_64);
        Log.println(Log.DEBUG,"insert data",filePath2_64);
        Log.println(Log.DEBUG,"insert data",filePath3_64);
        Log.println(Log.DEBUG,"insert data",filePath4_64);
        Log.println(Log.DEBUG,"insert data",filePath5_64);
        Log.println(Log.DEBUG,"insert data",filePath6_64);
        Log.println(Log.DEBUG,"insert data",filePath7_64);
        Log.println(Log.DEBUG,"insert data",filePath8_64);
        Log.println(Log.DEBUG,"insert data",filePath9_64);
        Log.println(Log.DEBUG,"insert data",filePath10_64);
        Log.println(Log.DEBUG,"insert data",filePath11_64);
        Log.println(Log.DEBUG,"insert data",filePath12_64);
        Log.println(Log.DEBUG,"insert data",filePath13_64);
        Log.println(Log.DEBUG,"insert data",filePath14_64);
        Log.println(Log.DEBUG,"insert data",filePath15_64);


        AndroidNetworking.post("https://teknusa.com/api/v1/data/activity/upload")
                .addBodyParameter("user_id", encryptions.getDecrypt(db.getUserDetails().get("uid")))
                .addBodyParameter("milestone_id", GET_MILESTONE_ID)
                .addBodyParameter("site_id", GET_SITE_ID)
                .addBodyParameter("activity_id", GET_ACTIVITY_ID)
                .addBodyParameter("site_activity_id",encryptions.getDecrypt( GET_SITE_ACTIVITY_ID))
                .addBodyParameter("title", GET_ACTIVITY_NAME)
                .addBodyParameter("content", GET_CONTENT)
                .addBodyParameter("date_created", GET_DATE_CREATED)
                .addBodyParameter("percentage", GET_PERCENTAGE)
                .addBodyParameter("longitude", GET_LONGITUDE)
                .addBodyParameter("latitude", GET_LATITUDE)
                .addBodyParameter("img1", filePath1_64)
                .addBodyParameter("img2", filePath2_64)
                .addBodyParameter("img3", filePath3_64)
                .addBodyParameter("img4", filePath4_64)
                .addBodyParameter("img5", filePath5_64)
                .addBodyParameter("img6", filePath6_64)
                .addBodyParameter("img7", filePath7_64)
                .addBodyParameter("img8", filePath8_64)
                .addBodyParameter("img9", filePath9_64)
                .addBodyParameter("img10", filePath10_64)
                .addBodyParameter("img11", filePath11_64)
                .addBodyParameter("img12", filePath12_64)
                .addBodyParameter("img13", filePath13_64)
                .addBodyParameter("img14", filePath14_64)
                .addBodyParameter("img15", filePath15_64)
                .setTag("upload")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.println(Log.DEBUG,"response",response.optString("status"));

                        if (response.optString("status").equals("200")){
//                            fillData(response.optString("data"));
                            hideProgressBar();

                            Toast.makeText(getActivity(). getApplicationContext(), "upload sukses",
                                    Toast.LENGTH_SHORT).show();
//                                JSONObject json = response.getJSONObject("data");
                            db.deleteTempLocal(encryptions.getEncrypt(GET_ID));
                            LogFragment dasbboard = new LogFragment();
                            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.frame, dasbboard, "taskList");
                            getActivity().getSupportFragmentManager().popBackStack();
                            getActivity().setTitle("Activity Log");
                            fragmentTransaction.commit();

                        } else {
                            hideProgressBar();
                            Toast.makeText(getActivity().getApplicationContext(), "upload data gagal",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                        Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                        Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        hideProgressBar();
                        Toast.makeText(getActivity().getApplicationContext(), "Ccnnection error",
                                Toast.LENGTH_SHORT).show();
                        // handle error
                    }
                });
    }

    private void showProgressBar(){
        progressBar.setVisibility(View.VISIBLE);
        btnSave.setVisibility(View.GONE);
        btnDelete.setVisibility(View.GONE);

    }

    private void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
        btnSave.setVisibility(View.VISIBLE);
        btnDelete.setVisibility(View.VISIBLE);
    }


}
