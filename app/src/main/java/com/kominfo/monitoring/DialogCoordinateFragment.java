package com.kominfo.monitoring;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogCoordinateFragment extends DialogFragment {


    Button btnTryAgain;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.popup_gps,container,false);
        getDialog().setTitle("Attention");
        btnTryAgain = (Button) view.findViewById(R.id.btnTryAgain);
        btnTryAgain.setText("Try Again");
        btnTryAgain.setHeight(15);
        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }

}
