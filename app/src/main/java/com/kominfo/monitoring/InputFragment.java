package com.kominfo.monitoring;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kominfo.monitoring.helper.CreateUniqId;
import com.kominfo.monitoring.helper.Encryptions;
import com.kominfo.monitoring.helper.GPSTracker;
import com.kominfo.monitoring.helper.ImageToString;
import com.kominfo.monitoring.helper.InputFilterMinMax;
import com.kominfo.monitoring.helper.Permision;
import com.kominfo.monitoring.helper.SQLiteHandler;
import com.kominfo.monitoring.helper.SessionManager;
import com.kominfo.monitoring.helper.SiteCoordinateChecker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class InputFragment extends Fragment {
    final static String SITE_ACTIVITY_ID = "site_activity_id";
    final static  String ACTIVITY_TITLE = "activity_TITLE";
    final static String ACTIVITY_ID = "activity_id";
    final static String PERCENTACE = "percentage";
    final static String DATE_MODIFIED = "date_modified";
    final static String MILESTONE_ID = "milestone_id";
    final static String SITE_ID = "site_id";
    final static String GTA_FLAG = "gta_flag";
    final static String PROJECT_ID = "project_id";
    private static final int PICK_FROM_CAMERA = 2;
    SiteCoordinateChecker scc;
    Boolean cordinateChecker;
    ImageToString imageToString;
    Encryptions encryptions;
    Bitmap myBitmap, myBitmap2, myBitmap3, myBitmap4, myBitmap5, myBitmap6, myBitmap7, myBitmap8, myBitmap9, myBitmap10, myBitmap11, myBitmap12, myBitmap13, myBitmap14, myBitmap15;
    Permision permision;
    SQLiteHandler db;
    LocationManager locationManager;
    SessionManager session;
    String GET_LAT_IMAGE;
    String GET_LONG_IMAGE;
    String GET_LONGITUDE, GET_SITE_LONGITUDE;
    String GET_LATITUDE, GET_SITE_LATITUDE;
    String GET_ACTIVITY_ID;
    String GET_ACTIVITY_NAME;
    String GET_PERCENTAGE;
    String GET_DATE_MODIFIED;
    String GET_MILESTONE_ID;
    String GET_SITE_ACTIVITY_ID;
    String GET_SITE_ID;
    String GET_PROJECT_ID;
    String GET_MILESTONE_NAME;
    String GET_SITE_NAME;
    String GET_UNIQ_ID;
    String GET_PHOTO_NAME;
    String GET_GTA_FLAG;
    String sessionInput;
    String filePath1, filePath2, filePath3, filePath4 , filePath5, filePath6, filePath7, filePath8, filePath9, filePath10, filePath11, filePath12, filePath13, filePath14, filePath15, fotoPath;
    Integer count = 0;
    Uri outputFileUri;
    LinearLayout lo1, lo2, lo3, lo4, lo5, lo6, lo7, lo8, lo9, lo10, lo11, lo12, lo13, lo14, lo15;
    CreateUniqId createUniqId;

    GPSTracker gps;
    private String KEY_IMAGE = "image";
    private String KEY_NAME = "image_name";
    File mediaFile;

    private String KEY_IMAGE_2 = "image_2";
    private String KEY_NAME_2 = "image_name_2";
    File mediaFile2;

    private String KEY_IMAGE_3 = "image_3";
    private String KEY_NAME_3 = "image_name_3";
    File mediaFile3;

    String GET_PETUNJUK_FOTO;

    private String KEY_IMAGE_4 = "image_4";
    private String KEY_NAME_4 = "image_name_4";
    File mediaFile4;

    private String KEY_IMAGE_5 = "image_5";
    private String KEY_NAME_5 = "image_name_5";
    File mediaFile5;

    private String KEY_IMAGE_6 = "image_6";
    private String KEY_NAME_6 = "image_name_6";
    File mediaFile6;

    private String KEY_IMAGE_7 = "image_7";
    private String KEY_NAME_7 = "image_name_7";
    File mediaFile7;

    private String KEY_IMAGE_8 = "image_8";
    private String KEY_NAME_8 = "image_name_8";
    File mediaFile8;

    private String KEY_IMAGE_9 = "image_9";
    private String KEY_NAME_9 = "image_name_9";
    File mediaFile9;

    private String KEY_IMAGE_10 = "image_10";
    private String KEY_NAME_10 = "image_name_10";
    File mediaFile10;

    private String KEY_IMAGE_11 = "image_11";
    private String KEY_NAME_11 = "image_name_11";
    File mediaFile11;

    private String KEY_IMAGE_12 = "image_12";
    private String KEY_NAME_12 = "image_name_12";
    File mediaFile12;

    private String KEY_IMAGE_13 = "image_13";
    private String KEY_NAME_13 = "image_name_13";
    File mediaFile13;

    private String KEY_IMAGE_14 = "image_14";
    private String KEY_NAME_14 = "image_name_14";
    File mediaFile14;

    private String KEY_IMAGE_15 = "image_15";
    private String KEY_NAME_15 = "image_name_15";
    File mediaFile15;




    double radius;
    String f1, f2,f3,f4,f5;
    TextView textTitle, milestone, site, persentase, modified, coordinate, textPhotoUpload, txt_petunuk_foto;
    TextView textImageLat1, textImageLong1,textImageLat2, textImageLong2,textImageLat3, textImageLong3, textImageLat4, textImageLong4,textImageLat5, textImageLong5,textImageLat6, textImageLong6,textImageLat7, textImageLong7,textImageLat8, textImageLong8,textImageLat9, textImageLong9,textImageLat10, textImageLong10,textImageLat11, textImageLong11,textImageLat12, textImageLong12,textImageLat13, textImageLong13,textImageLat14, textImageLong14,textImageLat15, textImageLong15;
    Button deleteImg1,deleteImg2,deleteImg3,deleteImg4,deleteImg5,deleteImg6,deleteImg7,deleteImg8,deleteImg9,deleteImg10,deleteImg11,deleteImg12,deleteImg13,deleteImg14,deleteImg15;
    Integer uploadFoto = 0;
    Button btnSave, btnCancel;
    FloatingActionButton btnTakePicture, btnIssue;
    ImageView img1, img2, img3, img4, img5, img6, img7, img8, img9, img10, img11, img12, img13, img14, img15;
    Integer lastDelete;
    EditText edt_deskripsi, edt_persentase;
    ProgressBar progressTakePicture;
    PopupWindow mPopUpWindow;

    public InputFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input, container, false);
//        final FragmentManager fm = getFragmentManager();
//        final DialogCoordinateFragment n = new DialogCoordinateFragment();
        createUniqId = new CreateUniqId();
        sessionInput = createUniqId.generateUniqId();
        session = new SessionManager(getActivity().getApplicationContext());
        imageToString = new ImageToString();
        encryptions = new Encryptions();
//        n.show(fm,"Attenton");
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            showGPSDisabledAlertToUser();
        }



        edt_deskripsi = (EditText) view.findViewById(R.id.edt_deskripsi);
        edt_persentase = (EditText) view.findViewById(R.id.edt_persentase);
        txt_petunuk_foto = (TextView) view.findViewById(R.id.txt_petunjuk_foto);
        textTitle = (TextView) view.findViewById(R.id.textTask);
        textImageLat1 = (TextView) view.findViewById(R.id.textImageLat);
        textImageLong1 = (TextView) view.findViewById(R.id.textImageLong);
        textImageLat2 = (TextView) view.findViewById(R.id.textImageLat2);
        textImageLong2 = (TextView) view.findViewById(R.id.textImageLong2);
        textImageLat3 = (TextView) view.findViewById(R.id.textImageLat3);
        textImageLong3 = (TextView) view.findViewById(R.id.textImageLong3);
        textImageLat4 = (TextView) view.findViewById(R.id.textImageLat4);
        textImageLong4 = (TextView) view.findViewById(R.id.textImageLong4);
        textImageLat5 = (TextView) view.findViewById(R.id.textImageLat5);
        textImageLong5 = (TextView) view.findViewById(R.id.textImageLong5);
        textImageLat6= (TextView) view.findViewById(R.id.textImageLat6);
        textImageLong6 = (TextView) view.findViewById(R.id.textImageLong6);
        textImageLat7 = (TextView) view.findViewById(R.id.textImageLat7);
        textImageLong7 = (TextView) view.findViewById(R.id.textImageLong7);
        textImageLat8 = (TextView) view.findViewById(R.id.textImageLat8);
        textImageLong8 = (TextView) view.findViewById(R.id.textImageLong8);
        textImageLat9 = (TextView) view.findViewById(R.id.textImageLat9);
        textImageLong9 = (TextView) view.findViewById(R.id.textImageLong9);
        textImageLat10 = (TextView) view.findViewById(R.id.textImageLat10);
        textImageLong10 = (TextView) view.findViewById(R.id.textImageLong10);
        textImageLat11 = (TextView) view.findViewById(R.id.textImageLat11);
        textImageLong11 = (TextView) view.findViewById(R.id.textImageLong11);
        textImageLat12 = (TextView) view.findViewById(R.id.textImageLat12);
        textImageLong12 = (TextView) view.findViewById(R.id.textImageLong12);
        textImageLat13 = (TextView) view.findViewById(R.id.textImageLat13);
        textImageLong13 = (TextView) view.findViewById(R.id.textImageLong13);
        textImageLat14 = (TextView) view.findViewById(R.id.textImageLat14);
        textImageLong14 = (TextView) view.findViewById(R.id.textImageLong14);
        textImageLat15 = (TextView) view.findViewById(R.id.textImageLat15);
        textImageLong15 = (TextView) view.findViewById(R.id.textImageLong15);
        textPhotoUpload = (TextView) view.findViewById(R.id.textUploadFoto);
        progressTakePicture = (ProgressBar) view.findViewById(R.id.progressTakePicture);
        filePath1 = null;
        filePath2 = null;
        filePath3 = null;
        filePath4 = null;
        filePath5 = null;
        filePath6 = null;
        filePath7 = null;
        filePath8 = null;
        filePath9 = null;
        filePath10 = null;
        filePath11 = null;
        filePath12 = null;
        filePath13 = null;
        filePath14 = null;
        filePath15 = null;

        session.setSessionInput(sessionInput);
        deleteImg1 = (Button) view.findViewById(R.id.btnDeteleImg1);
        deleteImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath1 = null;
                lo1.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg2 = (Button) view.findViewById(R.id.btnDeteleImg2);
        deleteImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath2 = null;
                lo2.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg3 = (Button) view.findViewById(R.id.btnDeteleImg3);
        deleteImg3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath3 = null;
                lo3.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg4 = (Button) view.findViewById(R.id.btnDeteleImg4);
        deleteImg4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath4 = null;
                lo4.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg5 = (Button) view.findViewById(R.id.btnDeteleImg5);
        deleteImg5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath5 = null;
                lo5.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg6 = (Button) view.findViewById(R.id.btnDeteleImg6);
        deleteImg6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath6 = null;
                lo6.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg7 = (Button) view.findViewById(R.id.btnDeteleImg7);
        deleteImg7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath7 = null;
                lo7.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg8 = (Button) view.findViewById(R.id.btnDeteleImg8);
        deleteImg8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath8 = null;
                lo8.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg9 = (Button) view.findViewById(R.id.btnDeteleImg9);
        deleteImg9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath9 = null;
                lo9.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg10 = (Button) view.findViewById(R.id.btnDeteleImg10);
        deleteImg10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath10 = null;
                lo10.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg11 = (Button) view.findViewById(R.id.btnDeteleImg11);
        deleteImg11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath11 = null;
                lo11.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg12 = (Button) view.findViewById(R.id.btnDeteleImg12);
        deleteImg12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath12 = null;
                lo12.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg13 = (Button) view.findViewById(R.id.btnDeteleImg13);
        deleteImg13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath13 = null;
                lo13.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg14 = (Button) view.findViewById(R.id.btnDeteleImg14);
        deleteImg14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath14 = null;
                lo14.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        deleteImg15 = (Button) view.findViewById(R.id.btnDeteleImg15);
        deleteImg15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePath1 = null;
                lo1.setVisibility(View.GONE);
                count -= 1;
                textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
            }
        });
        coordinate = (TextView) view.findViewById(R.id.textCoordinate);
        milestone = (TextView) view.findViewById(R.id.textMilestone) ;
        lo1 = (LinearLayout) view.findViewById(R.id.linear1);
        lo2 = (LinearLayout) view.findViewById(R.id.linear2);
        lo3 = (LinearLayout) view.findViewById(R.id.linear3);
        lo4 = (LinearLayout) view.findViewById(R.id.linear4);
        lo5 = (LinearLayout) view.findViewById(R.id.linear5);
        lo6 = (LinearLayout) view.findViewById(R.id.linear6);
        lo7 = (LinearLayout) view.findViewById(R.id.linear7);
        lo8 = (LinearLayout) view.findViewById(R.id.linear8);
        lo9 = (LinearLayout) view.findViewById(R.id.linear9);
        lo10 = (LinearLayout) view.findViewById(R.id.linear10);
        lo11 = (LinearLayout) view.findViewById(R.id.linear11);
        lo12 = (LinearLayout) view.findViewById(R.id.linear12);
        lo13 = (LinearLayout) view.findViewById(R.id.linear13);
        lo14 = (LinearLayout) view.findViewById(R.id.linear14);
        lo15 = (LinearLayout) view.findViewById(R.id.linear15);
        site = (TextView) view.findViewById(R.id.textSite);
        img1 = (ImageView) view.findViewById(R.id.img1);
        img2 = (ImageView) view.findViewById(R.id.img2);
        img3 = (ImageView) view.findViewById(R.id.img3);
        img4 = (ImageView) view.findViewById(R.id.img4);
        img5 = (ImageView) view.findViewById(R.id.img5);
        img6 = (ImageView) view.findViewById(R.id.img6);
        img7 = (ImageView) view.findViewById(R.id.img7);
        img8 = (ImageView) view.findViewById(R.id.img8);
        img9 = (ImageView) view.findViewById(R.id.img9);
        img10 = (ImageView) view.findViewById(R.id.img10);
        img11 = (ImageView) view.findViewById(R.id.img11);
        img12 = (ImageView) view.findViewById(R.id.img12);
        img13 = (ImageView) view.findViewById(R.id.img13);
        img14 = (ImageView) view.findViewById(R.id.img14);
        img15 = (ImageView) view.findViewById(R.id.img15);
        persentase = (TextView) view.findViewById(R.id.textPercentage);
        modified = (TextView) view.findViewById(R.id.textModified);
        btnTakePicture = (FloatingActionButton) view.findViewById(R.id.btnTakePicture);
        btnTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(count <= 15) {
                    try {
                        dispatchTakePictureIntent();
                    } catch (IOException e) {
                    }
//                    takePicture();

                }else{
                    count = 15;
                    Toast.makeText(getActivity().getApplicationContext(), "MAX 15 Photos",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnIssue = (FloatingActionButton) view.findViewById(R.id.btnIssue);
        btnIssue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListIssueFragment issueFragment= new ListIssueFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, issueFragment, "detail");
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().setTitle("Issue");
                fragmentTransaction.commit();
            }
        });


        btnSave = (Button) view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.println(Log.DEBUG,"GTA FLAG", GET_GTA_FLAG);
                if(GET_GTA_FLAG.equals("1")){

                    if(GET_PROJECT_ID.equals("2")){
                        radius= 0.05;
                    }else if(GET_PROJECT_ID.equals("1")){
                        radius= 0.1;
                    }else if(GET_PROJECT_ID.equals("3")){
                        radius= 0.1;
                    }
                    if(pointInCircle(gps.getLatitude(),gps.getLongitude(),radius,Double.valueOf(GET_SITE_LATITUDE),Double.valueOf(GET_SITE_LONGITUDE))){
                        if(edt_deskripsi.getText().length() == 0 || count == 0 ){
                            try {
                                new android.support.v7.app.AlertDialog.Builder(getActivity())
                                        .setTitle(android.R.string.dialog_alert_title)
                                        .setMessage(getString(R.string.please_fill))
                                        .setNegativeButton(android.R.string.ok, null)
                                        .create().show();
                            }catch (Exception e){

                            }
                        }else {
                            saveData();
                        }
                    }else{
                        try{
                            new android.support.v7.app.AlertDialog.Builder(getActivity())
                                    .setTitle("OUT OF RANGE!!")
                                    .setMessage("You cannot save this session")
                                    .setNegativeButton(android.R.string.yes, null)
                                    .create().show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }else {
                    saveData();
                }



            }
        });
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle(android.R.string.dialog_alert_title)
                            .setMessage(getString(R.string.close_task))
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {
                                    try{
                                        String query = "SELECT * FROM dm_temp_input";

                                        SQLiteDatabase dbs = db.getWritableDatabase();
                                        Cursor cursor = dbs.rawQuery(query,null);
                                        cursor.moveToFirst();
                                        if(cursor.getCount()>0){
                                            do{
                                                File photoFile = new File(
                                                        Environment.getExternalStorageDirectory()
                                                                + cursor.getString(cursor.getColumnIndex("photo"))
                                                );

                                                if (photoFile.exists()) {
                                                    photoFile.delete();
                                                }
                                            }while (cursor.moveToNext());
                                        }

                                        TaskDetail taskDetail = new TaskDetail();
                                        Bundle args = new Bundle();
                                        args.putString(TaskDetail.MILESTONE_ID,GET_MILESTONE_ID);
                                        taskDetail.setArguments(args);
                                        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.frame, taskDetail, "detail");
                                        getActivity().getSupportFragmentManager().popBackStack();
                                        getActivity().setTitle(GET_MILESTONE_NAME);
                                        fragmentTransaction.commit();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }


                                }
                            }).create().show();


                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        db = new SQLiteHandler(getActivity().getApplicationContext());
        Bundle args =  getArguments();


            Log.println(Log.DEBUG,"ACTIVITY ID", args.getString(ACTIVITY_ID));
            GET_SITE_ACTIVITY_ID = args.getString(SITE_ACTIVITY_ID);
            GET_ACTIVITY_ID = args.getString(ACTIVITY_ID);
            GET_ACTIVITY_NAME = args.getString(ACTIVITY_TITLE);
            GET_MILESTONE_ID = args.getString(MILESTONE_ID);
            GET_PERCENTAGE = args.getString(PERCENTACE);
            GET_DATE_MODIFIED = args.getString(DATE_MODIFIED);
            GET_SITE_ID = session.isSiteId();
            GET_GTA_FLAG = args.getString(GTA_FLAG);
            GET_PROJECT_ID = encryptions.getDecrypt(args.getString(PROJECT_ID));



        String getPetunjukFoto = "Select petunjuk_foto from dm_activity where id ='"+ GET_ACTIVITY_ID +"'";
        SQLiteDatabase dbPF = db.getReadableDatabase();
        Cursor cursorPF = dbPF.rawQuery(getPetunjukFoto, null);
        cursorPF.moveToFirst();
        if(cursorPF.getCount() > 0){
            GET_PETUNJUK_FOTO = encryptions.getDecrypt(cursorPF.getString( cursorPF.getColumnIndex("petunjuk_foto")));
        }

        txt_petunuk_foto.setText(GET_PETUNJUK_FOTO);

        String queryMilestone = "Select * from dm_milestone where id ='" + GET_MILESTONE_ID + "'";
        Log.println(Log.DEBUG,"query",queryMilestone);
        SQLiteDatabase dbs = db.getReadableDatabase();
        Cursor cursorMilestone = dbs.rawQuery(queryMilestone, null);
        cursorMilestone.moveToFirst();
        if(cursorMilestone.getCount() > 0){
            GET_MILESTONE_NAME = encryptions.getDecrypt(cursorMilestone.getString(cursorMilestone.getColumnIndex("name")));
        }
        cursorMilestone.close();

        String querySite = "Select * from dm_site where id ='" + GET_SITE_ID + "'";
        SQLiteDatabase dbSite = db.getReadableDatabase();
        Cursor cursorSite = dbSite.rawQuery(querySite, null);
        cursorSite.moveToFirst();
        if(cursorSite.getCount() > 0){
            GET_SITE_NAME = encryptions.getDecrypt(cursorSite.getString(cursorSite.getColumnIndex("title")));
            GET_SITE_LATITUDE = encryptions.getDecrypt(cursorSite.getString(cursorSite.getColumnIndex("latitude")));
            if(GET_SITE_LATITUDE.equals("null"))
                GET_SITE_LATITUDE = "0";
            GET_SITE_LONGITUDE =encryptions.getDecrypt( cursorSite.getString(cursorSite.getColumnIndex("longitude")));
            if(GET_SITE_LONGITUDE.equals("null"))
                GET_SITE_LONGITUDE = "0";
        }
        cursorSite.close();

//        Log.println(Log.DEBUG, "GET_ACTIVITY_NAME", GET_ACTIVITY_NAME );
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        permision = new Permision(getActivity());
        db = new SQLiteHandler(getActivity().getApplicationContext());
        scc = new SiteCoordinateChecker();
        getLoc();
        if (GET_LATITUDE.equals("0") || GET_LONGITUDE.equals("0")){
            try{
                new android.support.v7.app.AlertDialog.Builder(getActivity())
                        .setTitle("Attention")
                        .setMessage("Cannot get your coordinates")
                        .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getLoc();
                            }
                        })
                        .setNegativeButton(R.string.back, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try{
                                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                                            .setTitle(android.R.string.dialog_alert_title)
                                            .setMessage(getString(R.string.close_task))
                                            .setNegativeButton(android.R.string.no, null)
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                                public void onClick(DialogInterface arg0, int arg1) {
                                                    try{
                                                        String query = "SELECT * FROM dm_temp_input";

                                                        SQLiteDatabase dbs = db.getWritableDatabase();
                                                        Cursor cursor = dbs.rawQuery(query,null);
                                                        cursor.moveToFirst();
                                                        if(cursor.getCount()>0){
                                                            do{
                                                                File photoFile = new File(
                                                                        Environment.getExternalStorageDirectory()
                                                                                + cursor.getString(cursor.getColumnIndex("photo"))
                                                                );

                                                                if (photoFile.exists()) {
                                                                    photoFile.delete();
                                                                }
                                                            }while (cursor.moveToNext());
                                                        }

                                                        TaskDetail taskDetail = new TaskDetail();
                                                        Bundle args = new Bundle();
                                                        args.putString(TaskDetail.MILESTONE_ID,GET_MILESTONE_ID);
                                                        taskDetail.setArguments(args);
                                                        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                                        fragmentTransaction.replace(R.id.frame, taskDetail, "detail");
                                                        getActivity().getSupportFragmentManager().popBackStack();
                                                        getActivity().setTitle(GET_MILESTONE_NAME);
                                                        fragmentTransaction.commit();
                                                    }catch (Exception e){
                                                        e.printStackTrace();
                                                    }


                                                }
                                            }).create().show();


                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        })
                        .create().show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        Log.println(Log.DEBUG,"CHECK COORDINATE", GET_PROJECT_ID);
        Log.println(Log.DEBUG,"CHECK COORDINATE", String.valueOf(gps.getLongitude()));
        Log.println(Log.DEBUG,"CHECK COORDINATE", String.valueOf(gps.getLatitude()));
        Log.println(Log.DEBUG,"CHECK COORDINATE", GET_SITE_LONGITUDE);
        Log.println(Log.DEBUG,"CHECK COORDINATE", GET_SITE_LATITUDE);
        try{
            this.cordinateChecker = scc.coordinateChecker(GET_PROJECT_ID,gps.getLongitude(),gps.getLatitude(),Double.valueOf(GET_SITE_LONGITUDE),Double.valueOf(GET_SITE_LATITUDE), GET_GTA_FLAG);
            if(!this.cordinateChecker){
                try{
                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                            .setTitle("Information")
                            .setMessage("OUT OF RANGE!!")
                            .setNegativeButton(android.R.string.yes, null)
                            .create().show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            try{
                new android.support.v7.app.AlertDialog.Builder(getActivity())
                        .setTitle("OUT OF RANGE!!")
                        .setMessage(String.valueOf(this.cordinateChecker))
                        .setNegativeButton(android.R.string.yes, null)
                        .create().show();
            }catch (Exception ex){
                ex.printStackTrace();

            }
            e.printStackTrace();
        }






        textTitle.setText(GET_ACTIVITY_NAME);
        coordinate.setText("lat: " + GET_SITE_LATITUDE + " - long: " + GET_SITE_LONGITUDE);
        milestone.setText(GET_MILESTONE_NAME);
        site.setText(GET_SITE_NAME);
        persentase.setText(GET_PERCENTAGE + " %") ;
        edt_persentase.setFilters(new InputFilter[]{new InputFilterMinMax(getActivity().getApplicationContext(), "1", "100")});
        edt_persentase.setText("100");
//        edt_persentase.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if(!b){
//                    Log.println(Log.DEBUG,"lost focus", String.valueOf(edt_persentase.length()));
//                    if(String.valueOf(edt_persentase.length()).equals("0")){
//                        edt_persentase.setText(GET_PERCENTAGE);
//                        Toast toast =  Toast.makeText(getActivity().getApplicationContext(),"Set minimum percentage value to "+ GET_PERCENTAGE,Toast.LENGTH_LONG);
//                        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP,0,0);
//                        toast.show();
//                    }else if(Integer.valueOf(edt_persentase.getText().toString())  < Integer.valueOf(GET_PERCENTAGE)){
//                        edt_persentase.setText(GET_PERCENTAGE);
//                        Toast toast =  Toast.makeText(getActivity().getApplicationContext(),"Set minimum percentage value to "+ GET_PERCENTAGE,Toast.LENGTH_LONG);
//                        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP,0,0);
//                        toast.show();
//                    }
//                }else{
//                    if(String.valueOf(edt_persentase.length()).equals("0")){
//                        edt_persentase.setText("1");
//                    }
//                }
////                if(edt_persentase.length() != 0) {
//
////                }
//            }
//        });
//        edt_persentase.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
        modified.setText(GET_DATE_MODIFIED);
        // Inflate the layout for this fragment
        return view;
    }



    void saveData(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        Log.println(Log.DEBUG, "current date", currentDateandTime);
        String tempKey;
        String tempKeys;
        String parentId;
        String uniqId;
        uniqId = createUniqId.generateUniqId();
        db.addActivityLogFromLocal(encryptions.getEncrypt(uniqId), db.getUserDetails().get("uid"), encryptions.getEncrypt(GET_SITE_ACTIVITY_ID), GET_MILESTONE_ID, GET_SITE_ID, GET_ACTIVITY_ID, encryptions.getEncrypt(GET_ACTIVITY_NAME), encryptions.getEncrypt(edt_deskripsi.getText().toString()), encryptions.getEncrypt(edt_persentase.getText().toString()), encryptions.getEncrypt(""), encryptions.getEncrypt(GET_LONGITUDE), encryptions.getEncrypt(GET_LATITUDE), encryptions.getEncrypt(currentDateandTime), encryptions.getEncrypt(currentDateandTime), encryptions.getEncrypt(currentDateandTime), encryptions.getEncrypt("3"), encryptions.getEncrypt(""), encryptions.getEncrypt("local"));



        if (filePath1 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath1);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath1, encryptions.getEncrypt(currentDateandTime), filePath1);
            Log.println(Log.DEBUG, "filePath1", filePath1);
        }
        if (filePath2 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath2);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath2, encryptions.getEncrypt(currentDateandTime), filePath2);
            Log.println(Log.DEBUG, "filePath2", filePath2);
        }
        if (filePath3 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath3);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath3, encryptions.getEncrypt(currentDateandTime), filePath3);
            Log.println(Log.DEBUG, "filePath3", filePath3);
        }
        if (filePath4 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath4);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath4, encryptions.getEncrypt(currentDateandTime), filePath4);
            Log.println(Log.DEBUG, "filePath4", filePath4);
        }
        if (filePath5 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath5);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath5, encryptions.getEncrypt(currentDateandTime), filePath5);
            Log.println(Log.DEBUG, "filePath5", filePath5);
        }
        if (filePath6 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath6);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath6, encryptions.getEncrypt(currentDateandTime), filePath6);
            Log.println(Log.DEBUG, "filePath1", filePath6);
        }
        if (filePath7 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath7);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath7, encryptions.getEncrypt(currentDateandTime), filePath7);
            Log.println(Log.DEBUG, "filePath2", filePath7);
        }
        if (filePath8 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath8);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath8, encryptions.getEncrypt(currentDateandTime), filePath8);
            Log.println(Log.DEBUG, "filePath3", filePath8);
        }
        if (filePath9 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath9);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath9, encryptions.getEncrypt(currentDateandTime), filePath9);
            Log.println(Log.DEBUG, "filePath4", filePath9);
        }
        if (filePath10 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath10);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath10, encryptions.getEncrypt(currentDateandTime), filePath10);
            Log.println(Log.DEBUG, "filePath5", filePath10);
        }
        if (filePath11 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath11);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath11, encryptions.getEncrypt(currentDateandTime), filePath11);
            Log.println(Log.DEBUG, "filePath1", filePath11);
        }
        if (filePath12 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath12);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath12, encryptions.getEncrypt(currentDateandTime), filePath12);
            Log.println(Log.DEBUG, "filePath2", filePath12);
        }
        if (filePath13 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath13);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath13, encryptions.getEncrypt(currentDateandTime), filePath13);
            Log.println(Log.DEBUG, "filePath3", filePath13);
        }
        if (filePath14 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath14);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath14, encryptions.getEncrypt(currentDateandTime), filePath14);
            Log.println(Log.DEBUG, "filePath4", filePath14);
        }
        if (filePath15 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath15);
            db.addActivityPictFromLocal(encryptions.getEncrypt(uniqId), filePath15, encryptions.getEncrypt(currentDateandTime),filePath15);
            Log.println(Log.DEBUG, "filePath5", filePath15);
        }


        db.addActivityLogLocal(encryptions.getEncrypt(uniqId),db.getUserDetails().get("uid"), encryptions.getEncrypt(GET_SITE_ACTIVITY_ID), GET_MILESTONE_ID, GET_SITE_ID, GET_ACTIVITY_ID, encryptions.getEncrypt(GET_ACTIVITY_NAME), encryptions.getEncrypt(edt_deskripsi.getText().toString()), encryptions.getEncrypt(edt_persentase.getText().toString()), encryptions.getEncrypt(""), encryptions.getEncrypt(GET_LONGITUDE), encryptions.getEncrypt(GET_LATITUDE), encryptions.getEncrypt(currentDateandTime), encryptions.getEncrypt(currentDateandTime), encryptions.getEncrypt(currentDateandTime), encryptions.getEncrypt("3"), encryptions.getEncrypt(""), encryptions.getEncrypt(uniqId));


        if (filePath1 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath1);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath1, encryptions.getEncrypt(currentDateandTime), filePath1);
        }
        if (filePath2 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath2);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath2, encryptions.getEncrypt(currentDateandTime), filePath2);
        }
        if (filePath3 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath3);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath3, encryptions.getEncrypt(currentDateandTime), filePath3);
        }
        if (filePath4 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath4);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath4, encryptions.getEncrypt(currentDateandTime), filePath4);
        }
        if (filePath5 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath5);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath5, encryptions.getEncrypt(currentDateandTime), filePath5);
        }
        if (filePath6 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath6);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath6, encryptions.getEncrypt(currentDateandTime), filePath6);
        }
        if (filePath7 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath7);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath7, encryptions.getEncrypt(currentDateandTime), filePath7);
        }
        if (filePath8 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath8);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath8, encryptions.getEncrypt(currentDateandTime), filePath8);
        }
        if (filePath9 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath9);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath9, encryptions.getEncrypt(currentDateandTime), filePath9);
        }
        if (filePath10 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath10);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath10, encryptions.getEncrypt(currentDateandTime), filePath10);
        }
        if (filePath11 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath11);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath11, encryptions.getEncrypt(currentDateandTime),filePath11);
        }
        if (filePath12 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath12);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath12, encryptions.getEncrypt(currentDateandTime), filePath12);
        }
        if (filePath13 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath13);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath13, encryptions.getEncrypt(currentDateandTime), filePath13);
        }
        if (filePath14 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath14);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath14, encryptions.getEncrypt(currentDateandTime), filePath14);
        }
        if (filePath15 != null) {
//                            myBitmap = BitmapFactory.decodeFile(filePath15);
            db.addActivityPictLocal(encryptions.getEncrypt(uniqId), filePath15, encryptions.getEncrypt(currentDateandTime), filePath15);
        }

        String queryActLocal = "SELECT * FROM dm_activity_log where id ='"+ encryptions.getEncrypt(uniqId) +"'";
        SQLiteDatabase dbActLocal = db.getReadableDatabase();

        Cursor cursorActLocal = dbActLocal.rawQuery(queryActLocal, null);
        cursorActLocal.moveToFirst();
        if(cursorActLocal.getCount() > 0){
            DetailActivityLogFragment detailActivityLogFragment = new DetailActivityLogFragment();
            Bundle args = new Bundle();
            args.putString(DetailActivityLogFragment.ID,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("id"))));
            args.putString(DetailActivityLogFragment.ACTIVITY_TITLE,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("title"))));
            args.putString(DetailActivityLogFragment.ACTIVITY_ID,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("activity_id"))));
            args.putString(DetailActivityLogFragment.SITE_ACTIVITY_ID,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("site_activity_id"))));
            args.putString(DetailActivityLogFragment.PERCENTACE,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("percentage"))));
            args.putString(DetailActivityLogFragment.DATE_MODIFIED,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("date_modified"))));
            args.putString(DetailActivityLogFragment.DATE_CREATED,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("date_created"))));
            args.putString(DetailActivityLogFragment.MILESTONE_ID,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("milestone_id"))));
            args.putString(DetailActivityLogFragment.SITE_ID,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("site_id"))));
            args.putString(DetailActivityLogFragment.LATITUDE,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("latitude"))));
            args.putString(DetailActivityLogFragment.LONGITUDE,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("longitude"))));
            args.putString(DetailActivityLogFragment.CONTENT,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("content"))));
            args.putString(DetailActivityLogFragment.STATE,encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("state"))));

            detailActivityLogFragment.setArguments(args);
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, detailActivityLogFragment, "ActivityLogdetail");
            getActivity().getSupportFragmentManager().popBackStack();
            getActivity().setTitle("Detail");
            fragmentTransaction.commit();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(
                Environment.getExternalStorageDirectory()
                        + File.separator
                        + "bts_kominfo"
                        + File.separator
                        + "picture"
        );

        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        fotoPath = "file:" + image.getAbsolutePath();
        if (filePath1 == null) {
            f1 = "1";
            filePath1 = "file:" + image.getAbsolutePath();
        }else if(filePath2 == null){
            f1 = "2";
            filePath2 = "file:" + image.getAbsolutePath();
        }else if(filePath3 == null){
            f1 = "3";
            filePath3 = "file:" + image.getAbsolutePath();
        }else if(filePath4 == null){
            f1 = "4";
            filePath4 = "file:" + image.getAbsolutePath();
        }else if(filePath5 == null){
            f1 = "5";
            filePath5 = "file:" + image.getAbsolutePath();
        }else if (filePath6 == null) {
            f1 = "6";
            filePath6 = "file:" + image.getAbsolutePath();
        }else if(filePath7 == null){
            f1 = "7";
            filePath7 = "file:" + image.getAbsolutePath();
        }else if(filePath8 == null){
            f1 = "8";
            filePath8 = "file:" + image.getAbsolutePath();
        }else if(filePath9 == null){
            f1 = "9";
            filePath9 = "file:" + image.getAbsolutePath();
        }else if(filePath10 == null){
            f1 = "10";
            filePath10 = "file:" + image.getAbsolutePath();
        }else if (filePath11 == null) {
            f1 = "11";
            filePath11 = "file:" + image.getAbsolutePath();
        }else if(filePath12 == null){
            f1 = "12";
            filePath12 = "file:" + image.getAbsolutePath();
        }else if(filePath13 == null){
            f1 = "13";
            filePath13 = "file:" + image.getAbsolutePath();
        }else if(filePath14 == null){
            f1 = "14";
            filePath14 = "file:" + image.getAbsolutePath();
        }else if(filePath15 == null){
            f1 = "15";
            filePath15 = "file:" + image.getAbsolutePath();
        }
        return image;
    }

    boolean pointInCircle(double lat0, double lon0, double r, double lat, double lon) {
        double C = 40075.04, A = 360*r/C, B = A/Math.cos(Math.toRadians(lat0));
        return Math.pow((lat-lat0)/A, 2) + Math.pow((lon-lon0)/B, 2) < 1;
    }

    private void dispatchTakePictureIntent() throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                return;
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Log.println(Log.DEBUG,"photoFIle", photoFile.getPath());
                Uri photoURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName()+ ".my.package.name.provider",photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);

            }
        }
    }

    void takePicture() {

            if (!permision.checkPermissionForExternalStorage()) {
                permision.requestPermissionForExternalStorage();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File mediaStorageDir = new File(
                        Environment.getExternalStorageDirectory()
                                + File.separator
                                + "bts_kominfo"
                                + File.separator
                                + "picture"
                );

                if (!mediaStorageDir.exists()) {
                    mediaStorageDir.mkdirs();
                }

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
                try {
                    if (filePath1 == null) {
                        f1 = "1";
                        mediaFile = File.createTempFile(
                                "IMG_" + timeStamp + "_" + count,
                                ".jpg",
                                mediaStorageDir
                        );
                        outputFileUri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName()+ ".my.package.name.provider",mediaFile);
//                        outputFileUri = Uri.fromFile(mediaFile);
                        filePath1 = outputFileUri.getPath();
                    } else if (filePath2 == null) {
                        f1 = "2";
                        mediaFile2 = File.createTempFile(
                                "IMG_" + timeStamp + "_" + count + "_",
                                ".jpg",
                                mediaStorageDir
                        );
                        outputFileUri = Uri.fromFile(mediaFile2);
                       filePath2 = outputFileUri.getPath();
                    } else if (filePath3 == null) {
                        f1="3";
                        mediaFile3 = File.createTempFile(
                                "IMG_" + timeStamp + "_" + count,
                                ".jpg",
                                mediaStorageDir
                        );
                        outputFileUri = Uri.fromFile(mediaFile3);
                        filePath3 = outputFileUri.getPath();
                    } else if (filePath4 == null) {
                        f1 = "4";
                        mediaFile4 = File.createTempFile(
                                "IMG_" + timeStamp + "_" + count,
                                ".jpg",
                                mediaStorageDir
                        );
                        outputFileUri = Uri.fromFile(mediaFile4);
                        filePath4 = outputFileUri.getPath();
                    } else {
                        f1 = "5";
                        mediaFile5 = File.createTempFile(
                                "IMG_" + timeStamp + "_" + count,
                                ".jpg",
                                mediaStorageDir
                        );
                        outputFileUri = Uri.fromFile(mediaFile5);
                        filePath5 = outputFileUri.getPath();
                    }

                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

    }


    private void showGPSDisabledAlertToUser() {
        try{
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setTitle("Attention")
                    .setMessage("GPS is required in your device. Would you like to enable it?")
                    .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(callGPSSettingIntent);
                        }
                    })
                    .create().show();
        }catch (Exception e){
            e.printStackTrace();
        }


//
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity().getApplicationContext());
//        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
//                .setCancelable(false)
//                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                        startActivity(callGPSSettingIntent);
//                    }
//                });
//        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.cancel();
//            }
//        });
//        AlertDialog alert = alertDialogBuilder.create();
//        alert.show();
    }

    public void getLoc(){
        if(!permision.checkPermissionForFineLocation()){
            permision.requestPermissionForFineLocation();
        }else {
            if(!permision.checkPermissionForCoarseLocation()){
                permision.requestPermissionForCoarseLocation();
            }else{
                gps = new GPSTracker(getActivity());

                if(gps.canGetLocation()){
                    Double latitude = gps.getLatitude();
                    Double longitude = gps.getLongitude();
                    Location user = new Location("user");
                    user.setLongitude(longitude);
                    user.setLatitude(latitude);

                    GET_LATITUDE = Double.toString(latitude);
                    GET_LONGITUDE = Double.toString(longitude);


                    Log.println(Log.INFO, "latitude", GET_LATITUDE);
                    Log.println(Log.INFO, "logintude", GET_LONGITUDE);
                }else{


                    showGPSDisabledAlertToUser();
                }
            }
        }

    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public Bitmap mark(Bitmap src) {

        int w = src.getWidth();
        int h = src.getHeight();
        int pw = w - 300;
        int ph = h - 170;
        int pw2 = w - 300;
        int ph2 = h - 150;
        int pw3 = w - 300;
        int ph3 = h - 130;
        int pw4 = w - 300;
        int ph4 = h - 110;
        int pw5 = w - 300;
        int ph5 = h - 90;
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
        Canvas canvas = new Canvas(result);

        canvas.drawBitmap(src, 0, 0, null);
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);

        //paint.setAlpha(20);
        paint.setTextSize(15);
        //paint.setAntiAlias(true);
        paint.setUnderlineText(false);
        if(gps.canGetLocation()){
            canvas.drawText("latitude: " + GET_LATITUDE , pw, ph, paint);
            canvas.drawText("longitude: " + GET_LONGITUDE , pw2, ph2, paint);
        }

        canvas.drawText("time: " + new Date(), pw3, ph3, paint);
        canvas.drawText("name: " + db.getUserDetails().get("name") , pw4, ph4, paint);
        canvas.drawText("user: " + db.getUserDetails().get("username"), pw5, ph5, paint);
        return result;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            // Show the thumbnail on ImageView

                Uri imageUri = Uri.parse(fotoPath);
                Log.println(Log.DEBUG,"photo",imageUri.getPath());
            Log.println(Log.DEBUG,"photo",count.toString());
                File file = new File(imageUri.getPath());
                    try {
                        InputStream ims = new FileInputStream(file);
                        if(f1.equals("1")){
                            filePath1 = imageUri.getPath();
                            myBitmap = BitmapFactory.decodeStream(ims);
                            myBitmap = getResizedBitmap(myBitmap, 520, 520);
                            myBitmap = mark(myBitmap);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap));
                            filePath1 = imageToString.getStringImage(myBitmap);
                            img1.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap)));
                            textImageLat1.setText("Lat : " + gps.getLatitude());
                            textImageLong1.setText("Long : " + gps.getLongitude());
                            lo1.setVisibility(View.VISIBLE);
                            count += 1;
                        }else if(f1.equals("2")){
                            filePath2 = imageUri.getPath();
                            myBitmap2 = BitmapFactory.decodeStream(ims);
                            myBitmap2 = getResizedBitmap(myBitmap2, 520, 520);
                            myBitmap2 = mark(myBitmap2);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap2));
                            filePath2 = imageToString.getStringImage(myBitmap2);
                            img2.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap2)));
                            textImageLat2.setText("Lat : " + gps.getLatitude());
                            textImageLong2.setText("Long : " + gps.getLongitude());
                            lo2.setVisibility(View.VISIBLE);
                            count += 1;
                        }else if(f1.equals("3")){
                            filePath3 = imageUri.getPath();
                            myBitmap3 = BitmapFactory.decodeStream(ims);
                            myBitmap3 = getResizedBitmap(myBitmap3, 520, 520);
                            myBitmap3 = mark(myBitmap3);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap3));
                            filePath3 = imageToString.getStringImage(myBitmap3);
                            img3.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap2)));
                            textImageLat3.setText("Lat : " + gps.getLatitude());
                            textImageLong3.setText("Long : " + gps.getLongitude());
                            lo3.setVisibility(View.VISIBLE);
                            count += 1;
                        }else if(f1.equals("4")){
                            filePath4 = imageUri.getPath();
                            myBitmap4 = BitmapFactory.decodeStream(ims);
                            myBitmap4 = getResizedBitmap(myBitmap4, 520, 520);
                            myBitmap4 = mark(myBitmap4);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap4));
                            filePath4 = imageToString.getStringImage(myBitmap4);
                            img4.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap4)));
                            textImageLat4.setText("Lat : " + gps.getLatitude());
                            textImageLong4.setText("Long : " + gps.getLongitude());
                            lo4.setVisibility(View.VISIBLE);

                            count += 1;
                        }else if(f1.equals("5")){
                            filePath5 = imageUri.getPath();
                            myBitmap5 = BitmapFactory.decodeStream(ims);
                            myBitmap5 = getResizedBitmap(myBitmap5, 520, 520);
                            myBitmap5 = mark(myBitmap5);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap5));
                            filePath5 = imageToString.getStringImage(myBitmap5);
                            img5.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap5)));
                            textImageLat5.setText("Lat : " + gps.getLatitude());
                            textImageLong5.setText("Long : " + gps.getLongitude());
                            lo5.setVisibility(View.VISIBLE);
                            count += 1;
                        }else if(f1.equals("6")){
                            filePath6 = imageUri.getPath();
                            myBitmap6 = BitmapFactory.decodeStream(ims);
                            myBitmap6 = getResizedBitmap(myBitmap6, 520, 520);
                            myBitmap6 = mark(myBitmap6);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap6));
                            filePath6 = imageToString.getStringImage(myBitmap6);
                            img6.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap6)));
                            textImageLat6.setText("Lat : " + gps.getLatitude());
                            textImageLong6.setText("Long : " + gps.getLongitude());
                            lo6.setVisibility(View.VISIBLE);
                            count += 1;
                        }else if(f1.equals("7")){
                            filePath7 = imageUri.getPath();
                            myBitmap7 = BitmapFactory.decodeStream(ims);
                            myBitmap7 = getResizedBitmap(myBitmap7, 520, 520);
                            myBitmap7 = mark(myBitmap7);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap7));
                            filePath7 = imageToString.getStringImage(myBitmap7);
                            img7.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap7)));
                            textImageLat7.setText("Lat : " + gps.getLatitude());
                            textImageLong7.setText("Long : " + gps.getLongitude());
                            lo7.setVisibility(View.VISIBLE);
                            count += 1;
                        }else if(f1.equals("8")){
                            filePath8 = imageUri.getPath();
                            myBitmap8 = BitmapFactory.decodeStream(ims);
                            myBitmap8 = getResizedBitmap(myBitmap8, 520, 520);
                            myBitmap8 = mark(myBitmap8);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap8));
                            filePath8 = imageToString.getStringImage(myBitmap8);
                            img8.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap8)));
                            textImageLat8.setText("Lat : " + gps.getLatitude());
                            textImageLong8.setText("Long : " + gps.getLongitude());
                            lo8.setVisibility(View.VISIBLE);
                            count += 1;
                        }else if(f1.equals("9")){
                            filePath9 = imageUri.getPath();
                            myBitmap9 = BitmapFactory.decodeStream(ims);
                            myBitmap9 = getResizedBitmap(myBitmap9, 520, 520);
                            myBitmap9 = mark(myBitmap9);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap9));
                            filePath9 = imageToString.getStringImage(myBitmap9);
                            img9.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap9)));
                            textImageLat9.setText("Lat : " + gps.getLatitude());
                            textImageLong9.setText("Long : " + gps.getLongitude());
                            lo9.setVisibility(View.VISIBLE);

                            count += 1;
                        }else if(f1.equals("10")){
                            filePath10 = imageUri.getPath();
                            myBitmap10 = BitmapFactory.decodeStream(ims);
                            myBitmap10 = getResizedBitmap(myBitmap10, 520, 520);
                            myBitmap10 = mark(myBitmap10);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap10));
                            filePath10 = imageToString.getStringImage(myBitmap10);
                            img10.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap10)));
                            textImageLat10.setText("Lat : " + gps.getLatitude());
                            textImageLong10.setText("Long : " + gps.getLongitude());
                            lo10.setVisibility(View.VISIBLE);
                            count += 1;
                        }if(f1.equals("11")){
                            filePath11 = imageUri.getPath();
                            myBitmap11 = BitmapFactory.decodeStream(ims);
                            myBitmap11 = getResizedBitmap(myBitmap11, 520, 520);
                            myBitmap11 = mark(myBitmap11);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap11));
                            filePath11 = imageToString.getStringImage(myBitmap11);
                            img11.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap11)));
                            textImageLat11.setText("Lat : " + gps.getLatitude());
                            textImageLong11.setText("Long : " + gps.getLongitude());
                            lo11.setVisibility(View.VISIBLE);
                            count += 1;
                        }else if(f1.equals("12")){
                            filePath12 = imageUri.getPath();
                            myBitmap12 = BitmapFactory.decodeStream(ims);
                            myBitmap12 = getResizedBitmap(myBitmap12, 520, 520);
                            myBitmap12 = mark(myBitmap12);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap12));
                            filePath12 = imageToString.getStringImage(myBitmap12);
                            img12.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap12)));
                            textImageLat12.setText("Lat : " + gps.getLatitude());
                            textImageLong12.setText("Long : " + gps.getLongitude());
                            lo12.setVisibility(View.VISIBLE);
                            count += 1;
                        }else if(f1.equals("13")){
                            filePath13 = imageUri.getPath();
                            myBitmap13 = BitmapFactory.decodeStream(ims);
                            myBitmap13 = getResizedBitmap(myBitmap13, 520, 520);
                            myBitmap13 = mark(myBitmap13);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap13));
                            filePath13 = imageToString.getStringImage(myBitmap13);
                            img13.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap13)));
                            textImageLat13.setText("Lat : " + gps.getLatitude());
                            textImageLong13.setText("Long : " + gps.getLongitude());
                            lo13.setVisibility(View.VISIBLE);
                            count += 1;
                        }else if(f1.equals("14")){
                            filePath14 = imageUri.getPath();
                            myBitmap14 = BitmapFactory.decodeStream(ims);
                            myBitmap14 = getResizedBitmap(myBitmap14, 520, 520);
                            myBitmap14 = mark(myBitmap14);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap14));
                            filePath14 = imageToString.getStringImage(myBitmap14);
                            img14.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap14)));
                            textImageLat14.setText("Lat : " + gps.getLatitude());
                            textImageLong14.setText("Long : " + gps.getLongitude());
                            lo14.setVisibility(View.VISIBLE);

                            count += 1;
                        }else if(f1.equals("15")){
                            filePath15 = imageUri.getPath();
                            myBitmap15 = BitmapFactory.decodeStream(ims);
                            myBitmap15 = getResizedBitmap(myBitmap15, 520, 520);
                            myBitmap15 = mark(myBitmap15);
                            db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap15));
                            filePath15 = imageToString.getStringImage(myBitmap15);
                            img15.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap15)));
                            textImageLat15.setText("Lat : " + gps.getLatitude());
                            textImageLong15.setText("Long : " + gps.getLongitude());
                            lo15.setVisibility(View.VISIBLE);
                            count += 1;
                        }

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    boolean result = file.delete();
                    textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
                    MediaScannerConnection.scanFile(getActivity(),
                            new String[]{imageUri.getPath()}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                public void onScanCompleted(String path, Uri uri) {
                                }
                            });






            // ScanFile so it will be appeared on Gallery

        }
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        Log.println(Log.DEBUG, "resultcode", String.valueOf(resultCode));
//        if (resultCode == -1) {
//
//            new CountDownTimer(3000, 1000) {
//                public void onTick(long millisUntilFinished) {
//                    btnTakePicture.setVisibility(View.GONE);
//                    progressTakePicture.setVisibility(View.VISIBLE);
//                    if (f1 == "1"){
//                        Uri imageUri = Uri.parse(filePath1);
//                        File file = new File(imageUri.getPath());
//                        try{
//                            InputStream ims = new FileInputStream(file);
//                            myBitmap = BitmapFactory.decodeStream(ims);
//                            myBitmap = getResizedBitmap(myBitmap, 520, 520);
//                            myBitmap = mark(myBitmap);
//                            db.addInputTemp(imageUri.getPath());
//                            img1.setImageBitmap(BitmapFactory.decodeStream(ims));
//                        }catch (FileNotFoundException e){
//                            return;
//                        }
//
//
//                    }
//                    else if (f1 == "2"){
//                        myBitmap2 = BitmapFactory.decodeFile(filePath2);
//                        myBitmap2 = getResizedBitmap(myBitmap2, 520, 520);
//                        myBitmap2 = mark(myBitmap2);
//
//                    }
//                    else if (f1 == "3"){
//                        myBitmap3 = BitmapFactory.decodeFile(filePath3);
//                        myBitmap3 = getResizedBitmap(myBitmap3, 520, 520);
//                        myBitmap3 = mark(myBitmap3);
//
//                    }
//                    else if(f1 == "4") {
//                        myBitmap4 = BitmapFactory.decodeFile(filePath4);
//                        myBitmap4 = getResizedBitmap(myBitmap4, 520, 520);
//                        myBitmap4 = mark(myBitmap4);
//
//                    }
//                    else if(f1 == "5") {
//                        myBitmap5 = BitmapFactory.decodeFile(filePath5);
//                        myBitmap5 = getResizedBitmap(myBitmap5, 520, 520);
//                        myBitmap5 = mark(myBitmap5);
//
//                    }
//                }
//
//                public void onFinish() {
//                    btnTakePicture.setVisibility(View.VISIBLE);
//                    progressTakePicture.setVisibility(View.GONE);
//                    if (f1 == "1"){
//
//                        textImageLat1.setText("Lat : " + GET_LATITUDE);
//                        textImageLong1.setText("Long : " + GET_LONGITUDE);
//                        lo1.setVisibility(View.VISIBLE);
//                    }
//                    else if (f1 == "2"){
//                        db.addInputTemp(filePath2);
//                        img2.setImageBitmap(myBitmap2);
//                        textImageLat2.setText("Lat : " + GET_LATITUDE);
//                        textImageLong2.setText("Long : " + GET_LONGITUDE);
//                        lo2.setVisibility(View.VISIBLE);
//                    }
//                    else if (f1 == "3"){
//                        db.addInputTemp(filePath3);
//                        img3.setImageBitmap(myBitmap3);
//                        textImageLat3.setText("Lat : " + GET_LATITUDE);
//                        textImageLong3.setText("Long : " + GET_LONGITUDE);
//                        lo3.setVisibility(View.VISIBLE);
//                    }
//                    else if(f1 == "4") {
//                        db.addInputTemp(filePath4);
//                        img4.setImageBitmap(myBitmap4);
//                        textImageLat4.setText("Lat : " + GET_LATITUDE);
//                        textImageLong4.setText("Long : " + GET_LONGITUDE);
//                        lo4.setVisibility(View.VISIBLE);
//                    }
//                    else if(f1 == "5") {
//                        db.addInputTemp(filePath5);
//                        img5.setImageBitmap(myBitmap5);
//                        textImageLat5.setText("Lat : " + GET_LATITUDE);
//                        textImageLong5.setText("Long : " + GET_LONGITUDE);
//                        lo5.setVisibility(View.VISIBLE);
//                    }
//                }
//            }.start();
//
//            count += 1;
//
//            textPhotoUpload.setText("Upload Foto ("+ String.valueOf(count)+"/15)");
//
//        }
//
//
//    }


}
