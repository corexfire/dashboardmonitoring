package com.kominfo.monitoring;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.kominfo.monitoring.config.AppConfig;
import com.kominfo.monitoring.helper.CheckConnection;
import com.kominfo.monitoring.helper.Encryptions;
import com.kominfo.monitoring.helper.GPSTracker;
import com.kominfo.monitoring.helper.ImageToString;
import com.kominfo.monitoring.helper.Permision;
import com.kominfo.monitoring.helper.SQLiteHandler;
import com.kominfo.monitoring.helper.SessionManager;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class IssueFragment extends Fragment {
    final static  String ISSUE_ID = "issue_id";
    private SQLiteHandler db;
    Permision permision;
    GPSTracker gps;
    ImageToString imageToString;
    String sessionInput;
    SessionManager session;
    String filePath1, filePath2, filePath3, filePath4 , filePath5, fotoPath, f1;
    TextView txtSiteName, txtMilestoneName, txtActivityName,textPhotoUpload;
    EditText edtTitle, edtContent;
    MaterialSpinner spinner;
    String selectedItem;
    String img1String, img2String, img3String, img4String, img5String;
    Button btnSave, btnBack;
    FloatingActionButton btnTakePicture;
    ImageView img1, img2, img3, img4, img5;
    Encryptions encryptions;
    CheckConnection checkConnection;
    String GET_LATITUDE, GET_LONGITUDE;
    final static int MAX_TAKE_PICTURE = 5;
    Integer takePicture = 0;
    private static final int PICK_FROM_CAMERA = 2;
    Bitmap myBitmap, myBitmap2, myBitmap3, myBitmap4, myBitmap5;
    public IssueFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_issue, container, false);
        db = new SQLiteHandler(getActivity().getApplicationContext());
        btnTakePicture = (FloatingActionButton) view.findViewById(R.id.btnTakePicture);
        permision = new Permision(getActivity());
        textPhotoUpload = (TextView) view.findViewById(R.id.textPhotoUpload);
        imageToString = new ImageToString();

        gps = new GPSTracker(getActivity().getApplicationContext());
        getLoc();
        img1 = (ImageView) view.findViewById(R.id.img1);
        img2 = (ImageView) view.findViewById(R.id.img2);
        img3 = (ImageView) view.findViewById(R.id.img3);
        img4 = (ImageView) view.findViewById(R.id.img4);
        img5 = (ImageView) view.findViewById(R.id.img5);
        encryptions = new Encryptions();
        checkConnection = new CheckConnection();
        session = new SessionManager(getActivity().getApplicationContext());
        txtSiteName = (TextView) view.findViewById(R.id.txtSiteName);
        txtMilestoneName = (TextView) view.findViewById(R.id.txtMilestoneName);
        txtActivityName = (TextView) view.findViewById(R.id.txtActivityName);
        edtTitle = (EditText) view.findViewById(R.id.txtTittle);
        edtContent = (EditText) view.findViewById(R.id.txtContent);
//        edtContent.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                view.getParent().requestDisallowInterceptTouchEvent(true);
//                switch (motionEvent.getAction() & MotionEvent.ACTION_MASK){
//                    case MotionEvent.ACTION_UP:
//                        view.getParent().requestDisallowInterceptTouchEvent(false);
//                        return true;
//
//                }
//                return false;
//
//            }
//        });

        btnTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(takePicture <= MAX_TAKE_PICTURE) {
                    try {
                        dispatchTakePictureIntent();
                    } catch (IOException e) {
                    }
//                    takePicture();

                }else{
                    takePicture = 5;
                    Toast.makeText(getActivity().getApplicationContext(), "MAX 5 Photos",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnBack = (Button) view.findViewById(R.id.btnBack);
        btnSave = (Button) view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.addIssue(db.getUserDetails().get("uid"),session.isSiteId(),session.isMilestoneId(),session.isActivityId(),encryptions.getEncrypt(selectedItem),encryptions.getEncrypt( edtTitle.getText().toString()),encryptions.getEncrypt( edtContent.getText().toString()));
                db.addIssueLocal(db.getUserDetails().get("uid"),session.isSiteId(),session.isMilestoneId(),session.isActivityId(),encryptions.getEncrypt(selectedItem),encryptions.getEncrypt( edtTitle.getText().toString()),encryptions.getEncrypt( edtContent.getText().toString()));
                if(checkConnection.isInternetAvailable(getContext())){
                    String status;
                    if(selectedItem.equals("Critical"))
                        status = "1";
                    else
                        status ="0";
                    AndroidNetworking.post(AppConfig.link + "data/issue")
                            .addBodyParameter("user_id", encryptions.getDecrypt(db.getUserDetails().get("uid")))
                            .addBodyParameter("site_id", encryptions.getDecrypt(session.isSiteId()))
                            .addBodyParameter("milestone_id",encryptions.getDecrypt(session.isMilestoneId()))
                            .addBodyParameter("activity_id",encryptions.getDecrypt(session.isActivityId()))
                            .addBodyParameter("title", edtTitle.getText().toString())
                            .addBodyParameter("content" ,edtContent.getText().toString())
                            .addBodyParameter("status", status)
                            .setTag("insert issue")
                            .setPriority(Priority.MEDIUM)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    if(response.optString("status").equals("200")){
                                        Log.println(Log.DEBUG,"ISSUE","200");
                                    }else
                                    {
                                        Log.println(Log.DEBUG,"ISSUE","400");
                                    }
                                }

                                @Override
                                public void onError(ANError anError) {
                                        anError.printStackTrace();
                                }
                            });
                }
                ListIssueFragment issueFragment= new ListIssueFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, issueFragment, "detail");
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().setTitle("Issue");
                fragmentTransaction.commit();
            }
        });
        spinner = (MaterialSpinner ) view.findViewById(R.id.spinner);
        spinner.setItems("-- Select --","Contour Issue", "Bad weather","Hard access","Community Issue","Pemda Issue","Existing Signal Issue","Contract Issue","Material Issue","Team Issue");
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectedItem = item.toString();
//                Snackbar.make(view, "Clicked " + selectedItem, Snackbar.LENGTH_LONG).show();
            }
        });
        edtTitle = (EditText) view.findViewById(R.id.txtTittle);
        edtContent = (EditText) view.findViewById(R.id.txtContent);
        txtSiteName.setText(session.isSiteName());
        txtMilestoneName.setText(session.isMilestoneName());
        txtActivityName.setText(session.isActivityName());

        session = new SessionManager(getActivity().getApplicationContext());

        // Inflate the layout for this fragment
        return view;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(
                Environment.getExternalStorageDirectory()
                        + File.separator
                        + "bts_kominfo"
                        + File.separator
                        + "picture"
        );

        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        fotoPath = "file:" + image.getAbsolutePath();
        if (filePath1 == null) {
            f1 = "1";
            filePath1 = "file:" + image.getAbsolutePath();
        }else if(filePath2 == null){
            f1 = "2";
            filePath2 = "file:" + image.getAbsolutePath();
        }else if(filePath3 == null){
            f1 = "3";
            filePath3 = "file:" + image.getAbsolutePath();
        }else if(filePath4 == null){
            f1 = "4";
            filePath4 = "file:" + image.getAbsolutePath();
        }else if(filePath5 == null){
            f1 = "5";
            filePath5 = "file:" + image.getAbsolutePath();
        }
        return image;
    }

    private void dispatchTakePictureIntent() throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                return;
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Log.println(Log.DEBUG,"photoFIle", photoFile.getPath());
                Uri photoURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName()+ ".my.package.name.provider",photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);

            }
        }
    }

    public void getLoc(){
        if(!permision.checkPermissionForFineLocation()){
            permision.requestPermissionForFineLocation();
        }else {
            if(!permision.checkPermissionForCoarseLocation()){
                permision.requestPermissionForCoarseLocation();
            }else{
                gps = new GPSTracker(getActivity());

                if(gps.canGetLocation()){
                    Double latitude = gps.getLatitude();
                    Double longitude = gps.getLongitude();
                    Location user = new Location("user");
                    user.setLongitude(longitude);
                    user.setLatitude(latitude);

                    GET_LATITUDE = Double.toString(latitude);
                    GET_LONGITUDE = Double.toString(longitude);


                    Log.println(Log.INFO, "latitude", GET_LATITUDE);
                    Log.println(Log.INFO, "logintude", GET_LONGITUDE);
                }else{


                    showGPSDisabledAlertToUser();
                }
            }
        }

    }

    private void showGPSDisabledAlertToUser() {
        try{
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setTitle("Attention")
                    .setMessage("GPS is required in your device. Would you like to enable it?")
                    .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(callGPSSettingIntent);
                        }
                    })
                    .create().show();
        }catch (Exception e){
            e.printStackTrace();
        }


//
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity().getApplicationContext());
//        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
//                .setCancelable(false)
//                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                        startActivity(callGPSSettingIntent);
//                    }
//                });
//        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.cancel();
//            }
//        });
//        AlertDialog alert = alertDialogBuilder.create();
//        alert.show();
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public Bitmap mark(Bitmap src) {

        int w = src.getWidth();
        int h = src.getHeight();
        int pw = w - 300;
        int ph = h - 170;
        int pw2 = w - 300;
        int ph2 = h - 150;
        int pw3 = w - 300;
        int ph3 = h - 130;
        int pw4 = w - 300;
        int ph4 = h - 110;
        int pw5 = w - 300;
        int ph5 = h - 90;
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
        Canvas canvas = new Canvas(result);

        canvas.drawBitmap(src, 0, 0, null);
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);

        //paint.setAlpha(20);
        paint.setTextSize(15);
        //paint.setAntiAlias(true);
        paint.setUnderlineText(false);
        if(gps.canGetLocation()){
            canvas.drawText("latitude: " + GET_LATITUDE , pw, ph, paint);
            canvas.drawText("longitude: " + GET_LONGITUDE , pw2, ph2, paint);
        }

        canvas.drawText("time: " + new Date(), pw3, ph3, paint);
        canvas.drawText("name: " + db.getUserDetails().get("name") , pw4, ph4, paint);
        canvas.drawText("user: " + db.getUserDetails().get("username"), pw5, ph5, paint);
        return result;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            // Show the thumbnail on ImageView

            Uri imageUri = Uri.parse(fotoPath);
            Log.println(Log.DEBUG,"photo",imageUri.getPath());
            Log.println(Log.DEBUG,"photo",takePicture.toString());
            File file = new File(imageUri.getPath());
            try {
                InputStream ims = new FileInputStream(file);
                if(f1.equals("1")){
                    filePath1 = imageUri.getPath();
                    myBitmap = BitmapFactory.decodeStream(ims);
                    myBitmap = getResizedBitmap(myBitmap, 520, 520);
                    myBitmap = mark(myBitmap);
                    db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap));
                    filePath1 = imageToString.getStringImage(myBitmap);
                    img1.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap)));

//                    textImageLat1.setText("Lat : " + gps.getLatitude());
//                    textImageLong1.setText("Long : " + gps.getLongitude());
//                    lo1.setVisibility(View.VISIBLE);
                    takePicture += 1;
                }else if(f1.equals("2")){
                    filePath2 = imageUri.getPath();
                    myBitmap2 = BitmapFactory.decodeStream(ims);
                    myBitmap2 = getResizedBitmap(myBitmap2, 520, 520);
                    myBitmap2 = mark(myBitmap2);
                    db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap2));
                    filePath2 = imageToString.getStringImage(myBitmap2);
                    img2.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap2)));
//                    textImageLat2.setText("Lat : " + gps.getLatitude());
//                    textImageLong2.setText("Long : " + gps.getLongitude());
//                    lo2.setVisibility(View.VISIBLE);
                    takePicture += 1;
                }else if(f1.equals("3")){
                    filePath3 = imageUri.getPath();
                    myBitmap3 = BitmapFactory.decodeStream(ims);
                    myBitmap3 = getResizedBitmap(myBitmap3, 520, 520);
                    myBitmap3 = mark(myBitmap3);
                    db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap3));
                    filePath3 = imageToString.getStringImage(myBitmap3);
                    img3.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap2)));
//                    textImageLat3.setText("Lat : " + gps.getLatitude());
//                    textImageLong3.setText("Long : " + gps.getLongitude());
//                    lo3.setVisibility(View.VISIBLE);
                    takePicture += 1;
                }else if(f1.equals("4")){
                    filePath4 = imageUri.getPath();
                    myBitmap4 = BitmapFactory.decodeStream(ims);
                    myBitmap4 = getResizedBitmap(myBitmap4, 520, 520);
                    myBitmap4 = mark(myBitmap4);
                    db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap4));
                    filePath4 = imageToString.getStringImage(myBitmap4);
                    img4.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap4)));
//                    textImageLat4.setText("Lat : " + gps.getLatitude());
//                    textImageLong4.setText("Long : " + gps.getLongitude());
//                    lo4.setVisibility(View.VISIBLE);

                    takePicture += 1;
                }else if(f1.equals("5")){
                    filePath5 = imageUri.getPath();
                    myBitmap5 = BitmapFactory.decodeStream(ims);
                    myBitmap5 = getResizedBitmap(myBitmap5, 520, 520);
                    myBitmap5 = mark(myBitmap5);
                    db.addTempPict(sessionInput, imageToString.getStringImage(myBitmap5));
                    filePath5 = imageToString.getStringImage(myBitmap5);
                    img5.setImageBitmap(imageToString.Base64ToImage(imageToString.getStringImage(myBitmap5)));
//                    textImageLat5.setText("Lat : " + gps.getLatitude());
//                    textImageLong5.setText("Long : " + gps.getLongitude());
//                    lo5.setVisibility(View.VISIBLE);
                    takePicture += 1;
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            boolean result = file.delete();
            textPhotoUpload.setText("Upload Foto ("+ String.valueOf(takePicture)+"/5)");
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{imageUri.getPath()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                        }
                    });






            // ScanFile so it will be appeared on Gallery

        }
    }

}
