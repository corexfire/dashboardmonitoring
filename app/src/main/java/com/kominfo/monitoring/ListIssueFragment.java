package com.kominfo.monitoring;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.kominfo.monitoring.adapter.IssueAdapter;
import com.kominfo.monitoring.helper.CheckConnection;
import com.kominfo.monitoring.helper.Encryptions;
import com.kominfo.monitoring.helper.Permision;
import com.kominfo.monitoring.helper.SQLiteHandler;
import com.kominfo.monitoring.helper.SessionManager;
import com.kominfo.monitoring.model.Issue;
import com.kominfo.monitoring.model.SiteList;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListIssueFragment extends Fragment {
    private SQLiteHandler db;
    List<Issue> mcList;
    Permision permision;
    SessionManager session;
    ListView lv;
    String GET_USER_ID;
    String GET_SITE_NAME;
    String GET_MILESTONE_NAME;
    String GET_ACTIVITY_NAME;
    Button btnAddIssue;
    LinearLayout NoData;
    CheckConnection checkConnection;
    Encryptions encryptions;
    public ListIssueFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_issue, container, false);
        permision = new Permision(getActivity());
        checkConnection = new CheckConnection();
        lv = (ListView) view.findViewById(R.id.lv_issue);
        encryptions = new Encryptions();
        db = new SQLiteHandler(getActivity().getApplicationContext());
        NoData = (LinearLayout) view.findViewById(R.id.noData);
        mcList = new ArrayList<Issue>();
        session = new SessionManager(getActivity().getApplicationContext());
        btnAddIssue = (Button) view.findViewById(R.id.btnAddIssue);
        btnAddIssue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IssueFragment issueFragment= new IssueFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, issueFragment, "detail");
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().setTitle("Add new an issue");
                fragmentTransaction.commit();
            }
        });
        GET_USER_ID = db.getUserDetails().get("uid");
        GET_SITE_NAME = session.isSiteName();
        GET_MILESTONE_NAME = session.isMilestoneName();
        GET_ACTIVITY_NAME = session.isActivityName();


        String selectIssue = "SELECT * FROM dm_issue where user_id ='"+ GET_USER_ID +"'";

        SQLiteDatabase dbs = db.getReadableDatabase();
        Cursor cursor = dbs.rawQuery(selectIssue, null);
//        Cursor cursor = db.getMilestoneCount().get;
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            lv.setVisibility(View.VISIBLE);
            NoData.setVisibility(View.GONE);
            do{

                Issue issue = new Issue(cursor.getString(cursor.getColumnIndex("id")),GET_USER_ID, cursor.getString(cursor.getColumnIndex("site_id")),
                        cursor.getString(cursor.getColumnIndex("milestone_id")), cursor.getString(cursor.getColumnIndex("activity_id")),
                        encryptions.getDecrypt(cursor.getString(cursor.getColumnIndex("title"))),cursor.getString(cursor.getColumnIndex("content")),
                        cursor.getString(cursor.getColumnIndex("status")),session.isSiteName(),
                        session.isMilestoneName(),session.isActivityName()
                        );
                mcList.add(issue);
            }while (cursor.moveToNext());

        }else{
            lv.setVisibility(View.GONE);
            NoData.setVisibility(View.VISIBLE);
        }
        IssueAdapter ia = new IssueAdapter(getActivity().getApplicationContext(), R.layout.list_item_issue,mcList);
        cursor.close();
        lv.setAdapter(ia);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });
        // Inflate the layout for this fragment
        return view;
    }

}
