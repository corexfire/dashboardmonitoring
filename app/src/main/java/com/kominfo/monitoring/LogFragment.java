package com.kominfo.monitoring;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaCas;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.kominfo.monitoring.adapter.LogLocalAdapter;
import com.kominfo.monitoring.config.AppConfig;
import com.kominfo.monitoring.helper.CheckConnection;
import com.kominfo.monitoring.helper.Encryptions;
import com.kominfo.monitoring.helper.ImageToString;
import com.kominfo.monitoring.helper.SQLiteHandler;
import com.kominfo.monitoring.helper.SessionManager;
import com.kominfo.monitoring.model.ActivityLogLocal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static android.media.CamcorderProfile.get;


/**
 * A simple {@link Fragment} subclass.
 */
public class LogFragment extends Fragment {
    ListView lv;
    SQLiteHandler db;
    List<ActivityLogLocal> mcList;
    SwipeRefreshLayout swipeRefreshLayout;
    Encryptions encryptions;
    SessionManager sessionManager;
    String new_id;
    String filepath;
    Bitmap myBitmap;
    ImageToString imageToString;


    public LogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.task_log, container, false);
        lv = (ListView) view.findViewById(R.id.lvLog);
        db =new SQLiteHandler(getActivity().getApplicationContext());
        mcList = new ArrayList<ActivityLogLocal>();
        encryptions = new Encryptions();
        imageToString = new ImageToString();
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        sessionManager = new SessionManager(getActivity().getApplicationContext());
//        swipeRefreshLayout = new SwipeRefreshLayout(getActivity().getApplicationContext());
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//               swipeRefreshLayout.setEnabled(true);

                       fillData();

            }
        });
//        swipeRefreshLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                fillData();
//            }
//        });


        fetchData();
//        Toast.makeText(getActivity().getApplicationContext(), "Error in collecting data",
//                Toast.LENGTH_SHORT).show();

        // Inflate the layout for this fragment
        return view;
    }

    void fillData(){

        String selectQuery = "DELETE from dm_activity_log ";
        SQLiteDatabase dbs = db.getWritableDatabase();
        dbs.delete("dm_activity_log",null,null);
        dbs.close();

        String selectDataLocal = "Select * from dm_activity_log_local";
        SQLiteDatabase dbx = db.getReadableDatabase();
        Cursor cursor = dbx.rawQuery(selectDataLocal,null);
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            do{
                db.addActivityLogFromLocal(cursor.getString(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("user_id")),
                        cursor.getString(cursor.getColumnIndex("site_activity_id")),
                        cursor.getString(cursor.getColumnIndex("milestone_id")),
                        cursor.getString(cursor.getColumnIndex("site_id")),
                        cursor.getString(cursor.getColumnIndex("activity_id")),
                        cursor.getString(cursor.getColumnIndex("title")),
                        cursor.getString(cursor.getColumnIndex("content")),
                        cursor.getString(cursor.getColumnIndex("percentage")),
                        cursor.getString(cursor.getColumnIndex("photo")),
                        cursor.getString(cursor.getColumnIndex("longitude")),
                        cursor.getString(cursor.getColumnIndex("latitude")),
                        cursor.getString(cursor.getColumnIndex("date_created")),
                        cursor.getString(cursor.getColumnIndex("date_uploaded")),
                        cursor.getString(cursor.getColumnIndex("date_modified")),
                        cursor.getString(cursor.getColumnIndex("status")),
                        cursor.getString(cursor.getColumnIndex("id_modified")),
                        encryptions.getEncrypt("local"));
            }while (cursor.moveToNext());

        }
        dataActivityLog();

    }

    void dataActivityLog(){
        AndroidNetworking.post(AppConfig.link + "data/activity/log")
                .addBodyParameter("userID", encryptions.getDecrypt(db.getUserDetails().get("uid")))
                .setTag("get milestone")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.println(Log.DEBUG, "response_status", response.optString("status"));
                        if (response.optString("status").equals("200")) {
                            try {
//                                    JSONObject json = response.getJSONObject("data");
//                                    Log.println(Log.DEBUG,"milestone",json.getString("milestone"));
                                JSONArray arr = response.getJSONArray("data");
//                                    progressBar.setMax(arr.length());
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject o = arr.getJSONObject(i);
                                    Log.println(Log.DEBUG, "get_id", o.getString("id"));
                                    db.addActivityLog(encryptions.getEncrypt(o.getString("id")),
                                            encryptions.getEncrypt(o.getString("user_id")),
                                            encryptions.getEncrypt(o.getString("site_activity_id")),
                                            encryptions.getEncrypt(o.getString("milestone_id")),
                                            encryptions.getEncrypt(o.getString("site_id")),
                                            encryptions.getEncrypt(o.getString("activity_id")),
                                            encryptions.getEncrypt(o.getString("title")),
                                            encryptions.getEncrypt(o.getString("content")),
                                            encryptions.getEncrypt(o.getString("percentage")),
                                            encryptions.getEncrypt(o.getString("photo")),
                                            encryptions.getEncrypt(o.getString("longitude")),
                                            encryptions.getEncrypt(o.getString("latitude")),
                                            encryptions.getEncrypt(o.getString("date_created")),
                                            encryptions.getEncrypt(o.getString("date_uploaded")),
                                            encryptions.getEncrypt(o.getString("date_modified")),
                                            encryptions.getEncrypt(o.getString("status")),
                                            encryptions.getEncrypt(o.getString("id_modified")),encryptions.getEncrypt( "online")
                                    );
                                }
                                activityPict();
                            } catch (JSONException e) {

                            }
                        } else {
//                            activityPict();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                    public void onError(ANError error) {
                        // handle error
                    }
                });

    }
    void activityPict() {
        AndroidNetworking.post(AppConfig.link + "data/activity/pict")
                .addBodyParameter("userID", encryptions.getDecrypt(db.getUserDetails().get("uid")))
                .setTag("get milestone")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.println(Log.DEBUG, "response_status", response.optString("status"));
                        if (response.optString("status").equals("200")) {
                            try {
//                                    JSONObject json = response.getJSONObject("data");
//                                    Log.println(Log.DEBUG,"milestone",json.getString("milestone"));
                                JSONArray arr = response.getJSONArray("data");
//                                    progressBar.setMax(arr.length());
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject o = arr.getJSONObject(i);
                                    final String get_id = encryptions.getEncrypt(o.getString("id"));
                                    final String get_activity_log_id = encryptions.getEncrypt(o.getString("activity_log_id"));
                                    final String get_photo = encryptions.getEncrypt( o.getString("photo"));
                                    final String get_date_created = encryptions.getEncrypt(o.getString("date_created"));
                                    new CountDownTimer(3000, 1000) {
                                        public void onTick(long millisUntilFinished) {


                                        }

                                        public void onFinish() {
                                            File mediaStorageDir = new File(
                                                    Environment.getExternalStorageDirectory()
                                                            + File.separator
                                                            + "bts_kominfo"
                                                            + File.separator
                                                            + "picture"
                                                            + File.separator
                                                            + encryptions.getDecrypt(get_activity_log_id)
                                            );
                                            if (!mediaStorageDir.exists()) {
                                                mediaStorageDir.mkdirs();
                                            }


                                            //TODO download picture from server
                                            AndroidNetworking.download("https://bpppti.teknusa.com/assets/uploads/activity_log/" + encryptions.getDecrypt(get_activity_log_id) + "/" + encryptions.getDecrypt(get_photo), mediaStorageDir.getPath(), encryptions.getDecrypt(get_photo))
                                                    .setTag("Download Picture")
                                                    .setPriority(Priority.HIGH)
                                                    .build()
                                                    .setDownloadProgressListener(new DownloadProgressListener() {
                                                        @Override
                                                        public void onProgress(long bytesDownloaded, long totalBytes) {

                                                        }
                                                    })
                                                    .startDownload(new DownloadListener() {
                                                        @Override
                                                        public void onDownloadComplete() {
                                                            File mediaStorageDir = new File(
                                                                    Environment.getExternalStorageDirectory()
                                                                            + File.separator
                                                                            + "bts_kominfo"
                                                                            + File.separator
                                                                            + "picture"
                                                                            + File.separator
                                                                            + encryptions.getDecrypt(get_activity_log_id)
                                                            );
                                                            filepath =mediaStorageDir.getPath() + "/" + encryptions.getDecrypt(get_photo);


                                                            myBitmap = BitmapFactory.decodeFile(filepath);

                                                            db.addActivityPict(get_id, get_activity_log_id, get_photo,
                                                                    get_date_created,imageToString.getStringImage(myBitmap));
                                                            boolean result = mediaStorageDir.delete();


                                                            Log.println(Log.DEBUG, "download", "success");
                                                        }

                                                        @Override
                                                        public void onError(ANError anError) {
                                                            Log.println(Log.DEBUG, "download", "failed");
                                                        }
                                                    });
                                        }

                                    }.start();

                                    //end download picture

                                }
                               fetchData();
//                                userSiteGet();
                            } catch (JSONException e){

                            }
                        } else {
                        }
                    }

                    public void onError(ANError error) {
                        // handle error
                    }
                });


    }



    void fetchData(){
        swipeRefreshLayout.setRefreshing(false);
        mcList.clear();
        String queryActLocal = "SELECT * FROM dm_activity_log ORDER BY date_modified DESC";
        SQLiteDatabase dbActLocal = db.getReadableDatabase();

        Cursor cursorActLocal = dbActLocal.rawQuery(queryActLocal, null);
        cursorActLocal.moveToFirst();
        if(cursorActLocal.getCount() > 0){
            do {
                ActivityLogLocal mc = new ActivityLogLocal(
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("id"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("user_id"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("site_activity_id"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("milestone_id"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("site_id"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("activity_id"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("title"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("content"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("percentage"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("photo"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("longitude"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("latitude"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("date_created"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("date_uploaded"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("date_modified"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("status"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("id_modified"))),
                        encryptions.getDecrypt(cursorActLocal.getString(cursorActLocal.getColumnIndex("state")))
                );
                mcList.add(mc);
            }while (cursorActLocal.moveToNext());
            Collections.reverse(mcList);
            LogLocalAdapter la = new LogLocalAdapter(getActivity().getApplicationContext(),R.layout.list_activity_log,mcList);
            cursorActLocal.close();
            lv.setAdapter(la);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Log.println(Log.DEBUG,"get_detail", mcList.get(i).getTitle());
                    DetailActivityLogFragment detailActivityLogFragment = new DetailActivityLogFragment();
                    Bundle args = new Bundle();
                    args.putString(DetailActivityLogFragment.ID,mcList.get(i).getId());
                    args.putString(DetailActivityLogFragment.ACTIVITY_TITLE,mcList.get(i).getTitle());
                    args.putString(DetailActivityLogFragment.ACTIVITY_ID,mcList.get(i).getActivity_id());
                    args.putString(DetailActivityLogFragment.SITE_ACTIVITY_ID,mcList.get(i).getSite_activity_id());
                    args.putString(DetailActivityLogFragment.PERCENTACE,mcList.get(i).getPercentage());
                    args.putString(DetailActivityLogFragment.DATE_MODIFIED,mcList.get(i).getDate_modified());
                    args.putString(DetailActivityLogFragment.DATE_CREATED,mcList.get(i).getDate_created());
                    args.putString(DetailActivityLogFragment.MILESTONE_ID,mcList.get(i).getMilestone_id());
                    args.putString(DetailActivityLogFragment.SITE_ID,mcList.get(i).getSite_id());
                    args.putString(DetailActivityLogFragment.LATITUDE,mcList.get(i).getLatitude());
                    args.putString(DetailActivityLogFragment.LONGITUDE,mcList.get(i).getLongitude());
                    args.putString(DetailActivityLogFragment.CONTENT,mcList.get(i).getContent());
                    args.putString(DetailActivityLogFragment.STATE,mcList.get(i).getState());

                    detailActivityLogFragment.setArguments(args);
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame, detailActivityLogFragment, "ActivityLogdetail");
                    getActivity().getSupportFragmentManager().popBackStack();
                    getActivity().setTitle("Detail");
                    fragmentTransaction.commit();
                }
            });
        }

    }

}
