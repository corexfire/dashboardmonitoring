package com.kominfo.monitoring;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Environment;
import android.renderscript.RenderScript;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.kominfo.monitoring.helper.CheckConnection;
import com.kominfo.monitoring.helper.Encryptions;
import com.kominfo.monitoring.helper.GetVersion;
import com.kominfo.monitoring.helper.Permision;
import com.kominfo.monitoring.helper.SessionManager;
import com.kominfo.monitoring.helper.SQLiteHandler;
import com.kominfo.monitoring.config.AppConfig;

import com.kominfo.monitoring.helper.ConnectionReceiver;

import com.kominfo.monitoring.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

public class LoginActivity extends AppCompatActivity  {

    public  static String isConnect = "false";
    private CheckConnection conCheck;
    private ConnectionReceiver conRec;
    private ImageView imgOnline, imgOffline;
    private TextView textOnline, textOffline, textVersionName;
    private EditText textUsername, textPassword;
    private Button btnLogin;
    private ProgressBar progressBar;
    private SessionManager session;
    private SQLiteHandler db;
    private String GET_LONGITUDE;
    private String GET_LATITUDE;
    private String GET_NAME;
    private String GET_USERNAME;
    private String GET_SITE_ID;
    private String GET_UID;
    final User userLogin = new User();
     Permision permision;
    GetVersion getVersion;
    Encryptions encrypt;
    private String filePathNoMedia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        permision = new Permision(this);
        getVersion = new GetVersion();
        textVersionName = (TextView) findViewById(R.id.textVersionName);
        String version = getVersion.getVersionName(this.getApplicationContext());
        textVersionName.setText(version);
        encrypt = new Encryptions();

        if (!permision.checkPermissionForCamera()) {
            permision.requestPermissionForCamera();
        }
        if (!permision.checkPermissionForExternalStorage()) {
            permision.requestPermissionForExternalStorage();
        }
        if (!permision.checkPermissionForFineLocation()) {
            permision.requestPermissionForFineLocation();
        }
        if (!permision.checkPermissionForCoarseLocation()) {
            permision.requestPermissionForCoarseLocation();
        }




            imgOnline = (ImageView) findViewById(R.id.imgOnline);
            imgOffline = (ImageView) findViewById(R.id.imgOffline);
            textOnline = (TextView) findViewById(R.id.textOnline);
            textOffline = (TextView) findViewById(R.id.textOffline);
            progressBar = (ProgressBar) findViewById(R.id.progressBar2);
            textUsername = (EditText) findViewById(R.id.textUsername);
            textPassword = (EditText) findViewById(R.id.textPassword);
            db = new SQLiteHandler(getApplicationContext());
            permision = new Permision(this);
            session = new SessionManager(getApplicationContext());
            if (session.isLoggedIn()) {
//                MainActivity.isFirstLogin = true;
//                startActivity(new Intent(
//                        LoginActivity.this, MainActivity.class
//                ));
//                finish();
                startActivity(new Intent(LoginActivity.this, DownloadActivity.class));
                finish();
            } else {
                if (!permision.checkPermissionForExternalStorage()) {
                    permision.requestPermissionForExternalStorage();
                } else {
//            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File mediaStorageDir = new File(
                            Environment.getExternalStorageDirectory()
                                    + File.separator
                                    + AppConfig.internalStorageName
                    );

                    filePathNoMedia = mediaStorageDir.getPath() + File.separator + ".nomedia";
                    File fileNoMedia = new File(filePathNoMedia);
                    try{
                        if(fileNoMedia.createNewFile()){
                            System.out.println(filePathNoMedia + " File Created");
                        }else{
                            System.out.println("File " + fileNoMedia.getAbsolutePath() + " already exists");
                        }
                    }catch (IOException e){
                        System.err.println("Things went wrong: " + e.getMessage());
                        e.printStackTrace();
                    }




                    DeleteRecursive(mediaStorageDir);


                }
            }
            if (conCheck.isInternetAvailable(this)) {
                isConnect = "true";
            } else {
                isConnect = "false";
            }


            Log.println(Log.DEBUG, "isLoggedIn", Boolean.toString(session.isLoggedIn()));
            btnLogin = (Button) findViewById(R.id.buttonLogin);
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideKeyboard();
                    loadActivity();
//                showProgressBar();
                }
            });
        }


    private void DeleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
            {
                child.delete();
                DeleteRecursive(child);
            }

        fileOrDirectory.delete();
    }

    private void goOnline(){
        imgOffline.setVisibility(View.GONE);
        textOffline.setVisibility(View.GONE);
        imgOnline.setVisibility(View.VISIBLE);
        textOnline.setVisibility(View.VISIBLE);
    }

    private void goOffline(){
        imgOffline.setVisibility(View.VISIBLE);
        textOffline.setVisibility(View.VISIBLE);
        imgOnline.setVisibility(View.GONE);
        textOnline.setVisibility(View.GONE);
    }

    private void showProgressBar(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }



    @Override
    public void onResume() {
        super.onResume();

                Log.println(Log.DEBUG,"resume connection",isConnect.toString());
//                if(isConnect.equals("true")) {
//
//                    goOnline();
//                }else{
//                    goOffline();
//                }


    }

//    @Override
//    public void onPause(){
//        super.onPause();
//                Log.println(Log.DEBUG,"pause connection",isConnect.toString());
//                if(isConnect.equals("true")) {
//
//                    goOnline();
//                }else{
//                    goOffline();
//                }
//
//    }

    public void hideKeyboard() {
        if(getCurrentFocus()!=null){
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
    }

    public void Login(){
        AndroidNetworking.post(AppConfig.link + "user/login")
                .addBodyParameter("username", userLogin.getUsername())
                .addBodyParameter("password", userLogin.getPassword())
                .setTag("login")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.println(Log.DEBUG,"response",response.optString("status"));

                        if (response.optString("status").equals("200")){
                            Toast.makeText(getApplicationContext(), "login successfully",
                                    Toast.LENGTH_SHORT).show();
                            try{

                                JSONObject json= response.getJSONObject("data");
                                GET_UID = json.getString("id");
                                GET_NAME = json.getString("realname");
                                GET_USERNAME = json.getString("username");


                                GET_SITE_ID = json.getString("site_id");
                                Log.println(Log.DEBUG,"GET_NAME",GET_UID);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }




                            session.setLogin(true);
                            session.setComplete(false);
                            db.deleteUsers();
                            db.addUser(encrypt.getEncrypt( GET_UID), GET_NAME, GET_USERNAME, encrypt.getEncrypt( GET_SITE_ID));
                            MainActivity.isFirstLogin = true;
                            startActivity(new Intent(
                                    LoginActivity.this, MainActivity.class
                            ));
                            finish();
                            hideProgressBar();



//                            if(!session.isLoggedIn()) {

//                            }


//                            result =
//                                    "id\n" + response.optString("id") +
//                                            "\n\nusername\n" + response.optString("username") +
//                                            "\n\nname\n" + response.optString("name");


                        } else {
                            hideProgressBar();
                            Toast.makeText(getApplicationContext(), "login gagal",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                       hideProgressBar();
                        Toast.makeText(getApplicationContext(), "login gagal",
                                Toast.LENGTH_SHORT).show();
                        // handle error
                    }
                });

    }

    void loadActivity(){
        showProgressBar();
        if(conCheck.isInternetAvailable(this)) {
            userLogin.setUsername(textUsername.getText().toString());
            userLogin.setPassword(textPassword.getText().toString());
            if (!session.isLoggedIn())
                Login();
        }else{
            hideProgressBar();
            new AlertDialog.Builder(this)
                    .setTitle("Cannot Login")
                    .setMessage("Need internet connection")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
//                            LoginActivity.super.onBackPressed();

//                            loadActivity();
                        }
                    }).create().show();
        }
    }
}
