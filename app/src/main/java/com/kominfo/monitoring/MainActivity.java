package com.kominfo.monitoring;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import com.kominfo.monitoring.helper.CheckConnection;
import com.kominfo.monitoring.helper.Encryptions;
import com.kominfo.monitoring.helper.ImageToString;
import com.kominfo.monitoring.helper.Permision;
import com.kominfo.monitoring.helper.SessionManager;
import com.kominfo.monitoring.helper.SQLiteHandler;
import com.kominfo.monitoring.config.AppConfig;
import com.kominfo.monitoring.model.Input;
import com.kominfo.monitoring.model.MilestoneCount;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Permision permision;
    public static boolean isFirstLogin = false;
    final static String MILESTONE_ID = "milestone_id";
    final static String MILESTONE_NAME = "milestone_name";
    Bitmap myBitmap;
    String GET_SITE_ID;
    String GET_USER_ID;
    Integer GET_SITE_COUNT;
    Integer GET_FOR_COUNT;
    String filepath;
    Integer countProgres = 0;
    ImageToString imageToString;
    CheckConnection conCheck;
    SQLiteHandler db;
    SessionManager session;
    ProgressBar progressBar, progressBar2;
    ImageView imageView;
    TextView textView;
    private int totalMax = 0;
    private int totalProgress = 0;
    Encryptions encryptions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fm = getFragmentManager();
        imageToString = new ImageToString();
        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if(getFragmentManager().getBackStackEntryCount() == 0) finish();
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        int percent = (int) ((25/1000)*100);
        encryptions = new Encryptions();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(8);
        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
        permision = new Permision(this);
        if (!permision.checkPermissionForExternalStorage()) {
            permision.requestPermissionForExternalStorage();
        } else {
//            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File mediaStorageDir = new File(
                    Environment.getExternalStorageDirectory()
                            + File.separator
                            + AppConfig.internalStorageName
                            + File.separator
                            + "picture"
            );

            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdirs();
            }
        }





        textView = (TextView) findViewById(R.id.txtDownload);
//        totalMax = progressBar.getMax();

        imageView = (ImageView) findViewById(R.id.imageView2);

        db = new SQLiteHandler(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        GET_SITE_ID = encryptions.getDecrypt(db.getUserDetails().get("site_id"));
        GET_USER_ID = encryptions.getDecrypt(db.getUserDetails().get("uid"));
//        Log.println(Log.DEBUG,"GET_SITE_ID",GET_SITE_ID);
//        Log.println(Log.DEBUG, "isComplete", String.valueOf(session.isComplete()));
//        Log.println(Log.DEBUG, "isLogin", String.valueOf(session.isLoggedIn()));
        if(isFirstLogin) {
            if (!session.isComplete()) {
//                getAllActivity();
                db.deleteLocalData();
                getActivityByUserSite();
            }else{
//                startService(new Intent(this, dasboardService.class));
                imageView.setVisibility(View.GONE);
                textView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                progressBar2.setVisibility(View.GONE);
                session.setComplete(true);
                File mediaStorageDir = new File(
                        Environment.getExternalStorageDirectory()
                                + File.separator
                                + AppConfig.internalStorageName
                                + File.separator
                                + "picture"
                );

                deleteRecursive(mediaStorageDir);
                DashboardFragment dasbboard = new DashboardFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, dasbboard, "dashboard");
                getSupportFragmentManager().popBackStack();
                setTitle("Dashboard");
                fragmentTransaction.commit();
            }
        }else{
            imageView.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            progressBar2.setVisibility(View.GONE);
            session.setComplete(true);
            DashboardFragment dasbboard = new DashboardFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, dasbboard, "dashboard");
            getSupportFragmentManager().popBackStack();
            setTitle("Dashboard");
        fragmentTransaction.commit();
        }




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        View hView =  navigationView.getHeaderView(0);
        TextView nav_user = (TextView)hView.findViewById(R.id.textRealname);
        nav_user.setText(db.getUserDetails().get("name"));
    }

    //TODO run task
    class Task implements Runnable {
        @Override
        public void run() {
//            session.setComplete(false);
            //TODO get site activity


//            for (int i = 0; i <= progressBar.getMax(); i++) {
//                final int value = i;
//                try {
//                    Thread.sleep(200);
//
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
////                totalProgress = value;
//
//                    progressBar.setProgress(value);
//
//
//
//            }
        }
    }

    public void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        }

        fileOrDirectory.delete();
    }


    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
        if(fragment instanceof TaskListActivity){
            SiteListFragment dasbboard = new SiteListFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, dasbboard, "sitelist");
            getSupportFragmentManager().popBackStack();
            setTitle("Site List");
            fragmentTransaction.commit();
        }else if(fragment instanceof SiteListFragment){
            DashboardFragment dasbboard = new DashboardFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, dasbboard, "sitelist");
            getSupportFragmentManager().popBackStack();
            setTitle("Dashboard");
            fragmentTransaction.commit();
        }else if(fragment instanceof LogFragment){
            DashboardFragment dasbboard = new DashboardFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, dasbboard, "sitelist");
            getSupportFragmentManager().popBackStack();
            setTitle("Dashboard");
            fragmentTransaction.commit();
        }else if(fragment instanceof TaskDetail){
            TaskListActivity dasbboard = new TaskListActivity();
            Bundle args = new Bundle();

            args.putString(TaskListActivity.SITE_ID,session.isSiteId());
            dasbboard.setArguments(args);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, dasbboard, "taskList");
            getSupportFragmentManager().popBackStack();
            setTitle(session.isSiteName());
            fragmentTransaction.commit();
        }else if(fragment instanceof InputFragment){

            try{
                new AlertDialog.Builder(this)
                        .setTitle(android.R.string.dialog_alert_title)
                        .setMessage(getString(R.string.close_task))
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {
                                try{
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("milestone_shared_id",0);
                                    SharedPreferences pref2 = getApplicationContext().getSharedPreferences("milestone_shared_name",0);

                                    Log.println(Log.DEBUG,"get milestone id", pref.getString("milestone_id",null));
                                    Log.println(Log.DEBUG,"get milestone name", pref2.getString("milestone_name",null));

                                    TaskDetail taskDetail = new TaskDetail();
                                    Bundle args = new Bundle();
                                    args.putString(TaskDetail.MILESTONE_ID,pref.getString("milestone_id",null));
                                    taskDetail.setArguments(args);
                                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, taskDetail, "detail");
                                    getSupportFragmentManager().popBackStack();
                                    setTitle( pref2.getString("milestone_name",null));
                                    fragmentTransaction.commit();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }


                            }
                        }).create().show();


            }catch (Exception e){
                e.printStackTrace();
            }
        }else if(fragment instanceof DetailActivityLogFragment){
            LogFragment dasbboard = new LogFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, dasbboard, "taskList");
            getSupportFragmentManager().popBackStack();
            setTitle("Activity Log");
            fragmentTransaction.commit();
        }else if(fragment instanceof ListIssueFragment) {
            TaskDetail taskDetail = new TaskDetail();
            Bundle args = new Bundle();
            Input input = new Input(getApplicationContext());
            input.setMilestone(session.isMilestoneId(),session.isMilestoneName());

            args.putString(TaskDetail.MILESTONE_ID,session.isMilestoneId());
            taskDetail.setArguments(args);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, taskDetail, "detail");
            getSupportFragmentManager().popBackStack();
            setTitle(session.isMilestoneName());
            fragmentTransaction.commit();
        }
        else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            DashboardFragment dasbboard = new DashboardFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, dasbboard, "dashboard");
            getSupportFragmentManager().popBackStack();
            setTitle("Dashboard");
            fragmentTransaction.commit();
        } else if (id == R.id.nav_task) {
            SiteListFragment dasbboard = new SiteListFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, dasbboard, "sitelist");
            getSupportFragmentManager().popBackStack();
            setTitle("Site List");
            fragmentTransaction.commit();
        } else if (id == R.id.nav_log) {
            LogFragment dasbboard = new LogFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, dasbboard, "taskList");
            getSupportFragmentManager().popBackStack();
            setTitle("Activity Log");
            fragmentTransaction.commit();
        } else if (id == R.id.nav_change_password) {
            ChangePasswordFragment mainFragment = new ChangePasswordFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, mainFragment, "change_password");
            getSupportFragmentManager().popBackStack();
            fragmentTransaction.commit();
            setTitle("Ganti Password");
        }
        else if (id == R.id.nav_report) {
//            imageView.setVisibility(View.GONE);
//            textView.setVisibility(View.GONE);
//            progressBar.setVisibility(View.GONE);
            BugReport mainFragment = new BugReport();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, mainFragment, "laporan");
            getSupportFragmentManager().popBackStack();
            fragmentTransaction.commit();
            setTitle("Laporan");

        } else if (id == R.id.nav_logout) {
            new AlertDialog.Builder(this)
                    .setTitle(android.R.string.dialog_alert_title)
                    .setMessage(getString(R.string.logout))
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            try{
//                                stopService(new Intent(MainActivity.this, dasboardService.class));
                                session.setLogin(false);
                                db.deleteUsers();
                                startActivity(new Intent(
                                        MainActivity.this, LoginActivity.class
                                ));
                                finish();
                            }catch (Exception e){
                                e.printStackTrace();
                            }


                        }
                    }).create().show();




        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void checkProgress(){
        do{
//            startService(new Intent(MainActivity.this, dasboardService.class));
            imageView.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            progressBar2.setVisibility(View.GONE);
            session.setComplete(true);
            DashboardFragment dasbboard = new DashboardFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, dasbboard, "dashboard");
//                                                        getSupportFragmentManager().popBackStack();
            setTitle("Dashboard");
            fragmentTransaction.commit();
        }while (countProgres == 100);
    }

    void getActivityByUserSite(){
        AndroidNetworking.post(AppConfig.link + "user/site/get/all/")
                .addBodyParameter("user_id", GET_USER_ID)
                .setTag("get_site")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject responses) {
                        
                        if (responses.optString("status").equals("200")){
                            try {

                                JSONArray arr = responses.getJSONArray("data");

                                GET_SITE_COUNT = arr.length();
                                for(int j =0; j < arr.length();j++){
                                    JSONArray arr2 = arr.getJSONArray(j);
                                    for (int i = 0; i < arr2.length(); i++) {
                                        GET_FOR_COUNT = i;
                                        JSONObject o = arr2.getJSONObject(i);


                                        db.addSite(encryptions.getEncrypt(o.getString("id")),
                                                encryptions.getEncrypt(o.getString("kontraktor_id")),
                                                encryptions.getEncrypt(o.getString("sub_project_id")),
                                                encryptions.getEncrypt(o.getString("area_id")),
                                                encryptions.getEncrypt(o.getString("project_id")),
                                                encryptions.getEncrypt(o.getString("site_real_id")),
                                                encryptions.getEncrypt(o.getString("provinsi")),
                                                encryptions.getEncrypt(o.getString("kota")),
                                                encryptions.getEncrypt(o.getString("kecamatan")),
                                                encryptions.getEncrypt(o.getString("desa")),
                                                encryptions.getEncrypt(o.getString("title")),
                                                encryptions.getEncrypt(o.getString("content")),
                                                encryptions.getEncrypt(o.getString("longitude")),
                                                encryptions.getEncrypt(o.getString("latitude")),
                                                encryptions.getEncrypt(o.getString("bobot")),
                                                encryptions.getEncrypt(o.getString("date_created")),
                                                encryptions.getEncrypt(o.getString("date_modified")),
                                                encryptions.getEncrypt(o.getString("status")));
//                                        userSiteGetActivity(o.getString("id"));
                                    }
                                }


//                                do{
                                    countProgres = 100;
                                    userSiteGetActivity();
                                    dataMilestone();
                                getMilestoneCount();
//                                }while (GET_SITE_COUNT.equals(GET_FOR_COUNT));
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else{
                            userSiteGet();
//                            dataMilestone();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                            anError.printStackTrace();
                    }
                });




    }





    void userSiteGetActivity(){
        String selectSite = "Select * FROM dm_site";
        SQLiteDatabase dbs = db.getReadableDatabase();
        Cursor cursor = dbs.rawQuery(selectSite,null);
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            do{
                AndroidNetworking.post(AppConfig.link + "user/site/get/activity")
                        .addBodyParameter("siteID",encryptions.getDecrypt( cursor.getString(cursor.getColumnIndex("id"))))
                        .setTag("get activity")
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.println(Log.DEBUG, "response_status", response.optString("status"));
                                if (response.optString("status").equals("200")) {
                                    try {
//                                    JSONObject json = response.getJSONObject("data");
//                                    Log.println(Log.DEBUG,"milestone",json.getString("milestone"));
                                        JSONArray arr = response.getJSONArray("data");
                                        progressBar2.setMax(arr.length());
//                                    progressBar.setMax(arr.length());
                                        for (int i = 0; i < arr.length(); i++) {
                                            JSONObject o = arr.getJSONObject(i);
                                            Log.println(Log.DEBUG, "get_id", o.getString("id"));
                                            db.addSiteActivity(encryptions.getEncrypt(o.getString("id")),
                                                    encryptions.getEncrypt(o.getString("activity_id")),
                                                    encryptions.getEncrypt(o.getString("site_id")),
                                                    encryptions.getEncrypt(o.getString("milestone_id")),
                                                    encryptions.getEncrypt(o.getString("kontraktor_id")),
                                                    encryptions.getEncrypt(o.getString("sub_project_id")),
                                                    encryptions.getEncrypt(o.getString("area_id")),
                                                    encryptions.getEncrypt(o.getString("project_id")),
                                                    encryptions.getEncrypt(o.getString("title")),
                                                    encryptions.getEncrypt(o.getString("content")),
                                                    encryptions.getEncrypt(o.getString("bobot")),
                                                    encryptions.getEncrypt(o.getString("percentage")),
                                                    encryptions.getEncrypt(o.getString("baseline_start")),
                                                    encryptions.getEncrypt(o.getString("baseline_finish")),
                                                    encryptions.getEncrypt(o.getString("date_start")),
                                                    encryptions.getEncrypt(o.getString("date_finish")),
                                                    encryptions.getEncrypt(o.getString("date_created")),
                                                    encryptions.getEncrypt(o.getString("date_modified")),
                                                    encryptions.getEncrypt(o.getString("status")),
                                                    encryptions.getEncrypt(o.getString("gta_flag")));
                                            progressBar2.setProgress(i + 1);
                                        }
                                        progressBar.setProgress(1);

                                    }catch (JSONException e){

                                    }
                                }else
                                {
                                    progressBar.setProgress(1);

                                }
                            }
                            public void onError(ANError error) {
                                // handle error
                            }
                        });
            }while (cursor.moveToNext());
        }
//        milestoneProgress();
        dbs.close();
        cursor.close();

    }

    void userSiteGet(){
        AndroidNetworking.post(AppConfig.link + "user/site/get/")
                .addBodyParameter("siteID", GET_SITE_ID)
                .setTag("get activity")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.println(Log.DEBUG, "response_status", response.optString("status"));
                        if (response.optString("status").equals("200")) {
                            try {
                                JSONArray json = response.getJSONArray("data");
                                JSONObject o = json.getJSONObject(0);
                                progressBar2.setMax(1);
                                db.addSite(encryptions.getEncrypt(o.getString("id")),
                                        encryptions.getEncrypt(o.getString("kontraktor_id")),
                                        encryptions.getEncrypt(o.getString("sub_project_id")),
                                        encryptions.getEncrypt(o.getString("area_id")),
                                        encryptions.getEncrypt(o.getString("project_id")),
                                        encryptions.getEncrypt(o.getString("site_real_id")),
                                        encryptions.getEncrypt(o.getString("provinsi")),
                                        encryptions.getEncrypt(o.getString("kota")),
                                        encryptions.getEncrypt(o.getString("kecamatan")),
                                        encryptions.getEncrypt(o.getString("desa")),
                                        encryptions.getEncrypt(o.getString("title")),
                                        encryptions.getEncrypt(o.getString("content")),
                                        encryptions.getEncrypt(o.getString("longitude")),
                                        encryptions.getEncrypt(o.getString("latitude")),
                                        encryptions.getEncrypt(o.getString("bobot")),
                                        encryptions.getEncrypt(o.getString("date_created")),
                                        encryptions.getEncrypt(o.getString("date_modified")),
                                        encryptions.getEncrypt(o.getString("status")));
                                progressBar2.setProgress(1);
                                progressBar.setProgress(2);
                                dataMilestone();
//                                userSiteGetActivity(o.getString("id"));
                            } catch (JSONException e) {

                            }
                        } else {
                            progressBar.setProgress(2);
                            dataMilestone();
                        }
                    }
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }
    void dataMilestone(){
        AndroidNetworking.get(AppConfig.link + "data/milestone/")
                .setTag("get milestone")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.println(Log.DEBUG, "response_status", response.optString("status"));
                        if (response.optString("status").equals("200")) {
                            try {
//                                    JSONObject json = response.getJSONObject("data");
//                                    Log.println(Log.DEBUG,"milestone",json.getString("milestone"));
                                JSONArray arr = response.getJSONArray("data");
                                progressBar2.setMax(arr.length());
//                                    progressBar.setMax(arr.length());
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject o = arr.getJSONObject(i);
                                    Log.println(Log.DEBUG, "get_id", o.getString("id"));
                                    db.addMilestone(encryptions.getEncrypt(o.getString("id")),
                                            encryptions.getEncrypt(o.getString("parent_id")),
                                            encryptions.getEncrypt(o.getString("name")));
                                    progressBar2.setProgress(i + 1);
                                }
                                progressBar.setProgress(3);
                                countProgres = 2;
//                                dataActivity();
                            } catch (JSONException e) {

                            }
                        } else {
                            progressBar.setProgress(3);
//                            dataActivity();
                        }
                    }

                    public void onError(ANError error) {
                        // handle error
                    }
                });

    }
    void dataActivity(){
        AndroidNetworking.get(AppConfig.link + "data/activity/")
                .setTag("get milestone")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.println(Log.DEBUG, "response_status", response.optString("status"));
                        if (response.optString("status").equals("200")) {
                            try {
//                                    JSONObject json = response.getJSONObject("data");
//                                    Log.println(Log.DEBUG,"milestone",json.getString("milestone"));
                                JSONArray arr = response.getJSONArray("data");
                                progressBar2.setMax(arr.length());
//                                    progressBar.setMax(arr.length());
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject o = arr.getJSONObject(i);
                                    Log.println(Log.DEBUG, "get_id", o.getString("id"));
                                    db.addActivity(encryptions.getEncrypt(o.getString("id")),
                                            encryptions.getEncrypt(o.getString("area_id")),
                                            encryptions.getEncrypt(o.getString("milestone_id")),
                                            encryptions.getEncrypt(o.getString("title")),
                                            encryptions.getEncrypt(o.getString("petunjuk_foto")));
                                    progressBar2.setProgress(i + 1);
                                }
                                progressBar.setProgress(4);
                                countProgres = 3;
                                dataActivityLog();
                            } catch (JSONException e) {

                            }
                        } else {
                            progressBar.setProgress(4);
                            dataActivityLog();
                        }
                    }
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }
    void dataActivityLog(){
        AndroidNetworking.post(AppConfig.link + "data/activity/log")
                .addBodyParameter("userID", GET_USER_ID)
                .setTag("get milestone")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.println(Log.DEBUG, "response_status", response.optString("status"));
                        if (response.optString("status").equals("200")) {
                            try {
//                                    JSONObject json = response.getJSONObject("data");
//                                    Log.println(Log.DEBUG,"milestone",json.getString("milestone"));
                                JSONArray arr = response.getJSONArray("data");
                                progressBar2.setMax(arr.length());
//                                    progressBar.setMax(arr.length());
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject o = arr.getJSONObject(i);
                                    Log.println(Log.DEBUG, "get_id", o.getString("id"));
                                    db.addActivityLog(encryptions.getEncrypt(o.getString("id")),
                                            encryptions.getEncrypt(o.getString("user_id")),
                                            encryptions.getEncrypt(o.getString("site_activity_id")),
                                            encryptions.getEncrypt(o.getString("milestone_id")),
                                            encryptions.getEncrypt(o.getString("site_id")),
                                            encryptions.getEncrypt(o.getString("activity_id")),
                                            encryptions.getEncrypt(o.getString("title")),
                                            encryptions.getEncrypt(o.getString("content")),
                                            encryptions.getEncrypt(o.getString("percentage")),
                                            encryptions.getEncrypt(o.getString("photo")),
                                            encryptions.getEncrypt(o.getString("longitude")),
                                            encryptions.getEncrypt(o.getString("latitude")),
                                            encryptions.getEncrypt(o.getString("date_created")),
                                            encryptions.getEncrypt(o.getString("date_uploaded")),
                                            encryptions.getEncrypt(o.getString("date_modified")),
                                            encryptions.getEncrypt(o.getString("status")),
                                            encryptions.getEncrypt(o.getString("id_modified")),encryptions.getEncrypt( "online")
                                    );
                                    progressBar2.setProgress(i + 1);
                                }
                                progressBar.setProgress(5);
                                countProgres = 4;
                                activityPict();
                            } catch (JSONException e) {

                            }
                        } else {
                            progressBar.setProgress(5);
                            activityPict();
                        }
                    }
                    public void onError(ANError error) {
                        // handle error
                    }
                });

    }
    void activityPict() {
        AndroidNetworking.post(AppConfig.link + "data/activity/pict")
                .addBodyParameter("userID", GET_USER_ID)
                .setTag("get milestone")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.println(Log.DEBUG, "response_status", response.optString("status"));
                        if (response.optString("status").equals("200")) {
                            try {
//                                    JSONObject json = response.getJSONObject("data");
//                                    Log.println(Log.DEBUG,"milestone",json.getString("milestone"));
                                JSONArray arr = response.getJSONArray("data");
                                progressBar2.setMax(arr.length());
//                                    progressBar.setMax(arr.length());
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject o = arr.getJSONObject(i);
                                    final String get_id = encryptions.getEncrypt(o.getString("id"));
                                    final String get_activity_log_id = encryptions.getEncrypt(o.getString("activity_log_id"));
                                    final String get_photo = encryptions.getEncrypt( o.getString("photo"));
                                    final String get_date_created = encryptions.getEncrypt(o.getString("date_created"));
                                    new CountDownTimer(3000, 1000) {
                                        public void onTick(long millisUntilFinished) {


                                        }

                                        public void onFinish() {
                                            File mediaStorageDir = new File(
                                                    Environment.getExternalStorageDirectory()
                                                            + File.separator
                                                            + AppConfig.internalStorageName
                                                            + File.separator
                                                            + "picture"
                                                            + File.separator
                                                            + encryptions.getDecrypt(get_activity_log_id)
                                            );
                                            if (!mediaStorageDir.exists()) {
                                                mediaStorageDir.mkdirs();
                                            }


                                            //TODO download picture from server
                                            AndroidNetworking.download("https://bpppti.teknusa.com/assets/uploads/activity_log/" + encryptions.getDecrypt(get_activity_log_id) + "/" + encryptions.getDecrypt(get_photo), mediaStorageDir.getPath(), encryptions.getDecrypt(get_photo))
                                                    .setTag("Download Picture")
                                                    .setPriority(Priority.HIGH)
                                                    .build()
                                                    .setDownloadProgressListener(new DownloadProgressListener() {
                                                        @Override
                                                        public void onProgress(long bytesDownloaded, long totalBytes) {
                                                            
                                                        }
                                                    })
                                                    .startDownload(new DownloadListener() {
                                                        @Override
                                                        public void onDownloadComplete() {

                                                            File mediaStorageDir = new File(
                                                                    Environment.getExternalStorageDirectory()
                                                                            + File.separator
                                                                            + AppConfig.internalStorageName
                                                                            + File.separator
                                                                            + "picture"
                                                                            + File.separator
                                                                            + encryptions.getDecrypt(get_activity_log_id)
                                                            );
                                                            filepath =mediaStorageDir.getPath() + "/" + encryptions.getDecrypt(get_photo);


                                                                myBitmap = BitmapFactory.decodeFile(filepath);

                                                                db.addActivityPict(get_id, get_activity_log_id, get_photo,
                                                                        get_date_created,imageToString.getStringImage(myBitmap));
                                                                boolean result = mediaStorageDir.delete();

                                                            Log.println(Log.DEBUG, "download", "success");
                                                        }

                                                        @Override
                                                        public void onError(ANError anError) {
                                                            Log.println(Log.DEBUG, "download", "failed");
                                                        }
                                                    });
                                        }

                                    }.start();

                                    //end download picture

                                    progressBar2.setProgress(i + 1);
                                }
                                progressBar.setProgress(8);
                                countProgres = 5;
                                checkProgress();
                                if(progressBar.getProgress() == 8){

                                }
//                                userSiteGet();
                            } catch (JSONException e){

                            }
                        } else {
                            progressBar.setProgress(8);
                            checkProgress();
                        }
                    }

                    public void onError(ANError error) {
                        // handle error
                    }
                });


    }
    void milestoneProgress(){
        String selectSite = "Select * FROM dm_site";
        SQLiteDatabase dbs = db.getReadableDatabase();
        Cursor cursor = dbs.rawQuery(selectSite,null);
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            do{
                AndroidNetworking.post(AppConfig.link + "data/milestone/progress")
                        .addBodyParameter("siteID", cursor.getString(cursor.getColumnIndex("id")))
                        .setTag("get milestone")
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.println(Log.DEBUG,"response_status",response.optString("status"));
                                if (response.optString("status").equals("200")){
//                                    db.addDashboard("1",response.optString("persentase"),response.optString("progress"),response.optString("activity_log"));



                                }


                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });
            }while (cursor.moveToNext());
        }
        progressBar.setProgress(7);
        dbs.close();
        cursor.close();


    }

    void getMilestoneCount(){
        String selectSite = "Select * FROM dm_site";
        SQLiteDatabase dbs = db.getReadableDatabase();
        Cursor cursor = dbs.rawQuery(selectSite,null);
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            do{
                AndroidNetworking.post(AppConfig.link + "data/milestone/count")
                        .addBodyParameter("siteID", encryptions.getDecrypt(cursor.getString(cursor.getColumnIndex("id"))))
                        .setTag("get milestone")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.println(Log.DEBUG,"response_status",response.optString("status"));
                                if (response.optString("status").equals("200")){
                                    try{
//                                        JSONObject oObject = response.getJSONObject("data");
                                        JSONArray oArray = response.getJSONArray("data");
                                        JSONObject o = null;
                                        for (int i = 0;i<oArray.length(); i++){
                                            o = oArray.getJSONObject(i);
//                                            Log.println(Log.DEBUG,"milestone_name",o.getString("milestone"));
                                            db.addMilestoneCount(encryptions.getEncrypt(o.getString("milestone_id")),
                                                    encryptions.getEncrypt(o.getString("milestone")),
                                                    encryptions.getEncrypt(o.getString("count")),
                                                    encryptions.getEncrypt(o.getString("site_id")));
                                        }




                                    }catch (JSONException e){
                                        e.printStackTrace();
                                    }



                                } else {

                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });
            }while (cursor.moveToNext());
        }
        progressBar.setProgress(2);
        dataActivity();
        dbs.close();
        cursor.close();

    }
}
