package com.kominfo.monitoring;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kominfo.monitoring.adapter.SiteListAdapter;
import com.kominfo.monitoring.adapter.TaskAdapter;
import com.kominfo.monitoring.helper.Encryptions;
import com.kominfo.monitoring.helper.Permision;
import com.kominfo.monitoring.helper.SQLiteHandler;
import com.kominfo.monitoring.helper.SessionManager;
import com.kominfo.monitoring.model.Input;
import com.kominfo.monitoring.model.MilestoneCount;
import com.kominfo.monitoring.model.SiteCount;
import com.kominfo.monitoring.model.SiteList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SiteListFragment extends Fragment {
    private SQLiteHandler db;
    List<SiteList> mcList;
    Permision permision;
    ListView lv;
    public SiteListFragment() {
        // Required empty public constructor
    }
    SessionManager session;
    Encryptions encryptions;
    Integer GET_COUNT = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_site_list, container, false);
        permision = new Permision(getActivity());
        encryptions = new Encryptions();
        lv = (ListView) view.findViewById(R.id.list_view_site);
        db = new SQLiteHandler(getActivity().getApplicationContext());
        mcList = new ArrayList<SiteList>();
        session = new SessionManager(getActivity().getApplicationContext());
        if(!permision.checkPermissionForFineLocation()){
            permision.requestPermissionForFineLocation();
        }
            if(!permision.checkPermissionForCoarseLocation()){
                permision.requestPermissionForCoarseLocation();
            }
        String selectQuesry = "SELECT Distinct site_id FROM dm_site_activity";

        SQLiteDatabase dbs = db.getReadableDatabase();
        Cursor cursor = dbs.rawQuery(selectQuesry, null);
//        Cursor cursor = db.getMilestoneCount().get;
        cursor.moveToFirst();
        do{

            String selectSite = "Select * from dm_site where id = '"+ cursor.getString(0) +"'";

            SQLiteDatabase dbx = db.getReadableDatabase();
            Cursor cursorx = dbx.rawQuery(selectSite,null);
            cursorx.moveToFirst();
            if(cursorx.getCount() > 0){
                String selectQueryx = "SELECT DISTINCT milestone_id FROM dm_site_activity where site_id = '"+ cursor.getString(0) +"'";

                SQLiteDatabase dbsx = db.getReadableDatabase();
                Cursor cursorsx = dbsx.rawQuery(selectQueryx, null);
//        Cursor cursor = db.getMilestoneCount().get;
                cursorsx.moveToFirst();
                if(cursorsx.getCount() > 0){
                    do{
                        String selectMilestone = "select * from dm_milestone_count where milestone_id ='"+cursorsx.getString(0)+"' and site_id ='"+ cursor.getString(0) +"'";

                        SQLiteDatabase dbxx = db.getReadableDatabase();
                        Cursor cursorxx = dbxx.rawQuery(selectMilestone, null);
//        Cursor cursor = db.getMilestoneCount().get;
                        cursorxx.moveToFirst();
                        if(cursorxx.getCount() > 0) {
                            do {
                                GET_COUNT += Integer.valueOf(encryptions.getDecrypt(cursorxx.getString(2)));
//                                MilestoneCount mc = new MilestoneCount(cursorx.getString(0),  encryptions.getDecrypt(cursorx.getString(1)), encryptions.getDecrypt(cursorx.getString(2)));
//                                mcList.add(mc);
                            } while (cursorxx.moveToNext());
                        }
                        dbxx.close();
                        cursorxx.close();

//            Log.println(Log.DEBUG, "milestone_name", cursor.getString(1));
                    }while (cursorsx.moveToNext());
                }

                dbsx.close();
                cursorsx.close();
                SiteList mc = new SiteList( cursorx.getString(cursorx.getColumnIndex("id")), encryptions.getDecrypt(cursorx.getString(cursorx.getColumnIndex("title"))),encryptions.getDecrypt(cursorx.getString(cursorx.getColumnIndex("latitude"))),encryptions.getDecrypt(cursorx.getString(cursorx.getColumnIndex("longitude"))), String.valueOf(GET_COUNT));
                mcList.add(mc);
                GET_COUNT = 0;
            }
            cursorx.close();

//            Log.println(Log.DEBUG, "milestone_name", cursor.getString(1));
        }while (cursor.moveToNext());
        SiteListAdapter sl = new SiteListAdapter(getActivity().getApplicationContext(), R.layout.list_site,mcList);
        cursor.close();
        lv.setAdapter(sl);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TaskListActivity taskDetail = new TaskListActivity();
                Bundle args = new Bundle();

                args.putString(TaskListActivity.SITE_ID,mcList.get(i).getId());
                taskDetail.setArguments(args);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, taskDetail, "detail");
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().setTitle(mcList.get(i).getName());
                session.setSiteName(mcList.get(i).getName());
                fragmentTransaction.commit();
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

}
