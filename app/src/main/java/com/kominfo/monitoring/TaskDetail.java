package com.kominfo.monitoring;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kominfo.monitoring.adapter.TaskAdapter;
import com.kominfo.monitoring.adapter.TaskDetailAdapter;
import com.kominfo.monitoring.helper.Encryptions;
import com.kominfo.monitoring.helper.SQLiteHandler;
import com.kominfo.monitoring.helper.SessionManager;
import com.kominfo.monitoring.model.ActivityDetail;
import com.kominfo.monitoring.model.Input;
import com.kominfo.monitoring.model.MilestoneCount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TaskDetail extends Fragment {
    final static String MILESTONE_ID = "id";
    String GET_MILESTONE_ID;
    private SQLiteHandler db;
    ArrayList<HashMap<String, String>> data_list = new ArrayList<HashMap<String, String>>();
    List<ActivityDetail> mcList;
    ListView lv;
    SessionManager session;
    public TaskDetail() {
        // Required empty public constructor
    }
    Encryptions encryptions;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_detail, container, false);
        lv = (ListView) view.findViewById(R.id.list_view2);
        db = new SQLiteHandler(getActivity().getApplicationContext());
        encryptions = new Encryptions();
        mcList  = new ArrayList<ActivityDetail>();
        Bundle args = getArguments();
        session = new SessionManager(getActivity().getApplicationContext());
        if (args != null) {
            GET_MILESTONE_ID = args.getString(MILESTONE_ID);
//            Log.println(Log.DEBUG, "Milestone ID", GET_MILESTONE_ID);
        }

        String selectQuesry = "SELECT * FROM dm_site_activity where milestone_id = '" + GET_MILESTONE_ID + "' and site_id ='"+ session.isSiteId() +"'"  ;

        SQLiteDatabase dbs = db.getReadableDatabase();
        Cursor cursor = dbs.rawQuery(selectQuesry, null);
//        Cursor cursor = db.getMilestoneCount().get;
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            do{
                Log.println(Log.DEBUG, "id", cursor.getString(1));
                ActivityDetail mc = new ActivityDetail( cursor.getString(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("activity_id")),
                        cursor.getString(cursor.getColumnIndex("site_id")),
                        cursor.getString(cursor.getColumnIndex("milestone_id")),
                        cursor.getString(cursor.getColumnIndex("kontraktor_id")),
                        cursor.getString(cursor.getColumnIndex("sub_project_id")),
                        cursor.getString(cursor.getColumnIndex("area_id")),
                        cursor.getString(cursor.getColumnIndex("project_id")),
                        encryptions.getDecrypt(cursor.getString(cursor.getColumnIndex("title"))),
                        encryptions.getDecrypt(cursor.getString(cursor.getColumnIndex("content"))),
                        cursor.getString(cursor.getColumnIndex("bobot")),
                        encryptions.getDecrypt(cursor.getString(cursor.getColumnIndex("percentage"))),
                        cursor.getString(cursor.getColumnIndex("baseline_start")),
                        cursor.getString(cursor.getColumnIndex("baseline_finish")),
                        cursor.getString(cursor.getColumnIndex("date_start")),
                        cursor.getString(cursor.getColumnIndex("date_finish")),
                        cursor.getString(cursor.getColumnIndex("date_created")),
                        encryptions.getDecrypt(cursor.getString(cursor.getColumnIndex("date_modified"))),
                        encryptions.getDecrypt(cursor.getString(cursor.getColumnIndex("status"))),
                        encryptions.getDecrypt(cursor.getString(cursor.getColumnIndex("gta_flag"))));                mcList.add(mc);
            }while (cursor.moveToNext());
            TaskDetailAdapter ta = new TaskDetailAdapter(getActivity().getApplicationContext(), R.layout.list_item_detail, mcList);
            cursor.close();
            lv.setAdapter(ta);
            lv.setOnItemClickListener( new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Log.println(Log.INFO,"get_milestone_id", String.valueOf(mcList.get(i).getTitle()));
                    InputFragment inputFragment= new InputFragment();
                    Bundle args = new Bundle();
                    args.putString(InputFragment.SITE_ACTIVITY_ID, mcList.get(i).getId());
                    args.putString(InputFragment.ACTIVITY_ID,mcList.get(i).getactivity_id());
                    args.putString(InputFragment.ACTIVITY_TITLE,mcList.get(i).getTitle() );
                    args.putString(InputFragment.MILESTONE_ID,mcList.get(i).getMilestone_id());
                    args.putString(InputFragment.SITE_ID,mcList.get(i).getSite_id());
                    args.putString(InputFragment.PERCENTACE,mcList.get(i).getPercentage());
                    args.putString(InputFragment.DATE_MODIFIED,mcList.get(i).getDate_modified());
                    args.putString(InputFragment.GTA_FLAG, mcList.get(i).getGta_flag());
                    args.putString(InputFragment.PROJECT_ID, mcList.get(i).getProject_id());
                    String selectQuesry = "SELECT * FROM dm_activity where  id = '" + mcList.get(i).getactivity_id() + "'"  ;
                    session.setSiteActivityId(mcList.get(i).getId());
                    session.setPercentage(mcList.get(i).getPercentage());
                    session.setDateModified(mcList.get(i).getDate_modified());
                    session.setGtaFlag(mcList.get(i).getGta_flag());
                    SQLiteDatabase dbs = db.getReadableDatabase();
                    Cursor cursor = dbs.rawQuery(selectQuesry, null);
//        Cursor cursor = db.getMilestoneCount().get;
                    cursor.moveToFirst();
                    session.setActivityID(mcList.get(i).getactivity_id());
                    session.setActivityName(encryptions.getDecrypt(cursor.getString(cursor.getColumnIndex("title"))));
                    session.setSessionInput("");
                    cursor.close();
                    inputFragment.setArguments(args);
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame, inputFragment, "detail");
                    getActivity().getSupportFragmentManager().popBackStack();
                    getActivity().setTitle("Task");
                    fragmentTransaction.commit();

                }
            });
        }

        // Inflate the layout for this fragment
        return view;
    }




}
