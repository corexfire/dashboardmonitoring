package com.kominfo.monitoring;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kominfo.monitoring.adapter.TaskAdapter;
import com.kominfo.monitoring.helper.Encryptions;
import com.kominfo.monitoring.helper.SQLiteHandler;
import com.kominfo.monitoring.helper.SessionManager;
import com.kominfo.monitoring.model.Input;
import com.kominfo.monitoring.model.MilestoneCount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskListActivity extends Fragment {

    private SQLiteHandler db;
    ArrayList<HashMap<String, String>> data_list = new ArrayList<HashMap<String, String>>();
    List<MilestoneCount> mcList;
    ListView lv;
    String GET_SITE_ID;
    final static String SITE_ID = "id";
    SessionManager session;
    Encryptions encryptions;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_task_list, container, false);
        encryptions = new Encryptions();
        lv = (ListView) view.findViewById(R.id.list_view);
        db = new SQLiteHandler(getActivity().getApplicationContext());
        mcList  = new ArrayList<MilestoneCount>();
        Bundle args = getArguments();
        session = new SessionManager(getActivity().getApplicationContext());
        if (args != null) {
            GET_SITE_ID = args.getString(SITE_ID);
//            Log.println(Log.DEBUG, "Milestone ID", GET_MILESTONE_ID);
        }

        String selectQuesry = "SELECT DISTINCT milestone_id FROM dm_site_activity where site_id = '"+ GET_SITE_ID +"'";

        SQLiteDatabase dbs = db.getReadableDatabase();
        Cursor cursor = dbs.rawQuery(selectQuesry, null);
//        Cursor cursor = db.getMilestoneCount().get;
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            do{
                String selectMilestone = "select * from dm_milestone_count where milestone_id ='"+cursor.getString(0)+"' and site_id ='"+ GET_SITE_ID +"'";

                SQLiteDatabase dbx = db.getReadableDatabase();
                Cursor cursorx = dbx.rawQuery(selectMilestone, null);
//        Cursor cursor = db.getMilestoneCount().get;
                cursorx.moveToFirst();
                if(cursorx.getCount() > 0) {
                    do {
                        MilestoneCount mc = new MilestoneCount(cursorx.getString(0),  encryptions.getDecrypt(cursorx.getString(1)), encryptions.getDecrypt(cursorx.getString(2)));
                        mcList.add(mc);
                    } while (cursorx.moveToNext());
                }
                dbx.close();
                cursorx.close();

//            Log.println(Log.DEBUG, "milestone_name", cursor.getString(1));
            }while (cursor.moveToNext());
        }

        dbs.close();
        TaskAdapter ta = new TaskAdapter(getActivity().getApplicationContext(), R.layout.list_item, mcList);
        cursor.close();
        lv.setAdapter(ta);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.println(Log.INFO,"get_milestone_id", String.valueOf(mcList.get(i).getMilestone_id()));
                TaskDetail taskDetail = new TaskDetail();
                Bundle args = new Bundle();
                Input input = new Input(getActivity().getApplicationContext());
                input.setMilestone(mcList.get(i).getMilestone_id(),mcList.get(i).getMilestone_name());
                session.setSiteId(GET_SITE_ID);
                session.setMilestoneName(mcList.get(i).getMilestone_name());
                session.setMilestoneID(mcList.get(i).getMilestone_id());
                args.putString(TaskDetail.MILESTONE_ID,mcList.get(i).getMilestone_id());
                taskDetail.setArguments(args);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, taskDetail, "detail");
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().setTitle(mcList.get(i).getMilestone_name());
                fragmentTransaction.commit();
            }
        });
        // Inflate the layout for this fragment
        return view;
    }
}
