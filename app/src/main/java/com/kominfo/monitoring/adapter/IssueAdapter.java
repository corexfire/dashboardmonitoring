package com.kominfo.monitoring.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kominfo.monitoring.R;
import com.kominfo.monitoring.model.Issue;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by zubai on 8/3/2017.
 */

public class IssueAdapter extends ArrayAdapter<Issue> {
    Context context;
    public IssueAdapter(Context context, int resourceId, List<Issue> items){
        super(context, resourceId, items);
        this.context = context;
    }

    private class ViewHolder{
        TextView txtSiteName;
        TextView txtTitle;
        TextView txtIssueId;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        IssueAdapter.ViewHolder holder = null;
        Issue rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.list_item_issue, null);
            holder = new IssueAdapter.ViewHolder();
            holder.txtIssueId = (TextView) convertView.findViewById(R.id.txtIssueId);
            holder.txtSiteName = (TextView) convertView.findViewById(R.id.txtSiteName);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTittle);
            convertView.setTag(holder);
        }else{
            holder = (IssueAdapter.ViewHolder) convertView.getTag();
        }

        Log.println(Log.DEBUG,"TITLE",rowItem.getTitle());

        holder.txtSiteName.setText(rowItem.getSite_name());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.txtIssueId.setText(rowItem.getId());

        return convertView;
    }
}
