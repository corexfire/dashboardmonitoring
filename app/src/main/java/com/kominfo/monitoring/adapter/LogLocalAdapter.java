package com.kominfo.monitoring.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kominfo.monitoring.R;
import com.kominfo.monitoring.model.ActivityDetail;
import com.kominfo.monitoring.model.ActivityLogLocal;

import java.util.List;

/**
 * Created by Corexfire on 7/11/2017.
 */

public class LogLocalAdapter extends ArrayAdapter<ActivityLogLocal> {
    Context context;
    public LogLocalAdapter(Context context, int resourceId,
                             List<ActivityLogLocal> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    private class ViewHolder{
        TextView textTitle;
        ImageView imgLocal;
        ImageView imgUpload;
        TextView textContent;
        TextView textDateModified;
        Button btnUpload;
        TextView textVerified;
        TextView textUploaded;
        TextView textRejected;

    }
    public View getView(int position, View convertView, ViewGroup parent){
        LogLocalAdapter.ViewHolder holder = null;
        ActivityLogLocal rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.list_activity_log, null);
            holder = new LogLocalAdapter.ViewHolder();
            holder.textTitle = (TextView) convertView.findViewById(R.id.textTitle);
            holder.textContent = (TextView) convertView.findViewById(R.id.textContent);
            holder.textDateModified = (TextView) convertView.findViewById(R.id.textDateModified);
            holder.imgLocal = (ImageView) convertView.findViewById(R.id.imgLocal);
            holder.imgUpload = (ImageView) convertView.findViewById(R.id.imgUpload);
            holder.btnUpload = (Button) convertView.findViewById(R.id.btnUpload);
            holder.textRejected = (TextView) convertView.findViewById(R.id.textRejected);
            holder.textUploaded = (TextView) convertView.findViewById(R.id.textUploaded);
            holder.textVerified = (TextView) convertView.findViewById(R.id.textVerified);
            convertView.setTag(holder);
        }else{
            holder = (LogLocalAdapter.ViewHolder) convertView.getTag();
        }

        holder.textTitle.setText(rowItem.getTitle());
        holder.textContent.setText(rowItem.getContent());
        holder.textDateModified.setText(rowItem.getDate_modified());
        if(rowItem.getStatus().equals("3")){
            holder.imgLocal.setVisibility(View.VISIBLE);
            holder.imgUpload.setVisibility(View.GONE);
            holder.btnUpload.setVisibility(View.VISIBLE);
            holder.textRejected.setVisibility(View.GONE);
            holder.textUploaded.setVisibility(View.GONE);
            holder.textVerified.setVisibility(View.GONE);
        }else{
            switch (rowItem.getStatus()){
                case "0":
                    holder.imgLocal.setVisibility(View.GONE);
                    holder.imgUpload.setVisibility(View.VISIBLE);
                    holder.btnUpload.setVisibility(View.GONE);
                    holder.textRejected.setVisibility(View.GONE);
                    holder.textUploaded.setVisibility(View.VISIBLE);
                    holder.textVerified.setVisibility(View.GONE);
                    break;
                case "1":
                    holder.imgLocal.setVisibility(View.GONE);
                    holder.imgUpload.setVisibility(View.VISIBLE);
                    holder.btnUpload.setVisibility(View.GONE);
                    holder.textRejected.setVisibility(View.GONE);
                    holder.textUploaded.setVisibility(View.GONE);
                    holder.textVerified.setVisibility(View.VISIBLE);
                    break;
                case "2":
                    holder.imgLocal.setVisibility(View.GONE);
                    holder.imgUpload.setVisibility(View.VISIBLE);
                    holder.btnUpload.setVisibility(View.GONE);
                    holder.textRejected.setVisibility(View.VISIBLE);
                    holder.textUploaded.setVisibility(View.GONE);
                    holder.textVerified.setVisibility(View.GONE);
                    break;
            }
        }
        return convertView;
    }
}
