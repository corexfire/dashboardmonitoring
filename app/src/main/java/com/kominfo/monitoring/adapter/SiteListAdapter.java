package com.kominfo.monitoring.adapter;

/**
 * Created by zubai on 7/27/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kominfo.monitoring.R;
import com.kominfo.monitoring.model.SiteList;

import org.w3c.dom.Text;

import java.util.List;

public class SiteListAdapter extends ArrayAdapter<SiteList> {
    Context context;
    public SiteListAdapter(Context context, int resourceId,
                           List<SiteList> items){
        super(context, resourceId, items);
        this.context = context;
    }

    private class ViewHolder{
        TextView id;
        TextView name;
        TextView coordinate;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        SiteListAdapter.ViewHolder holder = null;
        SiteList rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.list_site, null);
            holder = new SiteListAdapter.ViewHolder();
            holder.id = (TextView) convertView.findViewById(R.id.site_id);
            holder.name = (TextView) convertView.findViewById(R.id.site_name);
            holder.coordinate = (TextView) convertView.findViewById(R.id.site_coordinate);
            convertView.setTag(holder);
        }else{
            holder = (SiteListAdapter.ViewHolder) convertView.getTag();
        }



        holder.id.setText(rowItem.getId());
        holder.name.setText(rowItem.getName() + " ("+rowItem.getSiteCount()+")");
        holder.coordinate.setText("Lat: " + rowItem.getLatitude() + " - Long: "+rowItem.getLongitude());
        return convertView;
    }

}
