package com.kominfo.monitoring.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kominfo.monitoring.R;
import com.kominfo.monitoring.model.MilestoneCount;

import java.util.List;

/**
 * Created by Corexfire on 6/22/2017.
 */

public class TaskAdapter extends ArrayAdapter<MilestoneCount> {
        Context context;

    public TaskAdapter(Context context, int resourceId,
                         List<MilestoneCount> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    private class ViewHolder{
        TextView milestone_name;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder holder = null;
        MilestoneCount rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.milestone_name = (TextView) convertView.findViewById(R.id.milestone_list);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.milestone_name.setText(rowItem.getMilestone_name() + " ("+ rowItem.getCount() +")");
        return convertView;
    }
}
