package com.kominfo.monitoring.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kominfo.monitoring.R;
import com.kominfo.monitoring.model.ActivityDetail;
import com.kominfo.monitoring.model.MilestoneCount;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Corexfire on 6/22/2017.
 */

public class TaskDetailAdapter extends ArrayAdapter<ActivityDetail> {
    Context context;

    public TaskDetailAdapter(Context context, int resourceId,
                       List<ActivityDetail> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    private class ViewHolder{

        TextView textTitle;
        TextView textPersentase;
        TextView textContent;
        TextView textDateModified;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder holder = null;
        ActivityDetail rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.list_item_detail, null);
            holder = new ViewHolder();
            holder.textTitle = (TextView) convertView.findViewById(R.id.textTitle);
            holder.textPersentase = (TextView) convertView.findViewById(R.id.textPersentase);
            holder.textContent = (TextView) convertView.findViewById(R.id.textContent);
            holder.textDateModified = (TextView) convertView.findViewById(R.id.textDateModified);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textTitle.setText(rowItem.getTitle());
        holder.textPersentase.setText(rowItem.getPercentage() + "%");
        holder.textContent.setText(rowItem.getContent());
        holder.textDateModified.setText(rowItem.getDate_modified());
        return convertView;
    }
}
