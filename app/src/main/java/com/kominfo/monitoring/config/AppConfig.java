package com.kominfo.monitoring.config;

/**
 * Created by Corexfire on 7/1/2017.
 */

public class AppConfig {
    public static String link = "https://teknusa.com/api/v1/";
    public static String linkV2 = "https://teknusa.com/api/v2/";
    public static String internalStorageName = "com.kominfo.monitoring";
}
