package com.kominfo.monitoring;

import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.kominfo.monitoring.config.AppConfig;
import com.kominfo.monitoring.helper.CheckConnection;
import com.kominfo.monitoring.helper.GPSTracker;
import com.kominfo.monitoring.helper.SQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Corexfire on 7/21/2017.
 */

public class dasboardService extends Service {
    SQLiteHandler db;
    Integer count = 1;
    Timer T=new Timer();
    String id;
    GPSTracker gps;
    String NFCID;
    String GET_USER_ID;
    String GET_ID;
    String new_id;
    CheckConnection connectionCheck;
    Activity activity;
    public Context context = this;
    public Handler handler = null;
    public static Runnable runnable = null;
    private NotificationManager mNM;

    // Unique Identification Number for the Notification.
    // We use it on Notification start, and to cancel it.
    private int NOTIFICATION = R.string.local_service_started;
    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        dasboardService getService() {
            return dasboardService.this;
        }
    }

    @Override
    public void onCreate() {
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        // Display a notification about us starting.
        showNotification();
        Toast.makeText(this, "Service created!", Toast.LENGTH_LONG).show();
        db = new SQLiteHandler(getApplicationContext());
        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                if(isInternetAvailable()){
                     GET_ID = db.getActivityLoglastID().get("id");
                   GET_USER_ID = db.getUserDetails().get("uid");
                    AndroidNetworking.post(AppConfig.link + "data/activity/log/update")
                            .addBodyParameter("user_id", GET_USER_ID)
                            .addBodyParameter("id",GET_ID)
                            .setTag("get data from service")
                            .setPriority(Priority.MEDIUM)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    if (response.optString("status").equals("200")) {
                                        Log.println(Log.DEBUG,"RESPONSE", "200");
                                        try {
                                            JSONArray arr = response.getJSONArray("data");
                                            for (int i =0; i<arr.length();i++) {
                                                JSONObject o = arr.getJSONObject(i);
                                                Log.println(Log.DEBUG, "get_id", o.getString("id"));
                                                new_id = o.getString("id");
                                                db.addActivityLog(o.getString("id"),
                                                        o.getString("user_id"),
                                                        o.getString("site_activity_id"),
                                                        o.getString("milestone_id"),
                                                        o.getString("site_id"),
                                                        o.getString("activity_id"),
                                                        o.getString("title"),
                                                        o.getString("content"),
                                                        o.getString("percentage"),
                                                        o.getString("photo"),
                                                        o.getString("longitude"),
                                                        o.getString("latitude"),
                                                        o.getString("date_created"),
                                                        o.getString("date_uploaded"),
                                                        o.getString("date_modified"),
                                                        o.getString("status"),
                                                        o.getString("id_modified"), "online"
                                                );
                                                new CountDownTimer(3000, 1000) {
                                                    public void onTick(long millisUntilFinished) {
                                                        AndroidNetworking.post(AppConfig.link + "data/activity/pict/update")
                                                                .addBodyParameter("id", new_id)
                                                                .setTag("get picture")
                                                                .setPriority(Priority.MEDIUM)
                                                                .build()
                                                                .getAsJSONObject(new JSONObjectRequestListener() {
                                                                    @Override
                                                                    public void onResponse(JSONObject response) {
                                                                        try{
                                                                            JSONArray arr = response.getJSONArray("data");
//                                    progressBar.setMax(arr.length());
                                                                            for (int i =0; i<arr.length();i++) {
                                                                                JSONObject o = arr.getJSONObject(i);
                                                                                final String get_id_new = o.getString("id");
                                                                                final String get_activity_log_id_new = o.getString("activity_log_id");
                                                                                final String get_photo_new = o.getString("photo");
                                                                                final String get_date_created_new = o.getString("date_created");
                                                                                new CountDownTimer(3000, 1000) {
                                                                                    public void onTick(long millisUntilFinished) {
//                                                                                        db.addActivityPict(get_id_new, get_activity_log_id_new, get_photo_new,
//                                                                                                get_date_created_new);

                                                                                    }

                                                                                    public void onFinish() {
                                                                                        File mediaStorageDir = new File(
                                                                                                Environment.getExternalStorageDirectory()
                                                                                                        + File.separator
                                                                                                        + "bts_kominfo"
                                                                                                        + File.separator
                                                                                                        + "picture"
                                                                                                        + File.separator
                                                                                                        + GET_ID
                                                                                        );
                                                                                        if (!mediaStorageDir.exists()) {
                                                                                            mediaStorageDir.mkdirs();
                                                                                        }


                                                                                        //TODO download picture from server
                                                                                        AndroidNetworking.download("https://bpppti.teknusa.com/assets/uploads/activity_log/" + get_activity_log_id_new + "/" + get_photo_new, mediaStorageDir.getPath(), get_photo_new)
                                                                                                .setTag("Download Picture")
                                                                                                .setPriority(Priority.IMMEDIATE)
                                                                                                .build()
                                                                                                .setDownloadProgressListener(new DownloadProgressListener() {
                                                                                                    @Override
                                                                                                    public void onProgress(long bytesDownloaded, long totalBytes) {

                                                                                                    }
                                                                                                })
                                                                                                .startDownload(new DownloadListener() {
                                                                                                    @Override
                                                                                                    public void onDownloadComplete() {
                                                                                                        Log.println(Log.DEBUG, "download", "success");
                                                                                                    }

                                                                                                    @Override
                                                                                                    public void onError(ANError anError) {
                                                                                                        Log.println(Log.DEBUG, "download", "failed");
                                                                                                    }
                                                                                                });
                                                                                    }

                                                                                }.start();
                                                                            }
                                                                        }catch (JSONException e){
                                                                            e.printStackTrace();
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onError(ANError anError) {

                                                                    }
                                                                });
                                                    }
                                                    public void onFinish() {

                                                    }
                                                }.start();
                                            }



                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }else{
                                        Log.println(Log.DEBUG,"RESPONSE", "400");
                                    }
                                }

                                @Override
                                public void onError(ANError anError) {

                                }
                            });
                }
//                     Toast.makeText(context, "connected to network", Toast.LENGTH_LONG).show();
                else{
                        Log.println(Log.DEBUG,"NO DATA", "NO DATA UPDATED");
                }
//                    Toast.makeText(context, " not connected to network", Toast.LENGTH_LONG).show();

                handler.postDelayed(runnable, 30000);
            }
        };

        handler.postDelayed(runnable, 30000);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        /* IF YOU WANT THIS SERVICE KILLED WITH THE APP THEN UNCOMMENT THE FOLLOWING LINE */
        handler.removeCallbacks(runnable);
        mNM.cancel(NOTIFICATION);
        Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
    }

    private void showNotification() {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = getText(R.string.local_service_started);

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, LoginActivity.class), 0);

        // Set the info for the views that show in the notification panel.
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.logo)  // the status icon
                .setTicker(text)  // the status text
                .setWhen(System.currentTimeMillis())  // the time stamp
                .setContentTitle(getText(R.string.app_name))  // the label of the entry
                .setContentText(text)  // the contents of the entry
                .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
                .build();

        // Send the notification.
        // We use a string id because it is a unique number.  We use it later to cancel.
        mNM.notify(R.string.local_service_started, notification);
    }

    @Override
    public void onStart(Intent intent, int startid) {
        Toast.makeText(this, "Service started by user.", Toast.LENGTH_LONG).show();
    }

    private boolean isInternetAvailable(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
