package com.kominfo.monitoring.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.kominfo.monitoring.LoginActivity;
import com.kominfo.monitoring.helper.CheckConnection;

/**
 * Created by Corexfire on 7/2/2017.
 */



public class ConnectionReceiver extends BroadcastReceiver  {
    @Override
    public void onReceive(Context context, Intent intent) {



            if (isConnected(context)) {
                LoginActivity.isConnect = "true";
            } else {
                LoginActivity.isConnect = "false";
            }

    }



    public boolean isConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        return isConnected;
    }
}
