package com.kominfo.monitoring.helper;

import se.simbio.encryption.Encryption;

/**
 * Created by zubai on 8/3/2017.
 */

public class Encryptions {
    Encryption encryption = Encryption.getDefault("c0rExf!re", "b4ngUn!nd0", new byte[16]);


    public String getEncrypt(String message){
        String encrypted = encryption.encryptOrNull(message);
        return  encrypted;
    }

    public String getDecrypt(String message){
        String decrypted = encryption.decryptOrNull(message);
        return  decrypted;
    }
}
