package com.kominfo.monitoring.helper;

import java.io.File;

/**
 * Created by zubai on 8/9/2017.
 */

public class FileDeleter {
    public void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        }

        fileOrDirectory.delete();
    }
}
