package com.kominfo.monitoring.helper;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by zubai on 8/1/2017.
 */

public class GetVersion  {


    public String getVersionName(Context context){
        String version = "Version : ";
        try{
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(
                    context.getPackageName(), 0);
             version += info.versionName;
        }catch (Exception e){
             version += "unknows";
            e.printStackTrace();
        }
        return version;
    }


}
