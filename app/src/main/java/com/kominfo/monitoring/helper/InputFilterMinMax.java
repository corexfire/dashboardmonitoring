package com.kominfo.monitoring.helper;

/**
 * Created by Corexfire on 7/19/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Gravity;
import android.widget.Toast;

public class InputFilterMinMax implements InputFilter {
    private int min, max;

    Context context;


    public InputFilterMinMax(Context context, int min, int max) {
        this.min = min;
        this.max = max;
        this.context = context;
    }

    public InputFilterMinMax(Context context, String min, String max) {
        this.context = context;
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            int input = Integer.parseInt(dest.toString() + source.toString());
            if (isInRange(min, max, input)) {

                    return null;
            }
            else{

                    Toast toast = Toast.makeText(context, "Percentage value : 1 - 100", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
                    toast.show();

//                return null;
            }
        } catch (NumberFormatException nfe) {

        }
        return "";
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}