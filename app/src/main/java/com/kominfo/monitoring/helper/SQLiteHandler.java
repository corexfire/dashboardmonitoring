package com.kominfo.monitoring.helper;

/**
 * Created by Corexfire on 6/29/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kominfo.monitoring.model.MilestoneCount;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SQLiteHandler extends SQLiteOpenHelper {
    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 7;

    // Database Name
    private static final String DATABASE_NAME = "kominfoMonitoring";
    private static final String TABLE_USER = "dm_user";
    private static final String TABLE_SITE_ACTIVITY = "dm_site_activity";
    private static final String TABLE_SITE = "dm_site";
    private static final String TABLE_ACTIVITY = "dm_activity";
    private static final String TABLE_ACTIVITY_LOG = "dm_activity_log";
    private static final String TABLE_ACTIVITY_PICT = "dm_activity_pict";
    private static final String TABLE_ACTIVITY_PICT_LOCAL = "dm_activity_pict_local";
    private static final String TABLE_ACTIVITY_LOG_LOCAL = "dm_activity_log_local";
    private static final String TABLE_USER_LOG = "dm_user_log";
    private static final String TABLE_MILESTONE = "dm_milestone";
    private static final String TABLE_DASHBOARD = "dm_dashboard";
    private static final String TABLE_MILESTONE_COUNT = "dm_milestone_count";
    private static final String TABLE_TEMP_ACTIVITY_LOG_ID = "dm_temp_activity_id";
    private static final String TABLE_INPUT_TEMP = "dm_temp_input";
    private static final String TABLE_TAKE_PICT_TEMP = "dm_take_pict_temp";
    private static final String TABLE_ISSUE = "dm_issue";
    private static final String TABLE_ISSUE_LOCAL = "dm_issue_local";
    private static final String TABLE_ISSUE_PICT = "dm_issue_pict";
    private static final String TABLE_ISSUE_PICT_TEMP = "dm_issue_pict_temp";

    private static final String KEY_ISSUE_ID = "id";
    private static final String KEY_ISSUE_USER_ID = "user_id";
    private static final String KEY_ISSUE_SITE_ID = "site_id";
    private static final String KEY_ISSUE_MILESTONE_ID = "milestone_id";
    private static final String KEY_ISSUE_ACTIVITY_ID = "activity_id";
    private static final String KEY_ISSUE_TITLE= "title";
    private static final String KEY_ISSUE_CONTENT = "content";
    private static final String KEY_ISSUE_STATUS = "status";

    private static final String KEY_TAKE_PICT_TEMP_SESSION_ID = "session_id";
    private static final String KEY_TAKE_PICT_TEMP_PICTURE = "picture";

    private static final String KEY_INPUT_TEMP_ID = "id";
    private static final String KEY_INPUT_TEMP_PHOTO = "photo";

    private static  final String KEY_TEMP_ACTIVITY_LOG_ID = "id";

    //TODO TABLE MILESTONE COUNT
    private static final String KEY_COUNT_MILESTONE_ID = "milestone_id";
    private static final String KEY_COUNT_MILESTONE_NAME = "milestone_name";
    private static final String KEY_COUNT_MILESTONE_COUNT = "count";


    //dashboard table
    //TODO TABLE DASHBOARD
    private static final String KEY_DASHBOARD = "id";
    private static final String KEY_DASHBOARD_PERSENTASE = "persentase";
    private static final String KEY_DASHBOARD_PROGRESS = "progress";
    private static final String KEY_DASHBOARD_ACTIVITY_LOG = "activity_log";

    // user table
    // TODO TABLE_USER
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_USER_SITE_ID = "site_id";
    private static final String KEY_UID = "uid";

    // site_activity table
    //TODO TABLE_SITE_ACTIVITY
    private static final String KEY_SITE_ACTIVITY_ID = "id";
    private static final String KEY_SITE_ACTIVITY_ACT_ID = "activity_id";
    private static final String KEY_SITE_ACTIVITY_SITE_ID = "site_id";
    private static final String KEY_SITE_ACTIVITY_MILESTONE_ID = "milestone_id";
    private static final String KEY_SITE_ACTIVITY_KONTRAKTOR_ID = "kontraktor_id";
    private static final String KEY_SITE_ACTIVITY_SUB_PROJECT_ID = "sub_project_id";
    private static final String KEY_SITE_ACTIVITY_AREA_ID = "area_id";
    private static final String KEY_SITE_ACTIVITY_PROJECT_ID = "project_id";
    private static final String KEY_SITE_ACTIVITY_TITLE = "title";
    private static final String KEY_SITE_ACTIVITY_CONTENT = "content";
    private static final String KEY_SITE_ACTIVITY_BOBOT = "bobot";
    private static final String KEY_SITE_ACTIVITY_PERCENTAGE = "percentage";
    private static final String KEY_SITE_ACTIVITY_BASELINE_START = "baseline_start";
    private static final String KEY_SITE_ACTIVITY_BASELINE_FINISH = "baseline_finish";
    private static final String KEY_SITE_ACTIVITY_DATE_START = "date_start";
    private static final String KEY_SITE_ACTIVITY_DATE_FINISH = "date_finish";
    private static final String KEY_SITE_ACTIVITY_DATE_CREATED = "date_created";
    private static final String KEY_SITE_ACTIVITY_DATE_MODIFIED = "date_modified";
    private static final String KEY_SITE_ACTIVITY_STATUS = "status";
    private static final String KEY_SITE_ACTIVITY_FLAG = "gta_flag";

    //site table
    //TODO TABLE_SITE
    private static final String KEY_SITE_ID = "id";
    private static final String KEY_SITE_KONTRAKTOR_ID = "kontraktor_id";
    private static final String KEY_SITE_SUB_PROJECT_ID = "sub_project_id";
    private static final String KEY_SITE_AREA_ID = "area_id";
    private static final String KEY_SITE_PROJECT_ID = "project_id";
    private static final String KEY_SITE_REAL_ID = "real_id";
    private static final String KEY_SITE_PROVINSI = "provinsi";
    private static final String KEY_SITE_KOTA = "kota";
    private static final String KEY_SITE_KECAMATAN = "kecamatan";
    private static final String KEY_SITE_DESA = "desa";
    private static final String KEY_SITE_TITLE = "title";
    private static final String KEY_SITE_CONTENT = "content";
    private static final String KEY_SITE_LONGITUDE = "longitude";
    private static final String KEY_SITE_LATITUDE = "latitude";
    private static final String KEY_SITE_BOBOT = "bobot";
    private static final String KEY_SITE_DATE_CREATED = "date_created";
    private static final String KEY_SITE_DATE_MODIFIED = "date_modified";
    private static final String KEY_SITE_STATUS = "status";


    // activity table
    //TODO TABLE_ACTIVITY
    private static final String KEY_ACTIVITY_ID = "id";
    private static final String KEY_ACTIVITY_AREA_ID = "area_id";
    private static final String KEY_ACTIVITY_MILESTONE_ID = "milestone_id";
    private static final String KEY_ACTIVITY_TITLE = "title";
    private static final String KEY_ACTIVITY_PETUNJUK_FOTO = "petunjuk_foto";

    // activity_log table
    //TODO TABLE_ACTIVITY_LOG
    private static final String KEY_ACTIVITY_LOG_ID = "id";
    private static final String KEY_ACTIVITY_LOG_USER_ID = "user_id";
    private static final String KEY_ACTIVITY_LOG_SITE_ACTIVITY_ID = "site_activity_id";
    private static final String KEY_ACTIVITY_LOG_MILESTONE_ID = "milestone_id";
    private static final String KEY_ACTIVITY_LOG_SITE_ID = "site_id";
    private static final String KEY_ACTIVITY_LOG_ACTIVITY_ID = "activity_id";
    private static final String KEY_ACTIVITY_LOG_TITLE = "title";
    private static final String KEY_ACTIVITY_LOG_CONTENT = "content";
    private static final String KEY_ACTIVITY_LOG_PERCENTAGE = "percentage";
    private static final String KEY_ACTIVITY_LOG_PHOTO ="photo";
    private static final String KEY_ACTIVITY_LOG_LONGITUDE = "longitude";
    private static final String KEY_ACTIVITY_LOG_LATITUDE = "latitude";
    private static final String KEY_ACTIVITY_LOG_DATE_CREATED = "date_created";
    private static final String KEY_ACTIVITY_LOG_DATE_UPLOADED = "date_uploaded";
    private static final String KEY_ACTIVITY_LOG_DATE_MODIFIED = "date_modified";
    private static final String KEY_ACTIVITY_LOG_STATUS = "status";
    private static final String KEY_ACTIVITY_LOG_ID_MODIFIED = "id_modified";
    private static final String KEY_ACTIVITY_LOG_STATE = "state";


    // activity_pict
    //TODO TABLE_ACTIVITY_PICT
    private static final String KEY_ACTIVITY_PICT_ID = "id";
    private static final String KEY_ACTIVITY_PICT_ACTIVITY_LOG_ID = "activity_log_id";
    private static final String KEY_ACTIVITY_PICT_PHOTO = "photo";
    private static final String KEY_ACTIVITY_PICT_DATE_CREATED = "date_created";
    private static final String KEY_ACTIVITY_PICT_BASE_64 = "s_photo";


    // activity_log local table
    //TODO TABLE_ACTIVITY_LOG_LOCAL
    private static final String KEY_LOCAL_ACTIVITY_LOG_ID = "id";
    private static final String KEY_LOCAL_ACTIVITY_LOG_USER_ID = "user_id";
    private static final String KEY_LOCAL_ACTIVITY_LOG_SITE_ACTIVITY_ID = "site_activity_id";
    private static final String KEY_LOCAL_ACTIVITY_LOG_MILESTONE_ID = "milestone_id";
    private static final String KEY_LOCAL_ACTIVITY_LOG_SITE_ID = "site_id";
    private static final String KEY_LOCAL_ACTIVITY_LOG_ACTIVITY_ID = "activity_id";
    private static final String KEY_LOCAL_ACTIVITY_LOG_TITLE = "title";
    private static final String KEY_LOCAL_ACTIVITY_LOG_CONTENT = "content";
    private static final String KEY_LOCAL_ACTIVITY_LOG_PERCENTAGE = "percentage";
    private static final String KEY_LOCAL_ACTIVITY_LOG_PHOTO ="photo";
    private static final String KEY_LOCAL_ACTIVITY_LOG_LONGITUDE = "longitude";
    private static final String KEY_LOCAL_ACTIVITY_LOG_LATITUDE = "latitude";
    private static final String KEY_LOCAL_ACTIVITY_LOG_DATE_CREATED = "date_created";
    private static final String KEY_LOCAL_ACTIVITY_LOG_DATE_UPLOADED = "date_uploaded";
    private static final String KEY_LOCAL_ACTIVITY_LOG_DATE_MODIFIED = "date_modified";
    private static final String KEY_LOCAL_ACTIVITY_LOG_STATUS = "status";
    private static final String KEY_LOCAL_ACTIVITY_LOG_ID_MODIFIED = "id_modified";
    private static final String KEY_LOCAL_PARENT_ID ="parent_id";

    //user_log table
    //TODO TABLE_USER_LOG
    private static final String KEY_LOCAL_USER_LOG_ID = "id";
    private static final String KEY_LOCAL_USER_LOG_USER_ID = "user_id";
    private static final String KEY_LOCAL_USER_LOG_TYPE_ID = "type_id";
    private static final String KEY_LOCAL_USER_LOG_LOG_DESCRIPTION = "log_description";
    private static final String KEY_LOCAL_USER_LOG_DATE_CREATED = "date_created";
    private static final String KEY_LOCAL_USER_LOG_READ_FLAG = "read_flag";

    //milestone table
    //TODO TABLE_MILESTONE
    private static final String KEY_MILESTONE_ID = "id";
    private static final String KEY_MILESTONE_PARENT_ID = "parent_id";
    private static final String KEY_MILESTONE_NAME = "name";

    private static  final String DEVICE_NAME = "device_name";
    private static  final String DEVICE_ADDRESS = "device_address";

    //TODO TABLE_TEMP_PICT_ISSUE
    private static final String KEY_ISSUE_PICT_ID_TEMP = "id";
    private static final String KEY_ISSUE_PICT_TEMP = "pict";
    private static final String KEY_ISSUE_DATE_CREATED_TEMP = "date_created";

    //TODO TABLE_PICT_ISSUE
    private static final String KEY_ISSUE_PICT_ID = "id";
    private static final String KEY_ISSUE_PICT = "pict";
    private static final String KEY_ISSUE_DATE_CREATED = "date_created";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ISSUE = "CREATE TABLE IF NOT EXISTS " + TABLE_ISSUE + "("
                + KEY_ISSUE_ID + " INTEGER PRIMARY KEY, "
                + KEY_ISSUE_USER_ID + " TEXT, "
                + KEY_ISSUE_SITE_ID + " TEXT, "
                + KEY_ISSUE_MILESTONE_ID + " TEXT, "
                + KEY_ISSUE_ACTIVITY_ID + " TEXT, "
                + KEY_ISSUE_TITLE + " TEXT, "
                + KEY_ISSUE_CONTENT + " TEXT, "
                + KEY_ISSUE_STATUS + " TEXT, "
                + KEY_ISSUE_DATE_CREATED + " TEXT "
                +")";
        db.execSQL(CREATE_ISSUE);

        String CREATE_ISSUE_PICT = "CREATE TABLE IF NOT EXISTS " + TABLE_ISSUE_PICT +" ("
                + KEY_ISSUE_PICT_ID + "TEXT, "
                + KEY_ISSUE_ID + " TEXT, "
                + KEY_ISSUE_PICT + " TEXT, "
                + KEY_ISSUE_DATE_CREATED + " TEXT"
                +")";
        db.execSQL(CREATE_ISSUE_PICT);

        String CREATE_ISSUE_PICT_TEMP = "CREATE TABLE IF NOT EXISTS  " + TABLE_ISSUE_PICT_TEMP +" ("
                + KEY_ISSUE_PICT_ID_TEMP + "TEXT, "
                + KEY_ISSUE_ID + " TEXT, "
                + KEY_ISSUE_PICT_TEMP + " TEXT, "
                + KEY_ISSUE_DATE_CREATED_TEMP + " TEXT"
                +")";
        db.execSQL(CREATE_ISSUE_PICT_TEMP);

        String CREATE_ISSUE_LOCAL = "CREATE TABLE IF NOT EXISTS  " + TABLE_ISSUE_LOCAL + "("
                + KEY_ISSUE_ID + " INTEGER PRIMARY KEY, "
                + KEY_ISSUE_USER_ID + " TEXT, "
                + KEY_ISSUE_SITE_ID + " TEXT, "
                + KEY_ISSUE_MILESTONE_ID + " TEXT, "
                + KEY_ISSUE_ACTIVITY_ID + " TEXT, "
                + KEY_ISSUE_TITLE + " TEXT, "
                + KEY_ISSUE_CONTENT + " TEXT, "
                + KEY_ISSUE_STATUS + " TEXT "
                +")";
        db.execSQL(CREATE_ISSUE_LOCAL);

        String CREATE_TEMP_ID = "CREATE TABLE IF NOT EXISTS  "+ TABLE_TEMP_ACTIVITY_LOG_ID + " ("
                + KEY_TEMP_ACTIVITY_LOG_ID + " TEXT"
                +")";

        db.execSQL(CREATE_TEMP_ID);

        String CREATE_TAKE_PICT_TEMP = "CREATE TABLE IF NOT EXISTS  "+ TABLE_TAKE_PICT_TEMP + " ("
                + KEY_TAKE_PICT_TEMP_SESSION_ID + " TEXT, "
                + KEY_TAKE_PICT_TEMP_PICTURE + " TEXT"
                +")";
        db.execSQL(CREATE_TAKE_PICT_TEMP);

        //TODO CREATE_INPUT_TEMP
        String CREATE_INPUT_TEMP = "CREATE TABLE IF NOT EXISTS  " + TABLE_INPUT_TEMP + "("
                + KEY_INPUT_TEMP_ID + " TEXT, "
                + KEY_INPUT_TEMP_PHOTO + " TEXT"
                +")";
        db.execSQL(CREATE_INPUT_TEMP);

        //TODO CREATE_MILESTONE_COUNT_TABLE
        String CREATE_MILESTONE_COUNT = "CREATE TABLE IF NOT EXISTS  "+ TABLE_MILESTONE_COUNT +" ("
                + KEY_COUNT_MILESTONE_ID + " TEXT,  "
                + KEY_COUNT_MILESTONE_NAME + " TEXT, "
                + KEY_COUNT_MILESTONE_COUNT + " TEXT, "
                + KEY_ACTIVITY_LOG_SITE_ID + " TEXT "
                +")";
        db.execSQL(CREATE_MILESTONE_COUNT);

        //TODO CREATE_DASHBOARD_TABLE
        String CREATE_DASHBOARD_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_DASHBOARD +  " (" + KEY_DASHBOARD + " INTEGER PRIMARY KEY, "
                + KEY_DASHBOARD_PERSENTASE +  " TEXT, "
                + KEY_DASHBOARD_PROGRESS + " TEXT, "
                + KEY_DASHBOARD_ACTIVITY_LOG + " TEXT"
                + " )";
        db.execSQL(CREATE_DASHBOARD_TABLE);

        //TODO CREATE_USER_TABLE
        String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_USER + "("
                + KEY_ID + " TEXT," + KEY_UID + " TEXT, " + KEY_NAME + " TEXT,"
                + KEY_USERNAME + " TEXT UNIQUE," + KEY_USER_SITE_ID + " TEXT" + ")";
        db.execSQL(CREATE_USER_TABLE);

        String CREATE_SITE_ACTIVITY_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_SITE_ACTIVITY + "("
                + KEY_SITE_ACTIVITY_ID + " TEXT, "
                + KEY_SITE_ACTIVITY_ACT_ID + " TEXT, "
                + KEY_SITE_ACTIVITY_SITE_ID + " TEXT, "
                + KEY_SITE_ACTIVITY_MILESTONE_ID + " TEXT, "
                + KEY_SITE_ACTIVITY_KONTRAKTOR_ID + " TEXT, "
                + KEY_SITE_ACTIVITY_SUB_PROJECT_ID + " TEXT, "
                + KEY_SITE_ACTIVITY_AREA_ID + " TEXT, "
                + KEY_SITE_ACTIVITY_PROJECT_ID + " TEXT, "
                + KEY_SITE_ACTIVITY_TITLE + " TEXT, "
                + KEY_SITE_ACTIVITY_CONTENT + " TEXT, "
                + KEY_SITE_ACTIVITY_BOBOT + " TEXT, "
                + KEY_SITE_ACTIVITY_PERCENTAGE + " TEXT, "
                + KEY_SITE_ACTIVITY_BASELINE_START + " TEXT, "
                + KEY_SITE_ACTIVITY_BASELINE_FINISH + " TEXT, "
                + KEY_SITE_ACTIVITY_DATE_START + " TEXT, "
                + KEY_SITE_ACTIVITY_DATE_FINISH + " TEXT, "
                + KEY_SITE_ACTIVITY_DATE_CREATED + " TEXT, "
                + KEY_SITE_ACTIVITY_DATE_MODIFIED + " TEXT, "
                + KEY_SITE_ACTIVITY_STATUS + " TEXT, "
                + KEY_SITE_ACTIVITY_FLAG + " TEXT "
                +")";
        db.execSQL(CREATE_SITE_ACTIVITY_TABLE);

        //TODO CREATE_SITE_TABLE
        String CREATE_SITE_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_SITE + "("
                + KEY_SITE_ID + " TEXT, "
                + KEY_SITE_KONTRAKTOR_ID + " TEXT, "
                + KEY_SITE_ACTIVITY_SUB_PROJECT_ID + " TEXT, "
                + KEY_SITE_AREA_ID + " TEXT, "
                + KEY_SITE_PROJECT_ID + " TEXT, "
                + KEY_SITE_REAL_ID + " TEXT, "
                + KEY_SITE_PROVINSI + " TEXT, "
                + KEY_SITE_KOTA + " TEXT, "
                + KEY_SITE_KECAMATAN + " TEXT, "
                + KEY_SITE_DESA + " TEXT, "
                + KEY_SITE_TITLE + " TEXT, "
                + KEY_SITE_CONTENT + " TEXT, "
                + KEY_SITE_LONGITUDE + " TEXT, "
                + KEY_SITE_LATITUDE + " TEXT, "
                + KEY_SITE_BOBOT + " TEXT, "
                + KEY_SITE_DATE_CREATED + " TEXT, "
                + KEY_SITE_DATE_MODIFIED + " TEXT, "
                + KEY_SITE_STATUS + " TEXT "
                + "  )";
        db.execSQL(CREATE_SITE_TABLE);

        //TODO CREATE_ACTIVITY_TABLE
        String CREATE_ACTIVITY_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_ACTIVITY + "("
                + KEY_ACTIVITY_ID + " TEXT , "
                + KEY_ACTIVITY_AREA_ID + " TEXT, "
                + KEY_ACTIVITY_MILESTONE_ID + " TEXT, "
                + KEY_ACTIVITY_TITLE + " TEXT, "
                + KEY_ACTIVITY_PETUNJUK_FOTO + " TEXT "
                +")";
        db.execSQL(CREATE_ACTIVITY_TABLE);

        //TODO CREATE_ACTIVITY_LOG_TABLE
        String CREATE_ACTIVITY_LOG_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_ACTIVITY_LOG + "("
                + KEY_ACTIVITY_LOG_ID + " TEXT, "
                + KEY_ACTIVITY_LOG_USER_ID + " TEXT, "
                + KEY_ACTIVITY_LOG_SITE_ACTIVITY_ID + " TEXT, "
                + KEY_ACTIVITY_LOG_MILESTONE_ID + " TEXT, "
                + KEY_ACTIVITY_LOG_SITE_ID + " TEXT, "
                + KEY_ACTIVITY_LOG_ACTIVITY_ID + " TEXT, "
                + KEY_ACTIVITY_LOG_TITLE + " TEXT, "
                + KEY_ACTIVITY_LOG_CONTENT + " TEXT, "
                + KEY_ACTIVITY_LOG_PERCENTAGE + " TEXT, "
                + KEY_ACTIVITY_LOG_PHOTO + " TEXT, "
                + KEY_ACTIVITY_LOG_LONGITUDE + " TEXT, "
                + KEY_ACTIVITY_LOG_LATITUDE + " TEXT, "
                + KEY_ACTIVITY_LOG_DATE_CREATED + " TEXT, "
                + KEY_ACTIVITY_LOG_DATE_UPLOADED + " TEXT, "
                + KEY_ACTIVITY_LOG_DATE_MODIFIED + " TEXT, "
                + KEY_ACTIVITY_LOG_STATUS + " TEXT, "
                + KEY_ACTIVITY_LOG_ID_MODIFIED + " TEXT, "
                + KEY_ACTIVITY_LOG_STATE + " TEXT "
                +")";
        db.execSQL(CREATE_ACTIVITY_LOG_TABLE);

        //TODO CREATE_ACTIVITY_PICT_TABLE
        String CREATE_ACTIVITY_PICT_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_ACTIVITY_PICT + "("
                + KEY_ACTIVITY_PICT_ID + " TEXT, "
                + KEY_ACTIVITY_PICT_ACTIVITY_LOG_ID  + " TEXT, "
                + KEY_ACTIVITY_PICT_PHOTO  + " TEXT, "
                + KEY_ACTIVITY_PICT_DATE_CREATED  + " TEXT, "
                + KEY_ACTIVITY_PICT_BASE_64 + " TEXT "
                +")";
        db.execSQL(CREATE_ACTIVITY_PICT_TABLE);

        String CREATE_ACTIVITY_PICT_TABLE_LOCAL = "CREATE TABLE IF NOT EXISTS  " + TABLE_ACTIVITY_PICT_LOCAL + "("
                + KEY_ACTIVITY_PICT_ID + " TEXT, "
                + KEY_ACTIVITY_PICT_ACTIVITY_LOG_ID  + " TEXT, "
                + KEY_ACTIVITY_PICT_PHOTO  + " TEXT, "
                + KEY_ACTIVITY_PICT_DATE_CREATED  + " TEXT, "
                + KEY_ACTIVITY_PICT_BASE_64 + " TEXT "
                +")";
        db.execSQL(CREATE_ACTIVITY_PICT_TABLE_LOCAL);

        //TODO CREATE_ACTIVITY_LOG_LOCAL_TABLE
        String CREATE_ACTIVITY_LOG_LOCAL_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_ACTIVITY_LOG_LOCAL + "("
                + KEY_LOCAL_ACTIVITY_LOG_ID + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_USER_ID + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_SITE_ACTIVITY_ID + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_MILESTONE_ID + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_SITE_ID + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_ACTIVITY_ID + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_TITLE + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_CONTENT + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_PERCENTAGE + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_PHOTO + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_LONGITUDE + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_LATITUDE + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_DATE_CREATED + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_DATE_UPLOADED + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_DATE_MODIFIED + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_STATUS + " TEXT, "
                + KEY_LOCAL_ACTIVITY_LOG_ID_MODIFIED + " TEXT, "
                + KEY_LOCAL_PARENT_ID + " TEXT "
                +")";
        db.execSQL(CREATE_ACTIVITY_LOG_LOCAL_TABLE);

        //TODO CREATE_USER_LOG_TABLE
        String CREATE_USER_LOG_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_USER_LOG + " ("
                + KEY_LOCAL_USER_LOG_ID + " TEXT, "
                + KEY_LOCAL_USER_LOG_USER_ID + " TEXT, "
                + KEY_LOCAL_USER_LOG_TYPE_ID + " TEXT, "
                + KEY_LOCAL_USER_LOG_LOG_DESCRIPTION + " TEXT, "
                + KEY_LOCAL_USER_LOG_DATE_CREATED + " TEXT, "
                + KEY_LOCAL_USER_LOG_READ_FLAG + " TEXT "
                +") ";
        db.execSQL(CREATE_USER_LOG_TABLE);

        //TODO CREATE_MILESTONE_TABLE
        String CREATE_MILESTONE_TABLE = "CREATE TABLE IF NOT EXISTS  " + TABLE_MILESTONE + " ("
                + KEY_MILESTONE_ID + " TEXT, "
                + KEY_MILESTONE_PARENT_ID + " TEXT, "
                + KEY_MILESTONE_NAME + " TEXT "
                +")";
        db.execSQL(CREATE_MILESTONE_TABLE);




    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed

//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SITE_ACTIVITY);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SITE);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACTIVITY);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACTIVITY_LOG);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACTIVITY_PICT);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACTIVITY_LOG_LOCAL);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACTIVITY_PICT_LOCAL);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_LOG);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MILESTONE);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DASHBOARD);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MILESTONE_COUNT);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEMP_ACTIVITY_LOG_ID);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INPUT_TEMP);
//
//        // CREATE TABLE IF NOT EXISTS s again
        onCreate(db);
    }



    public void addIssue(String user_id, String site_id, String milestone_id, String activity_id, String status, String title, String content){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ISSUE_USER_ID, user_id);
        values.put(KEY_ISSUE_SITE_ID, site_id);
        values.put(KEY_ISSUE_MILESTONE_ID, milestone_id);
        values.put(KEY_ISSUE_ACTIVITY_ID, activity_id);
        values.put(KEY_ISSUE_STATUS, status);
        values.put(KEY_ISSUE_TITLE, title);
        values.put(KEY_ISSUE_CONTENT, content);

        long ids = db.insert(TABLE_ISSUE, null, values);
        db.close();
    }

    public void addIssueLocal(String user_id, String site_id, String milestone_id, String activity_id, String status, String title, String content){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ISSUE_USER_ID, user_id);
        values.put(KEY_ISSUE_SITE_ID, site_id);
        values.put(KEY_ISSUE_MILESTONE_ID, milestone_id);
        values.put(KEY_ISSUE_ACTIVITY_ID, activity_id);
        values.put(KEY_ISSUE_STATUS, status);
        values.put(KEY_ISSUE_TITLE, title);
        values.put(KEY_ISSUE_CONTENT, content);

        long ids = db.insert(TABLE_ISSUE_LOCAL, null, values);
        db.close();
    }

    public void addInputTemp(String photo){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_INPUT_TEMP_PHOTO, photo);

        long ids = db.insert(TABLE_INPUT_TEMP, null, values);
        db.close();
    }

    public void addTempPict(String session_id, String picture){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TAKE_PICT_TEMP_SESSION_ID, session_id);
        values.put(KEY_TAKE_PICT_TEMP_PICTURE, picture);
        long ids = db.insert(TABLE_TAKE_PICT_TEMP, null, values);
        db.close();
    }

    public void addTempKey(String id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TEMP_ACTIVITY_LOG_ID, id);

        long ids = db.insert(TABLE_TEMP_ACTIVITY_LOG_ID, null, values);
        db.close();
    };

    //TODO addMilestoneCount
    public void addMilestoneCount(String milestone_id, String milestone_name, String count, String site_id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_COUNT_MILESTONE_ID, milestone_id);
        values.put(KEY_COUNT_MILESTONE_NAME, milestone_name);
        values.put(KEY_COUNT_MILESTONE_COUNT, count);
        values.put(KEY_ACTIVITY_LOG_SITE_ID, site_id);

        long id = db.insert(TABLE_MILESTONE_COUNT, null, values);
        db.close();
    }

    //TODO addDashboard
    public void addDashboard( String id, String persentase, String progress, String activity){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DASHBOARD, id);
        values.put(KEY_DASHBOARD_PERSENTASE, persentase);
        values.put(KEY_DASHBOARD_PROGRESS, progress);
        values.put(KEY_DASHBOARD_ACTIVITY_LOG, activity);

        long idIns = db.insert(TABLE_DASHBOARD, null, values);
        db.close();
    }

    //TODO addUser
    public void addUser( String uid, String name, String username, String site_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_UID, uid);
        values.put(KEY_NAME, name); // Name
        values.put(KEY_USERNAME, username); // Email
        values.put(KEY_USER_SITE_ID, site_id);


        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    //TODO addSiteActivity
    public void addSiteActivity(String id,
                                String activity_id,
                                String site_id,
                                String milestone_id,
                                String kontraktor_id,
                                String sub_project_id,
                                String area_id,
                                String project_id,
                                String title,
                                String content,
                                String bobot,
                                String percentage,
                                String baseline_start,
                                String baseline_finish,
                                String date_start,
                                String date_finish,
                                String date_created,
                                String date_modified,
                                String status,
                                String gta_flag
                                ){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SITE_ACTIVITY_ID, id);
        values.put(KEY_SITE_ACTIVITY_ACT_ID, activity_id);
        values.put(KEY_SITE_ACTIVITY_SITE_ID, site_id);
        values.put(KEY_SITE_ACTIVITY_MILESTONE_ID, milestone_id);
        values.put(KEY_SITE_ACTIVITY_KONTRAKTOR_ID, kontraktor_id);
        values.put(KEY_SITE_ACTIVITY_SUB_PROJECT_ID, sub_project_id);
        values.put(KEY_SITE_ACTIVITY_AREA_ID, area_id);
        values.put(KEY_SITE_ACTIVITY_PROJECT_ID, project_id);
        values.put(KEY_SITE_ACTIVITY_TITLE, title);
        values.put(KEY_SITE_ACTIVITY_CONTENT, content);
        values.put(KEY_SITE_ACTIVITY_BOBOT, bobot);
        values.put(KEY_SITE_ACTIVITY_PERCENTAGE, percentage);
        values.put(KEY_SITE_ACTIVITY_BASELINE_START, baseline_start);
        values.put(KEY_SITE_ACTIVITY_BASELINE_FINISH, baseline_finish);
        values.put(KEY_SITE_ACTIVITY_DATE_START, date_start);
        values.put(KEY_SITE_ACTIVITY_DATE_FINISH, date_finish);
        values.put(KEY_SITE_ACTIVITY_DATE_CREATED, date_created);
        values.put(KEY_SITE_ACTIVITY_DATE_MODIFIED, date_modified);
        values.put(KEY_SITE_ACTIVITY_STATUS, status);
        values.put(KEY_SITE_ACTIVITY_FLAG, gta_flag);

        // Inserting Row
        long idIns = db.insert(TABLE_SITE_ACTIVITY, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New site activity inserted into sqlite: " + idIns);
    }

    //TODO addSite
    public void addSite( String id,
                        String kontraktor_id,
                         String sub_project_id,
                         String area_id,
                         String project_id,
                         String real_id,
                         String provinsi,
                         String kota,
                         String kecamatan,
                         String desa,
                         String title,
                         String content,
                         String longitude,
                         String latitude,
                         String bobot,
                         String date_created,
                         String date_modified,
                         String status){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SITE_ID, id);
        values.put(KEY_SITE_KONTRAKTOR_ID, kontraktor_id);
        values.put(KEY_SITE_SUB_PROJECT_ID, sub_project_id);
        values.put(KEY_SITE_AREA_ID, area_id);
        values.put(KEY_SITE_PROJECT_ID, project_id);
        values.put(KEY_SITE_REAL_ID, real_id);
        values.put(KEY_SITE_PROVINSI, provinsi);
        values.put(KEY_SITE_KOTA, kota);
        values.put(KEY_SITE_KECAMATAN, kecamatan);
        values.put(KEY_SITE_DESA, desa);
        values.put(KEY_SITE_TITLE, title);
        values.put(KEY_SITE_CONTENT, content);
        values.put(KEY_SITE_LONGITUDE, longitude);
        values.put(KEY_SITE_LATITUDE, latitude);
        values.put(KEY_SITE_BOBOT, bobot);
        values.put(KEY_SITE_DATE_CREATED, date_created);
        values.put(KEY_SITE_DATE_MODIFIED, date_modified);
        values.put(KEY_SITE_STATUS, status);

        // Inserting Row
        long ids = db.insert(TABLE_SITE, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New site  inserted into sqlite: " + ids);

    }

    //TODO addActivity
    public void addActivity(String id,
                            String area_id,
                            String milestone_id,
                            String title,
                            String petunnjuk_foto){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ACTIVITY_ID, id);
        values.put(KEY_ACTIVITY_AREA_ID, area_id);
        values.put(KEY_ACTIVITY_MILESTONE_ID, milestone_id);
        values.put(KEY_ACTIVITY_TITLE, title);
        values.put(KEY_ACTIVITY_PETUNJUK_FOTO,petunnjuk_foto);


        // Inserting Row
        long idIns = db.insert(TABLE_ACTIVITY, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New site activity inserted into sqlite: " + idIns);
    }

    //TODO addActivityLog
    public void addActivityLog( String id,
                                String user_id,
                                String site_activity_id,
                                String milestone_id,
                                String site_id,
                                String activity_id,
                                String title,
                                String content,
                                String percentage,
                                String photo,
                                String longitude,
                                String latitude,
                                String date_created,
                                String date_uploaded,
                                String date_modified,
                                String status,
                                String id_modified,
                                String state){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ACTIVITY_LOG_ID, id);
        values.put(KEY_ACTIVITY_LOG_USER_ID, user_id);
        values.put(KEY_ACTIVITY_LOG_SITE_ACTIVITY_ID, site_activity_id);
        values.put(KEY_ACTIVITY_LOG_MILESTONE_ID, milestone_id);
        values.put(KEY_ACTIVITY_LOG_SITE_ID, site_id);
        values.put(KEY_ACTIVITY_LOG_ACTIVITY_ID, activity_id);
        values.put(KEY_ACTIVITY_LOG_TITLE, title);
        values.put(KEY_ACTIVITY_LOG_CONTENT, content);
        values.put(KEY_ACTIVITY_LOG_PERCENTAGE, percentage);
        values.put(KEY_ACTIVITY_LOG_PHOTO, photo);
        values.put(KEY_ACTIVITY_LOG_LONGITUDE, longitude);
        values.put(KEY_ACTIVITY_LOG_LATITUDE, latitude);
        values.put(KEY_ACTIVITY_LOG_DATE_CREATED, date_created);
        values.put(KEY_ACTIVITY_LOG_DATE_UPLOADED, date_uploaded);
        values.put(KEY_ACTIVITY_LOG_DATE_MODIFIED, date_modified);
        values.put(KEY_ACTIVITY_LOG_STATUS, status);
        values.put(KEY_ACTIVITY_LOG_ID_MODIFIED, id_modified);
        values.put(KEY_ACTIVITY_LOG_STATE, state);

        // Inserting Row
        long idNs = db.insert(TABLE_ACTIVITY_LOG, null, values);
        addTempKey(String.valueOf(idNs));
        db.close(); // Closing database connection

        Log.d(TAG, "New site activity inserted into sqlite: " + idNs);

    }
    public void addActivityLogFromLocal(
                                String id,
                                String user_id,
                                String site_activity_id,
                                String milestone_id,
                                String site_id,
                                String activity_id,
                                String title,
                                String content,
                                String percentage,
                                String photo,
                                String longitude,
                                String latitude,
                                String date_created,
                                String date_uploaded,
                                String date_modified,
                                String status,
                                String id_modified,
                                String state){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ACTIVITY_LOG_ID, id);
        values.put(KEY_ACTIVITY_LOG_USER_ID, user_id);
        values.put(KEY_ACTIVITY_LOG_SITE_ACTIVITY_ID, site_activity_id);
        values.put(KEY_ACTIVITY_LOG_MILESTONE_ID, milestone_id);
        values.put(KEY_ACTIVITY_LOG_SITE_ID, site_id);
        values.put(KEY_ACTIVITY_LOG_ACTIVITY_ID, activity_id);
        values.put(KEY_ACTIVITY_LOG_TITLE, title);
        values.put(KEY_ACTIVITY_LOG_CONTENT, content);
        values.put(KEY_ACTIVITY_LOG_PERCENTAGE, percentage);
        values.put(KEY_ACTIVITY_LOG_PHOTO, photo);
        values.put(KEY_ACTIVITY_LOG_LONGITUDE, longitude);
        values.put(KEY_ACTIVITY_LOG_LATITUDE, latitude);
        values.put(KEY_ACTIVITY_LOG_DATE_CREATED, date_created);
        values.put(KEY_ACTIVITY_LOG_DATE_UPLOADED, date_uploaded);
        values.put(KEY_ACTIVITY_LOG_DATE_MODIFIED, date_modified);
        values.put(KEY_ACTIVITY_LOG_STATUS, status);
        values.put(KEY_ACTIVITY_LOG_ID_MODIFIED, id_modified);
        values.put(KEY_ACTIVITY_LOG_STATE,state);

        // Inserting Row
        long idNs = db.insert(TABLE_ACTIVITY_LOG, null, values);
        addTempKey(String.valueOf(idNs));
        db.close(); // Closing database connection

        Log.d(TAG, "New site activity inserted into sqlite: " + idNs);

    }

    //TODO addActivityPict
    public void addActivityPict(String id,String activity_log_id,
                            String photo,
                            String date_created, String s_photo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ACTIVITY_PICT_ID, id);
        values.put(KEY_ACTIVITY_PICT_ACTIVITY_LOG_ID, activity_log_id);
        values.put(KEY_ACTIVITY_PICT_PHOTO, photo);
        values.put(KEY_ACTIVITY_PICT_DATE_CREATED, date_created);
        values.put(KEY_ACTIVITY_PICT_BASE_64, s_photo);

        long idIns = db.insert(TABLE_ACTIVITY_PICT, null, values);
        db.close();
    }

    public void addActivityPictFromLocal(String activity_log_id,
                                String photo,
                                String date_created, String s_photo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ACTIVITY_PICT_ACTIVITY_LOG_ID, activity_log_id);
        values.put(KEY_ACTIVITY_PICT_PHOTO, photo);
        values.put(KEY_ACTIVITY_PICT_DATE_CREATED, date_created);
        values.put(KEY_ACTIVITY_PICT_BASE_64, s_photo);
        long idIns = db.insert(TABLE_ACTIVITY_PICT, null, values);
        db.close();
    }
        //TODO addActivityPict
    public void addActivityPictLocal(String activity_log_id,
                                String photo,
                                String date_created, String s_photo){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ACTIVITY_PICT_ACTIVITY_LOG_ID, activity_log_id);
        values.put(KEY_ACTIVITY_PICT_PHOTO, photo);
        values.put(KEY_ACTIVITY_PICT_DATE_CREATED, date_created);
        values.put(KEY_ACTIVITY_PICT_BASE_64, s_photo);

        // Inserting Row
        long idIns = db.insert(TABLE_ACTIVITY_PICT_LOCAL, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New site activity inserted into sqlite: " + idIns);
    }

    //TODO addActivityLog
    public void addActivityLogLocal(
                                String id,
                                String user_id,
                                String site_activity_id,
                                String milestone_id,
                                String site_id,
                                String activity_id,
                                String title,
                                String content,
                                String percentage,
                                String photo,
                                String longitude,
                                String latitude,
                                String date_created,
                                String date_uploaded,
                                String date_modified,
                                String status,
                                String id_modified,
                                String parent_id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LOCAL_ACTIVITY_LOG_ID,id);
        values.put(KEY_LOCAL_ACTIVITY_LOG_USER_ID, user_id);
        values.put(KEY_LOCAL_ACTIVITY_LOG_SITE_ACTIVITY_ID, site_activity_id);
        values.put(KEY_LOCAL_ACTIVITY_LOG_MILESTONE_ID, milestone_id);
        values.put(KEY_LOCAL_ACTIVITY_LOG_SITE_ID, site_id);
        values.put(KEY_LOCAL_ACTIVITY_LOG_ACTIVITY_ID, activity_id);
        values.put(KEY_LOCAL_ACTIVITY_LOG_TITLE, title);
        values.put(KEY_LOCAL_ACTIVITY_LOG_CONTENT, content);
        values.put(KEY_LOCAL_ACTIVITY_LOG_PERCENTAGE, percentage);
        values.put(KEY_LOCAL_ACTIVITY_LOG_PHOTO, photo);
        values.put(KEY_LOCAL_ACTIVITY_LOG_LONGITUDE, longitude);
        values.put(KEY_LOCAL_ACTIVITY_LOG_LATITUDE, latitude);
        values.put(KEY_LOCAL_ACTIVITY_LOG_DATE_CREATED, date_created);
        values.put(KEY_LOCAL_ACTIVITY_LOG_DATE_UPLOADED, date_uploaded);
        values.put(KEY_LOCAL_ACTIVITY_LOG_DATE_MODIFIED, date_modified);
        values.put(KEY_LOCAL_ACTIVITY_LOG_STATUS, status);
        values.put(KEY_LOCAL_ACTIVITY_LOG_ID_MODIFIED, id_modified);
        values.put(KEY_LOCAL_PARENT_ID, parent_id);

        // Inserting Row
        long idIns = db.insert(TABLE_ACTIVITY_LOG_LOCAL, null, values);
        addTempKey(String.valueOf(idIns));
        db.close(); // Closing database connection

        Log.d(TAG, "New site activity inserted into sqlite: " + idIns);

    }

    //TODO addUserLog
    public void addUserLog(String user_id,
                                String type_id,
                                String log_description,
                           String date_created,
                           String read_flag){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LOCAL_USER_LOG_USER_ID, user_id);
        values.put(KEY_LOCAL_USER_LOG_TYPE_ID, type_id);
        values.put(KEY_LOCAL_USER_LOG_LOG_DESCRIPTION, log_description);
        values.put(KEY_LOCAL_USER_LOG_DATE_CREATED, date_created);
        values.put(KEY_LOCAL_USER_LOG_READ_FLAG, read_flag);


        // Inserting Row
        long id = db.insert(TABLE_USER_LOG, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New site activity inserted into sqlite: " + id);
    }

    //TODO addMilestone
    public void addMilestone(String id,
                            String parent_id,
                           String name){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_MILESTONE_ID, id);
        values.put(KEY_MILESTONE_PARENT_ID, parent_id);
        values.put(KEY_MILESTONE_NAME, name);



        // Inserting Row
        long idIns = db.insert(TABLE_MILESTONE, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New site activity inserted into sqlite: " + idIns);
    }

    //TODO updateUser
    public void updateUser(String name, String username, String id, String uid){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name); // Name
        values.put(KEY_USERNAME, username); // Email


        db.update(TABLE_USER, values, "id =" + id,null);
        db.close(); // Closing database connection

    }

    // TODO getSite
    public HashMap<String, String> getSite(){
        HashMap<String, String> sitesData = new HashMap<>();
        String selectQuery = "SELECT * FROM " + TABLE_SITE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            sitesData.put("id", cursor.getString(0));
            sitesData.put("kontraktor_id", cursor.getString(1));
            sitesData.put("sub_project_id", cursor.getString(2));
            sitesData.put("area_id", cursor.getString(3));
            sitesData.put("project_id", cursor.getString(4));
            sitesData.put("real_id", cursor.getString(5));
            sitesData.put("provinsi", cursor.getString(6));
            sitesData.put("kota", cursor.getString(7));
            sitesData.put("kecamatan", cursor.getString(8));
            sitesData.put("desa", cursor.getString(9));
            sitesData.put("title", cursor.getString(10));
            sitesData.put("content", cursor.getString(11));
            sitesData.put("longitude", cursor.getString(12));
            sitesData.put("latitude", cursor.getString(13));
            sitesData.put("bobot", cursor.getString(14));
            sitesData.put("date_created", cursor.getString(15));
            sitesData.put("date_modified", cursor.getString(16));
            sitesData.put("status", cursor.getString(17));
        }
        cursor.close();
        db.close();
        return sitesData;
    }

    //TODO getMilestoneCount
    public Cursor getMilestoneCount(){
        String selectQuesry = "SELECT * FROM " + TABLE_MILESTONE_COUNT;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuesry, null);

        cursor.close();
        db.close();
        return  cursor;
    }

    //TODO getUserDetails
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("uid", cursor.getString(1));
            user.put("name", cursor.getString(2));
            user.put("username", cursor.getString(3));
            user.put("site_id", cursor.getString(4));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    public HashMap<String, String> getActivityLoglastID(){
        HashMap<String,String> lastId = new HashMap<>();
        String selectQuery = "SELECT " + KEY_ACTIVITY_LOG_ID + " from " + TABLE_ACTIVITY_LOG + " where "
                + KEY_ACTIVITY_LOG_STATE + " = 'online' order by "
                + KEY_ACTIVITY_LOG_DATE_MODIFIED + " desc limit 1";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            lastId.put(KEY_ACTIVITY_LOG_ID, cursor.getString(cursor.getColumnIndex(KEY_ACTIVITY_LOG_ID)));
        }
        cursor.close();
        db.close();

        return lastId;
    }

    public HashMap<String, String> getTempKey(){
        HashMap<String, String> dashboard = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_TEMP_ACTIVITY_LOG_ID;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            dashboard.put("id", cursor.getString(0));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + dashboard.toString());

        return dashboard;
    }

    public HashMap<String, String> getDashboardDetails() {
        HashMap<String, String> dashboard = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_DASHBOARD;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            dashboard.put("id", cursor.getString(0));
            dashboard.put("persentase", cursor.getString(1));
            dashboard.put("progress", cursor.getString(2));
            dashboard.put("activity_log", cursor.getString(3));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + dashboard.toString());

        return dashboard;
    }

    public void deleteInputTemp(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_INPUT_TEMP,null,null);
        db.close();
    }

    public void deleteInputTempWithPhoto(){
        String query = "SELECT * FROM " +TABLE_INPUT_TEMP;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            do{
                File file = new File(cursor.getString(cursor.getColumnIndex("photo")));
                file.delete();
            }while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
    }

    public void deleteTempLocal(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ACTIVITY_LOG,"id ='" + id+"'",null);
        db.delete(TABLE_ACTIVITY_PICT, "activity_log_id ='"+id+"' ",null);
        String selectQuery = "SELECT  * FROM " + TABLE_ACTIVITY_LOG_LOCAL + " where parent_id='"+id+"'";
        SQLiteDatabase dbs = this.getReadableDatabase();
        Cursor cursor = dbs.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            String selectPict = "SELECT * FROM " +TABLE_ACTIVITY_PICT_LOCAL+ " where activity_log_id ='"+id+"'";
            SQLiteDatabase dbx = this.getReadableDatabase();
            Cursor cursorx = dbx.rawQuery(selectPict, null);
            cursorx.moveToFirst();
            if(cursorx.getCount() > 0){
                do{
                    File file = new File(cursorx.getString(cursorx.getColumnIndex("photo")));
                    Log.println(Log.DEBUG,"Delete File", file.getPath());
                    file.delete();
                }while (cursorx.moveToNext());
            }
            cursorx.close();

            db.delete(TABLE_ACTIVITY_PICT_LOCAL, "activity_log_id ='"+id+"' ",null);
            db.delete(TABLE_ACTIVITY_LOG_LOCAL,"parent_id ='" + id+"'",null);
            dbx.close();
        }
        cursor.close();
        dbs.close();

        db.close();
    }
    public void deleteTempKey(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TEMP_ACTIVITY_LOG_ID, null, null);
        db.close();
    };

    public void deleteLocalData(){
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
//        db.delete(TABLE_USER, null, null);
        db.delete(TABLE_ACTIVITY, null,null);
        db.delete(TABLE_MILESTONE_COUNT, null,null);
        db.delete(TABLE_MILESTONE, null, null);
        db.delete(TABLE_SITE_ACTIVITY, null, null);
        db.delete(TABLE_SITE, null , null);
        db.delete(TABLE_DASHBOARD, null, null);
        db.delete(TABLE_ACTIVITY_PICT, null, null);
        db.delete(TABLE_ACTIVITY_LOG, null, null);
        db.delete(TABLE_ACTIVITY_LOG_LOCAL, null, null);
        db.delete(TABLE_MILESTONE_COUNT, null,null);
        db.close();
    }


    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.delete(TABLE_ACTIVITY, null,null);
        db.delete(TABLE_MILESTONE_COUNT, null,null);
        db.delete(TABLE_MILESTONE, null, null);
        db.delete(TABLE_SITE_ACTIVITY, null, null);
        db.delete(TABLE_SITE, null , null);
        db.delete(TABLE_DASHBOARD, null, null);
        db.delete(TABLE_ACTIVITY_PICT, null, null);
        db.delete(TABLE_ACTIVITY_LOG, null, null);
        db.delete(TABLE_ACTIVITY_LOG_LOCAL, null, null);
        db.delete(TABLE_MILESTONE_COUNT, null,null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }
}
