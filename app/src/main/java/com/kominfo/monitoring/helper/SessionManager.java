package com.kominfo.monitoring.helper;

/**
 * Created by Corexfire on 6/29/2017.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager {

    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences pref2;
    SharedPreferences pref3;
    SharedPreferences pref4;
    SharedPreferences pref5;
    SharedPreferences pref6;
    SharedPreferences pref7;
    SharedPreferences pref8;
    SharedPreferences pref9;

    SharedPreferences pref_site_activity_id;
    SharedPreferences pref_precentage;
    SharedPreferences pref_date_modified;
    SharedPreferences pref_gta_flag;

    Editor editor;
    Editor editor2;
    Editor editor3;
    Editor editor4;
    Editor editor5;
    Editor editor6;
    Editor editor7;
    Editor editor8;
    Editor editor9;

    Editor editor_site_activity_id;
    Editor editor_percentage;
    Editor editor_date_modified;
    Editor editor_gta_flag;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_SITE_ACTIVITY_ID = "site_activity_id";
    private static final String PREF_PERCENTAGE = "percentage";
    private static final String PREF_DATE_MODIFIED = "date_modified";
    private static final String PREF_GTA_FLAG = "gta_flag";
    private static final String PREF_NAME = "dashboardmonitoring_db";
    private static final String PREF_NAME2 = "download_complete";
    private static final String PREF_NAME3 = "site_id";
    private static final String PREF_NAME4 = "site_name";
    private static final String PREF_NAME5 = "milestone_name";
    private static final String PREF_NAME6 = "activity_name";
    private static final String PREF_NAME7 = "milestone_id";
    private static final String PREF_NAME8 = "activity_id";
    private static final String PREF_NAME9 = "session_input";

    private static final String KEY_SITE_ACTIVITY_ID = "site_activity_id";
    private static final String KEY_PERCENTAGE = "percentage";
    private static final String KEY_DATE_MODIFIED = "date_modified";
    private static final String KEY_GTA_FLAG = "gta_flag";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_IS_PROCESS_COMPLETE = "isComplete";
    private static final String KEY_SITE_ID = "site_id";
    private static final String KEY_SITE_NAME = "site_name";
    private static final String KEY_MILESTONE_NAME = "milestone_name";
    private static final String KEY_ACTIVITY_NAME = "activity_name";
    private static final String KEY_MILESTONE_ID = "milestone_id";
    private static final String KEY_ACTIVITY_ID = "activity_id";
    private static final String KEY_SESSION_INPUT = "session_input";


    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        pref2 = _context.getSharedPreferences(PREF_NAME2, PRIVATE_MODE);
        pref3 = _context.getSharedPreferences(PREF_NAME3,PRIVATE_MODE);
        pref4 = _context.getSharedPreferences(PREF_NAME4, PRIVATE_MODE);
        pref5 = _context.getSharedPreferences(PREF_NAME5, PRIVATE_MODE);
        pref6 = _context.getSharedPreferences(PREF_NAME6, PRIVATE_MODE);
        pref7 = _context.getSharedPreferences(PREF_NAME7,PRIVATE_MODE);
        pref8 = _context.getSharedPreferences(PREF_NAME8,PRIVATE_MODE);
        pref9 = _context.getSharedPreferences(PREF_NAME9,PRIVATE_MODE);
        pref_site_activity_id = _context.getSharedPreferences(PREF_SITE_ACTIVITY_ID, PRIVATE_MODE);
        pref_precentage = _context.getSharedPreferences(PREF_PERCENTAGE, PRIVATE_MODE);
        pref_date_modified = _context.getSharedPreferences(PREF_DATE_MODIFIED,PRIVATE_MODE);
        pref_gta_flag = _context.getSharedPreferences(PREF_GTA_FLAG,PRIVATE_MODE);

        editor = pref.edit();
        editor2 = pref2.edit();
        editor3 = pref3.edit();
        editor4 = pref4.edit();
        editor5 = pref5.edit();
        editor6 = pref6.edit();
        editor7 = pref7.edit();
        editor8 = pref8.edit();
        editor9 = pref9.edit();
        editor_site_activity_id = pref_site_activity_id.edit();
        editor_percentage = pref_precentage.edit();
        editor_date_modified = pref_date_modified.edit();
        editor_gta_flag = pref_gta_flag.edit();
    }

    public void setSiteActivityId(String site_activity_id){
        editor_site_activity_id.putString(KEY_SITE_ACTIVITY_ID, site_activity_id);
        editor_site_activity_id.commit();
    }

    public void setPercentage(String percentage){
        editor_percentage.putString(KEY_PERCENTAGE, percentage);
        editor_percentage.commit();
    }

    public void setDateModified(String date_modified){
        editor_date_modified.putString(KEY_DATE_MODIFIED, date_modified);
        editor_date_modified.commit();
    }

    public void setGtaFlag(String gta_flag){
        editor_gta_flag.putString(KEY_GTA_FLAG,gta_flag);
        editor_gta_flag.commit();
    }

    public void setSessionInput(String sessionInput){
        editor9.putString(KEY_SESSION_INPUT, sessionInput);
        editor9.commit();
    }

    public void setActivityID(String isActiviyId){
        editor8.putString(KEY_ACTIVITY_ID,isActiviyId);
        editor8.commit();
    }
    public void setMilestoneID(String isMilestoneId){
        editor7.putString(KEY_MILESTONE_ID,isMilestoneId);
        editor7.commit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public void setComplete(boolean isComplete){
        editor2.putBoolean(KEY_IS_PROCESS_COMPLETE, isComplete);
        editor2.commit();
    }

    public void setSiteId(String siteId){
        editor3.putString(KEY_SITE_ID,siteId);
        editor3.commit();
    }

    public void setSiteName(String siteName){
        editor4.putString(KEY_SITE_NAME,siteName);
        editor4.commit();
    }

    public void setMilestoneName(String milestoneName){
        editor5.putString(KEY_MILESTONE_NAME,milestoneName);
        editor5.commit();
    }

    public void setActivityName(String activityName){
        editor6.putString(KEY_ACTIVITY_NAME, activityName);
        editor6.commit();
    }

    public String isSiteActivityId(){return  pref_site_activity_id.getString(KEY_SITE_ACTIVITY_ID,null);}
    public String isPercentage(){return  pref_precentage.getString(KEY_PERCENTAGE, null);}
    public String isDateModified(){return  pref_date_modified.getString(KEY_DATE_MODIFIED,null);}
    public String isGtaFlag(){return  pref_gta_flag.getString(KEY_GTA_FLAG,null);}
    public String isSessionInput(){return pref9.getString(KEY_SESSION_INPUT,null);}
    public String isActivityId(){return pref8.getString(KEY_ACTIVITY_ID,null);}
    public String isMilestoneId(){return pref7.getString(KEY_MILESTONE_ID,null);}
    public String isMilestoneName(){return pref5.getString(KEY_MILESTONE_NAME, null);}
    public String isActivityName(){return  pref6.getString(KEY_ACTIVITY_NAME,null);}
    public String isSiteName(){return pref4.getString(KEY_SITE_NAME,null); }
    public String isSiteId(){return pref3.getString(KEY_SITE_ID,null);}
    public boolean isComplete(){ return  pref2.getBoolean(KEY_IS_PROCESS_COMPLETE, false);}
    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }
}