package com.kominfo.monitoring.helper;

import android.content.Context;
import android.util.Log;

/**
 * Created by zubai on 9/5/2017.
 */

public class SiteCoordinateChecker {
    private Boolean isLocked = true;
    private double radius;
    private static final double MAX_DISTANCE_OH = 0.1;
    private static final double MAX_DISTANCE_FT = 0.05;

    public Boolean coordinateChecker(String project_id, double current_longitude, double current_latitude, double site_longitude, double site_latitude, String gta_flag ){
        Log.println(Log.DEBUG,"gta_flag",gta_flag);
        if(gta_flag.equals("1")) {
            if (project_id.equals("1"))
                radius = MAX_DISTANCE_OH;
            else if (project_id.equals("2"))
                radius = MAX_DISTANCE_FT;
            else if (project_id.equals("3"))
                radius = MAX_DISTANCE_OH;

            if (pointInCircle(current_latitude, current_longitude, radius, site_latitude, site_longitude)) {
                return true;
            } else {
                return false;
            }
        }else{
            return true;
        }

    }

    boolean pointInCircle(double lat0, double lon0, double r, double lat, double lon) {
        double C = 40075.04, A = 360*r/C, B = A/Math.cos(Math.toRadians(lat0));
        return Math.pow((lat-lat0)/A, 2) + Math.pow((lon-lon0)/B, 2) < 1;
    }




}
