package com.kominfo.monitoring.model;

/**
 * Created by Corexfire on 7/5/2017.
 */

public class ActivityDetail {
    
    public ActivityDetail (String id,
                           String activity_id,
                            String site_id,
                            String milestone_id,
                            String kontraktor_id,
                            String sub_project_id,
                            String area_id,
                            String project_id,
                            String title,
                            String content,
                            String bobot,
                            String percentage,
                            String baseline_start,
                            String baseline_finish,
                            String date_start,
                            String date_finish,
                            String date_created,
                            String date_modified,
                            String status,
                            String gta_flag){
        this.id = id;
        this.activity_id = activity_id;
        this.site_id = site_id;
        this.area_id = area_id;
        this.baseline_finish = baseline_finish;
        this.baseline_start = baseline_start;
        this.milestone_id = milestone_id;
        this.project_id = project_id;
        this.kontraktor_id = kontraktor_id;
        this.sub_project_id = sub_project_id;
        this.title = title;
        this.content = content;
        this.bobot = bobot;
        this.percentage = percentage;
        this.date_start = date_start;
        this.date_created = date_created;
        this.date_finish = date_finish;
        this.date_modified = date_modified;
        this.status = status;
        this.gta_flag = gta_flag;
    }
    private String id;
    private String activity_id;
    private String site_id;
    private String milestone_id;
    private String kontraktor_id;
    private String sub_project_id;
    private String area_id;



    private String project_id;
    private String title;
    private String content;
    private String bobot;
    private String percentage;
    private String baseline_start;
    private String baseline_finish;
    private String date_start;
    private String date_finish;
    private String date_created;
    private String date_modified;


    public String getProject_id() {
        return project_id;
    }
    public String getGta_flag() {
        return gta_flag;
    }

    private String gta_flag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getactivity_id() {
        return activity_id;
    }

    public void setactivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getMilestone_id() {
        return milestone_id;
    }

    public void setMilestone_id(String milestone_id) {
        this.milestone_id = milestone_id;
    }

    public String getKontraktor_id() {
        return kontraktor_id;
    }

    public void setKontraktor_id(String kontraktor_id) {
        this.kontraktor_id = kontraktor_id;
    }

    public String getSub_project_id() {
        return sub_project_id;
    }

    public void setSub_project_id(String sub_project_id) {
        this.sub_project_id = sub_project_id;
    }

    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBobot() {
        return bobot;
    }

    public void setBobot(String bobot) {
        this.bobot = bobot;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getBaseline_start() {
        return baseline_start;
    }

    public void setBaseline_start(String baseline_start) {
        this.baseline_start = baseline_start;
    }

    public String getBaseline_finish() {
        return baseline_finish;
    }

    public void setBaseline_finish(String baseline_finish) {
        this.baseline_finish = baseline_finish;
    }

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public String getDate_finish() {
        return date_finish;
    }

    public void setDate_finish(String date_finish) {
        this.date_finish = date_finish;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(String date_modified) {
        this.date_modified = date_modified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

}
