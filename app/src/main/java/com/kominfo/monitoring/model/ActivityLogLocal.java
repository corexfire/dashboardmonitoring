package com.kominfo.monitoring.model;

/**
 * Created by Corexfire on 7/11/2017.
 */

public class ActivityLogLocal {

    public ActivityLogLocal(
            String id,
            String user_id,
            String site_activity_id,
            String milestone_id,
            String site_id,
            String activity_id,
            String title,
            String content,
            String percentage,
            String photo,
            String longitude,
            String latitude,
            String date_created,
            String date_uploaded,
            String date_modified,
            String status,
            String id_modified,
            String state
    ){
        this.id = id;
        this.user_id = user_id;
        this.site_activity_id = site_activity_id;
        this.milestone_id = milestone_id;
        this.site_id = site_id;
        this.activity_id = activity_id;
        this.title = title;
        this.content = content;
        this.percentage = percentage;
        this.photo = photo;
        this.longitude = longitude;
        this.latitude = latitude;
        this.date_created = date_created;
        this.date_uploaded = date_uploaded;
        this.date_modified = date_modified;
        this.status = status;
        this.id_modified = id_modified;
        this.state = state;

    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSite_activity_id() {
        return site_activity_id;
    }

    public void setSite_activity_id(String site_activity_id) {
        this.site_activity_id = site_activity_id;
    }

    public String getMilestone_id() {
        return milestone_id;
    }

    public void setMilestone_id(String milestone_id) {
        this.milestone_id = milestone_id;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDate_uploaded() {
        return date_uploaded;
    }

    public void setDate_uploaded(String date_uploaded) {
        this.date_uploaded = date_uploaded;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(String date_modified) {
        this.date_modified = date_modified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId_modified() {
        return id_modified;
    }

    public void setId_modified(String id_modified) {
        this.id_modified = id_modified;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    private String state;


    private String id;
    private String user_id;
    private String site_activity_id;
    private String milestone_id;
    private String site_id;
    private String activity_id;
    private String title;
    private String content;
    private String percentage;
    private String photo;
    private String longitude;
    private String latitude;
    private String date_created;
    private String date_uploaded;
    private String date_modified;
    private String status;
    private String id_modified;


}
