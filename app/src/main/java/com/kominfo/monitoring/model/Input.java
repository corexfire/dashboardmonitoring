package com.kominfo.monitoring.model;

/**
 * Created by Corexfire on 6/22/2017.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.kominfo.monitoring.helper.SessionManager;

public class Input {

    private static String TAG = Input.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences pref2;

    Editor editor;
    Editor editor2;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    private static final String PREF_ID = "milestone_shared_id";
    private static final String PREF_NAME = "milestone_shared_name";

    private static final String KEY_MILESTONE_ID = "milestone_id";
    private static final String KEY_MILESTONE_NAME = "milestone_name";

    public Input(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_ID,PRIVATE_MODE);
        pref2 = _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = pref.edit();
        editor2 = pref2.edit();
    }

    public void setMilestone(String id, String name){
        editor.putString(KEY_MILESTONE_ID, id);
        editor2.putString(KEY_MILESTONE_NAME, name);

        editor.commit();
        editor2.commit();

    }



}
