package com.kominfo.monitoring.model;

/**
 * Created by zubai on 8/3/2017.
 */

public class Issue {

    public Issue(String id,String user_id,  String site_id, String milestone_id, String activity_id, String title, String content, String status, String site_name, String milestone_name, String activity_name){
        this.id = id;
        this.user_id = user_id;
        this.site_id = site_id;
        this.milestone_id = milestone_id;
        this.activity_id = activity_id;
        this.title = title;
        this.content = content;
        this.status = status;
        this.milestone_name = milestone_name;
        this.activity_name = activity_name;
        this.site_name = site_name;
    }

    public String getSite_name() {
        return site_name;
    }

    public String getMilestone_name() {
        return milestone_name;
    }

    public String getActivity_name() {
        return activity_name;
    }

    private String site_name;
    private String milestone_name;
    private String activity_name;

    public String getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getSite_id() {
        return site_id;
    }

    public String getMilestone_id() {
        return milestone_id;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getStatus() {
        return status;
    }

    private String id;
    private String user_id;
    private String site_id;
    private String milestone_id;
    private String activity_id;
    private String title;
    private String content;
    private String status;

}
