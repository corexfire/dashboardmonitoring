package com.kominfo.monitoring.model;

/**
 * Created by Corexfire on 7/5/2017.
 */

public class MilestoneCount {

    public MilestoneCount(String milestone_id, String milestone_name, String count){
        this.milestone_id = milestone_id;
        this.milestone_name = milestone_name;
        this.count = count;
    }

    public String getMilestone_id() {
        return milestone_id;
    }

    public void setMilestone_id(String milestone_id) {
        this.milestone_id = milestone_id;
    }

    public String getMilestone_name() {
        return milestone_name;
    }

    public void setMilestone_name(String milestone_name) {
        this.milestone_name = milestone_name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    private String milestone_id;
    private String milestone_name;
    private String count;
}
