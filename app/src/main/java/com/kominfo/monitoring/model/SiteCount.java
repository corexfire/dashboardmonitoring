package com.kominfo.monitoring.model;

/**
 * Created by zubai on 7/29/2017.
 */

public class SiteCount {

    public SiteCount(String id, String name, String latitude, String longitude){
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    private String id;
    private String name;
    private String longitude;
    private String latitude;


}
