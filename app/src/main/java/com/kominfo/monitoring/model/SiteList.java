package com.kominfo.monitoring.model;

/**
 * Created by zubai on 7/27/2017.
 */

public class SiteList {

    public SiteList(String id, String name, String latitude, String longitude, String siteCount){
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.siteCount = siteCount;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getSiteCount() {
        return siteCount;
    }

    private String id;
    private String name;



    private String siteCount;
    private String longitude;
    private String latitude;
}
