package com.kominfo.monitoring.model;

/**
 * Created by Corexfire on 6/22/2017.
 */

public class User {
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String password;
    private String username;
}
